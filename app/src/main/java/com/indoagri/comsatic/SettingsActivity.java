package com.indoagri.comsatic;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.indoagri.comsatic.Retrofit.DB.DatabaseQuery;
import com.indoagri.comsatic.Retrofit.Model.UserModel;
import com.indoagri.comsatic.Retrofit.SharePreference;


public class SettingsActivity extends AppCompatActivity {
    DatabaseQuery query;
    UserModel userModel;
    SharePreference sharePreference;
    Toolbar toolbar;
    TextView mTextToolbar;
    Button btnLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        setUpToolbar();
        btnLogout = (findViewById(R.id.btn_logout));
        sharePreference = new SharePreference(SettingsActivity.this);
        btnLogout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                sharePreference.setLogin(false);
                Intent a = new Intent(SettingsActivity.this, LoginActivity.class);
                a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                finish();
                startActivity(a);
            }
        });
        CardView cardfooter = (findViewById(R.id.cardFooter));
        TextView txtAppsName = (findViewById(R.id.card_appName));
        txtAppsName.setText(getResources().getString(R.string.app_description));
    }
    private void setUpToolbar() {
        toolbar = (findViewById(R.id.toolbar));
        mTextToolbar= (findViewById(R.id.txt_Toolbar));
        mTextToolbar.setText("Setting");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

}
