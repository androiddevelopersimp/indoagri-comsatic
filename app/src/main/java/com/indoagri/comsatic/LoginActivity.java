package com.indoagri.comsatic;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.indoagri.comsatic.Common.Constants;
import com.indoagri.comsatic.Common.DeviceUtils;
import com.indoagri.comsatic.Retrofit.ApiServices;
import com.indoagri.comsatic.Retrofit.DB.DatabaseQuery;
import com.indoagri.comsatic.Retrofit.Model.UserModel;
import com.indoagri.comsatic.Retrofit.NetClient;
import com.indoagri.comsatic.Retrofit.SharePreference;
import com.indoagri.comsatic.Retrofit.UsersLoginResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.indoagri.comsatic.Retrofit.Model.UserModel.XML_DOMAIN;
import static com.indoagri.comsatic.Retrofit.Model.UserModel.XML_EMAIL;
import static com.indoagri.comsatic.Retrofit.Model.UserModel.XML_USERNAME;
import static com.indoagri.comsatic.Retrofit.Model.UserModel.XML_USERPASSWORD;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "LOGIN";
    TextInputLayout emailWrapper,usernameWrapper,passwordWrapper;
    EditText editUsername,editEmail,editPassword,editDomain;
    Spinner DomainSpinner;
    CheckBox checkBoxAggree;
    boolean aggree = false;
    Button btnLogin;
    ApiServices apiServices;
    ProgressDialog pDialog;
    DatabaseQuery query;
    UserModel userModel;
    UsersLoginResponse usersLoginResponse;
    SharePreference sharePreference;
    boolean isLogin;
    String userad;
    String useremail;
    String userdomain;
    String userpassword;
    Cursor cursor;
    @Override
    protected void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        apiServices = NetClient.AuthProduction().create(ApiServices.class);
        TextView txtVersion = (findViewById(R.id.txtVersion));
        txtVersion.setText("Vers. "+Constants.versionApps);
        query = new DatabaseQuery(LoginActivity.this);
        pDialog = new ProgressDialog(getApplicationContext());
        emailWrapper = (findViewById(R.id.email_wrapper));
        usernameWrapper = (findViewById(R.id.username_wrapper));
        passwordWrapper = (findViewById(R.id.password_wrapper));
        editUsername = (findViewById(R.id.etUsername));
        editEmail = (findViewById(R.id.etEmail));
        editPassword = (findViewById(R.id.etPassword));
        editDomain = (findViewById(R.id.editDomain));
        DomainSpinner = (findViewById(R.id.DomainSpinner));
        checkBoxAggree = (findViewById(R.id.checkboxAggree));
        btnLogin = (findViewById(R.id.btn_login));
        btnLogin.setOnClickListener(this);
        sharePreference = new SharePreference(LoginActivity.this);
        setSpinner();
        DomainSpinnerInit();
        setCheckBoxListener();
        if(sharePreference.isFormLoginEmail()!=null){
            editEmail.setText(sharePreference.isFormLoginEmail());
        }
        if(sharePreference.isFormLoginUsername()!=null){
            editUsername.setText(sharePreference.isFormLoginUsername());
        }
        initLogin();
    }

    private void initLogin(){
        cursor = getContentResolver().query(Uri.parse("content://com.simp.portal.db.UserProvider/users"), null, null, null, null);
        if(cursor!=null){
            if (cursor.moveToFirst()) {
                isLogin = (cursor.getInt(cursor.getColumnIndex("isLogin")) == 1);
                if(isLogin){
                    userad = cursor.getString(cursor.getColumnIndex(XML_USERNAME));
                    useremail = cursor.getString(cursor.getColumnIndex(XML_EMAIL));
                    userdomain = cursor.getString(cursor.getColumnIndex(XML_DOMAIN));
                    userpassword = cursor.getString(cursor.getColumnIndex(XML_USERPASSWORD));
                    String Message = getResources().getString(R.string.msg_process_login);
                    GetuserForLogin(LoginActivity.this,useremail,Message,userpassword,userad,userdomain);
                }else{
                    editUsername.setText("");
                    editEmail.setText("");
                    editPassword.setText("");
                }
            }else{
                editUsername.setText("");
                editEmail.setText("");
                editPassword.setText("");
            }
        }else{
            editUsername.setText("");
            editEmail.setText("");
            editPassword.setText("");
        }
    }
    private void DomainSpinnerInit(){
        DomainSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View v,
                                       int postion, long arg3) {
                String SpinerValue3 = parent.getItemAtPosition(postion).toString();
                //or this can be also right: selecteditem = level[i];
                if(SpinerValue3.equalsIgnoreCase("SIMP")){
                    editDomain.setText("INDOFOOD");
                }else{
                    editDomain.setText(SpinerValue3);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }
        });
    }

    void setSpinner(){
        ArrayAdapter adapter = ArrayAdapter.createFromResource(
                getApplicationContext(), R.array.domainspinner, R.layout.item_spinner);
        adapter.setDropDownViewResource(R.layout.item_spinner);
        DomainSpinner.setAdapter(adapter);
    }
    private void setCheckBoxListener() {
        checkBoxAggree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked()) {
                    aggree = true;
                }
            }
        });
    }
    @Override
    public void onClick(View v) {

            if(v==btnLogin){

                if(editPassword.getText().toString().length()<=0){
                    Toast.makeText(getApplicationContext(),"Password masih Kosong",Toast.LENGTH_LONG).show();
                    return;
                }
                if(editUsername.getText().toString().length()<=0){
                    Toast.makeText(getApplicationContext(),"User AD masih kosong",Toast.LENGTH_LONG).show();
                    return;
                }
                userad = editUsername.getText().toString();
                useremail = editEmail.getText().toString();
                userdomain = editDomain.getText().toString();
                userpassword = editPassword.getText().toString();
                String Message = getResources().getString(R.string.msg_process_login);
                GetuserForLogin(LoginActivity.this,useremail,Message,userpassword,userad,userdomain);

            }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private void GetuserForLogin(Context context,final String email,String DialogMessage,String password, final String username,String domain){
        pDialog = new ProgressDialog(context);
        pDialog.setMessage(DialogMessage);
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
        pDialog.show();
        Log.d(TAG, "loadFirstPage: ");

        getUsersMobile(email,password,username,domain).enqueue(new Callback<UsersLoginResponse>() {
            @Override
            public void onResponse(Call<UsersLoginResponse> call, Response<UsersLoginResponse> response) {
                pDialog.dismiss();
                if(response.code()==200){
                    // Toast.makeText(getApplicationContext(),"Berhasil",Toast.LENGTH_SHORT).show();
                    usersLoginResponse = fetchResults(response);
                    userModel = usersLoginResponse.getUserLogin();
                    if(query.COUNT(UserModel.TABLE_NAME,"Select * from "+UserModel.TABLE_NAME)==0){
                        query.INSERTQUERY(UserModel.TABLE_NAME,"INSERT INTO "+UserModel.TABLE_NAME+" ( "+
                                UserModel.XML_USERID+","+
                                XML_USERNAME+","+
                                UserModel.XML_SURNAME+","+
                                XML_EMAIL+","+
                                UserModel.XML_LDAP+","+
                                XML_DOMAIN+","+
                                UserModel.XML_USERDESCRIPTION+","+
                                UserModel.XML_COMPANY+","+
                                UserModel.XML_PROVINCE+","+
                                UserModel.XML_DEPARTMENT+","+
                                UserModel.XML_EMPLOYEEID+","+
                                UserModel.XML_USERFULLNAME+","+
                                UserModel.XML_USERPHONE+","+
                                XML_USERPASSWORD+","+
                                UserModel.XML_USERKEY+" ) VALUES ( "+
                                "'"+userModel.getUSERID()+"',"+
                                "'"+userModel.getUSERNAME().toLowerCase()+"',"+
                                "'"+userModel.getSURNAME()+"',"+
                                "'"+userModel.getEMAIL()+"',"+
                                "'"+userModel.getLDAP()+"',"+
                                "'"+userModel.getDOMAIN()+"',"+
                                "'"+userModel.getDESCRIPTION()+"',"+
                                "'"+userModel.getCOMPANY()+"',"+
                                "'"+userModel.getPROVINCE()+"',"+
                                "'"+userModel.getDEPARTMENT()+"',"+
                                "'"+userModel.getEMPLOYEEID()+"',"+
                                "'"+userModel.getFULLNAME()+"',"+
                                "'"+userModel.getPHONE()+"',"+
                                "'"+userpassword+"',"+
                                "'"+userModel.getKEY()+"')");
                    }
                    else{
                        if(query.DELETEEXISTING(UserModel.TABLE_NAME,"DELETE FROM "+UserModel.TABLE_NAME)){
                            query.INSERTQUERY(UserModel.TABLE_NAME,"INSERT INTO "+UserModel.TABLE_NAME+" ( "+
                                    UserModel.XML_USERID+","+
                                    XML_USERNAME+","+
                                    UserModel.XML_SURNAME+","+
                                    UserModel.XML_EMAIL+","+
                                    UserModel.XML_LDAP+","+
                                    XML_DOMAIN+","+
                                    UserModel.XML_USERDESCRIPTION+","+
                                    UserModel.XML_COMPANY+","+
                                    UserModel.XML_PROVINCE+","+
                                    UserModel.XML_DEPARTMENT+","+
                                    UserModel.XML_EMPLOYEEID+","+
                                    UserModel.XML_USERFULLNAME+","+
                                    UserModel.XML_USERPHONE+","+
                                    XML_USERPASSWORD+","+
                                    UserModel.XML_USERKEY+" ) VALUES ( "+
                                    "'"+userModel.getUSERID()+"',"+
                                    "'"+userModel.getUSERNAME().toLowerCase()+"',"+
                                    "'"+userModel.getSURNAME()+"',"+
                                    "'"+userModel.getEMAIL()+"',"+
                                    "'"+userModel.getLDAP()+"',"+
                                    "'"+userModel.getDOMAIN()+"',"+
                                    "'"+userModel.getDESCRIPTION()+"',"+
                                    "'"+userModel.getCOMPANY()+"',"+
                                    "'"+userModel.getPROVINCE()+"',"+
                                    "'"+userModel.getDEPARTMENT()+"',"+
                                    "'"+userModel.getEMPLOYEEID()+"',"+
                                    "'"+userModel.getFULLNAME()+"',"+
                                    "'"+userModel.getPHONE()+"',"+
                                    "'"+userpassword+"',"+
                                    "'"+userModel.getKEY()+"')");
                        }
                    }

                    sharePreference.setKeyEmailuser(userModel.getEMAIL());
                    sharePreference.setKeyUserdomain(userModel.getDOMAIN());
                    sharePreference.setKeyPhoneuser(userModel.getPHONE());
                    sharePreference.setKeyUsername(userModel.getUSERNAME().toLowerCase());
                    sharePreference.setKeyUserfullname(userModel.getFULLNAME());
                    sharePreference.setKeyUserdepartment(userModel.getDEPARTMENT());
                    sharePreference.setFORMLoginEmail(email);
                    sharePreference.setFormLoginUsername(username.toLowerCase());
                    sharePreference.setKeyUserpassword(userpassword);
                    sharePreference.setAggree(true);
                    sharePreference.setLogin(true);
                    String Message = getResources().getString(R.string.msg_process_login);
                    usersLoginResponse = fetchResults(response);
                    userModel = usersLoginResponse.getUserLogin();
                    Intent a = new Intent(LoginActivity.this, MainActivity.class);
                    a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    finish();
                    startActivity(a);
                }else{
                    Toast.makeText(getApplicationContext(),"gagal mengambil TOken",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UsersLoginResponse> call, Throwable t) {
                t.printStackTrace();
//                    progressBar.setVisibility(View.GONE);
                pDialog.dismiss();
                Toast.makeText(getApplicationContext(),t.getMessage(),Toast.LENGTH_SHORT).show();
                // TODO: 08/11/16 handle failure
            }
        });

    }

    private UsersLoginResponse fetchResults(Response<UsersLoginResponse> response) {
        UsersLoginResponse loginResponse = response.body();
        return loginResponse;
    }
    private Call<UsersLoginResponse> getUsersMobile(String email, String password, String username, String domain) {
//        String device = DeviceUtils.getDeviceID(LoginActivity.this);
        String androidId = Settings.Secure.getString(LoginActivity.this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return apiServices.LoginPermission(Constants.APIKEY,username,password,domain,androidId);
    }

}
