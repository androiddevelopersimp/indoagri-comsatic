package com.indoagri.comsatic.stock;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.indoagri.comsatic.ChooseDate;
import com.indoagri.comsatic.MainActivity;
import com.indoagri.comsatic.R;
import com.indoagri.comsatic.Retrofit.ApiServices;
import com.indoagri.comsatic.Retrofit.DB.DatabaseQuery;
import com.indoagri.comsatic.Retrofit.Model.GroupCompanyModel;
import com.indoagri.comsatic.Retrofit.Model.GroupProductModel;
import com.indoagri.comsatic.Retrofit.NetClient;
import com.indoagri.comsatic.Retrofit.Model.SalesStockModel;
import com.indoagri.comsatic.Retrofit.SharePreference;
import com.indoagri.comsatic.Retrofit.StockResponse;
import com.indoagri.comsatic.price.GrafikActivity;
import com.indoagri.comsatic.widget.calendar.activity.CustomCalendar;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StockActivity extends AppCompatActivity implements View.OnClickListener, CustomCalendar.CalendarListener {

    private int LISTDATE = 21;
    private int PRODUCTLIST = 22;
    private int COMPANYLIST = 23;

    private ApiServices apiServices;
    private DatabaseQuery database;
    Button btnSearch;
    TableLayout tableStock;
    TextView tvLastUpdate;
    EditText etDate;
    //    Spinner spCompany;
//    Spinner spProduct;
    EditText etCompany;
    EditText etProduct;
    ProgressDialog pDialog;

    String companyCode;
    String productCode;

    List<SalesStockModel> salesStockRespons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView title = (TextView) findViewById(R.id.title_toolbar);
        title.setText("Stock");

        tvLastUpdate = findViewById(R.id.tv_lastupdate);
        btnSearch = findViewById(R.id.btn_search);
        tableStock = findViewById(R.id.table_header);
        etDate = findViewById(R.id.et_date_from);
        etCompany = findViewById(R.id.et_company);
        etProduct = findViewById(R.id.et_product);
//        spCompany = findViewById(R.id.sp_company);
//        spProduct = findViewById(R.id.sp_product);

        database = new DatabaseQuery(StockActivity.this);
        apiServices = NetClient.DataProduction().create(ApiServices.class);

        pDialog = new ProgressDialog(StockActivity.this);
        btnSearch.setOnClickListener(this);
        etDate.setOnClickListener(this);
        etCompany.setOnClickListener(this);
        etProduct.setOnClickListener(this);

        if (deleteDataStock()) {
            getDataStock();
        }


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.et_date_from:
                Intent intent = new Intent(StockActivity.this, ChooseDateStock.class);
                startActivityForResult(intent, LISTDATE);
                break;
            case R.id.et_product:
                Intent intentListProduct = new Intent(StockActivity.this, ListProductStockActivity.class);
                startActivityForResult(intentListProduct, PRODUCTLIST);
                break;
            case R.id.et_company:
                Intent intentListCompany = new Intent(StockActivity.this, ListCompanyStockActivity.class);
                startActivityForResult(intentListCompany, COMPANYLIST);
                break;
            case R.id.btn_search:
                selectDataStock();
                break;

        }
    }

    public void selectDataStock() {
        database.openTransaction();

        if (etDate != null && !etDate.getText().toString().equals("")) {
            String sqldb_query = "select * FROM Table_Stock WHERE substr(CREATEDDATE,1,10) = '" + etDate.getText().toString().substring(0, 10) + "' ";
            if (!companyCode.equals("ALL")) {
                sqldb_query += " AND COMPANY = '" + companyCode + "' ";
            }
            if (!productCode.equals("ALL")) {
                sqldb_query += " AND MATERIAL = '" + productCode + "' ";
            }
            sqldb_query += "Order by CREATEDDATE";
            List<Object> listObject = database.getListDataRawQuery(sqldb_query, SalesStockModel.TABLE_NAME, null);
            database.closeTransaction();

            Log.e("DATASTOCK", "selectDataStock: " + listObject.size());

            if (listObject.size() > 0) {
                tableStock.setVisibility(View.VISIBLE);
                loadData(listObject);
            } else {
                tableStock.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Data tidak ditemukan", Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(getApplicationContext(), "Silahkan Pilih Tanggal", Toast.LENGTH_SHORT).show();
        }
    }

    public void getLastUpdate() {
        database.openTransaction();

        String sqldb_query = "select * FROM Table_Stock order by CREATEDDATE DESC LIMIT 1";
        List<Object> listObject = database.getListDataRawQuery(sqldb_query, SalesStockModel.TABLE_NAME, null);
        database.closeTransaction();

        for (Object o : listObject) {
            tvLastUpdate.setText("Latest update per " + ((SalesStockModel) o).getCreateddate().replace("T", " "));
        }

    }

//    private void setUpSpinnerProduct(final List<Object> list) {
//        List<String> product = new ArrayList<String>();
//        product.add("ALL");
//        for (int i = 0; i < list.size(); i++) {
//
//            GroupProductModel groupProductModel = (GroupProductModel) list.get(i);
//            product.add(groupProductModel.getGroupproduct());
//        }
//        ArrayAdapter<String> dtProduct = new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner, product);
//        dtProduct.setDropDownViewResource(R.layout.spinner_dropdown);
//        // attaching data adapter to spinner
//        spProduct.setAdapter(dtProduct);
//        spProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parentView1, View selectedItemView, int position, long id) {
//                // your code here
//                String dataProduct = parentView1.getItemAtPosition(position).toString();
//                if (dataProduct.equalsIgnoreCase("ALL")) {
//                    productCode = "ALL";
//                } else {
//                    productCode = ((GroupProductModel) list.get(position - 1)).getMaterial();
//                }
////                Toast.makeText(getApplicationContext(),productCode,Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parentView) {
//                // your code here
//            }
//        });
//    }

//    private void DataSourceProduct() {
//        database.openTransaction();
//        List<GroupProductModel> listTemp = new ArrayList<GroupProductModel>();
//        List<Object> listObject;
//
//        String sqldb_query = "select  a.groupproduct,a.subgroup,a.material,a.materialdescription from T_GROUPPRODUCT a INNER JOIN Table_Stock b on a.material = b.material group by groupproduct";
//        listObject = database.getListDataRawQuery(sqldb_query, GroupProductModel.TABLE_NAME, null);
//        database.closeTransaction();
//        if (listObject.size() > 0) {
//            for (int i = 0; i < listObject.size(); i++) {
//                GroupProductModel blk = (GroupProductModel) listObject.get(i);
//                listTemp.add(blk);
//            }
//
//            setUpSpinnerProduct(listObject);
//        } else {
//            //    Toast.makeText(getActivity(),"Data TIdak ada",Toast.LENGTH_SHORT).show();
//        }
//    }

//    private void setUpSpinnerCompany(final List<Object> list) {
//        List<String> company = new ArrayList<String>();
//        company.add("ALL");
//        for (int i = 0; i < list.size(); i++) {
//
//            GroupCompanyModel groupProductModel = (GroupCompanyModel) list.get(i);
//            company.add(groupProductModel.getDescription());
//        }
//        ArrayAdapter<String> dtCOmpany = new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner, company);
//        dtCOmpany.setDropDownViewResource(R.layout.spinner_dropdown);
//        // attaching data adapter to spinner
//        spCompany.setAdapter(dtCOmpany);
//        spCompany.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parentView1, View selectedItemView, int position, long id) {
//                // your code here
//                String dataCompany = parentView1.getItemAtPosition(position).toString();
//                if (dataCompany.equalsIgnoreCase("ALL")) {
//                    companyCode = "ALL";
//                } else {
//                    companyCode = ((GroupCompanyModel) list.get(position - 1)).getCompanycode();
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parentView) {
//                // your code here
//            }
//
//        });
//    }

//    private void DataSourceCompany() {
//        database.openTransaction();
//        List<GroupCompanyModel> listTemp = new ArrayList<GroupCompanyModel>();
//        List<Object> listObject;
//
//        String sqldb_query = "SELECT groupCompany,companyCode,description " +
//                "FROM T_GROUPCOMPANY a INNER JOIN Table_Stock b on a.companyCode = b.company group by groupCompany order by groupCompany desc ";
//        listObject = database.getListDataRawQuery(sqldb_query, GroupCompanyModel.TABLE_NAME, null);
//        database.closeTransaction();
//        if (listObject.size() > 0) {
//            for (int i = 0; i < listObject.size(); i++) {
//                GroupCompanyModel blk = (GroupCompanyModel) listObject.get(i);
//                listTemp.add(blk);
//            }
//
//            setUpSpinnerCompany(listObject);
//        } else {
//            //    Toast.makeText(getActivity(),"Data TIdak ada",Toast.LENGTH_SHORT).show();
//        }
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LISTDATE) {
            if (resultCode == RESULT_OK) {
                String TANGGAL = data.getStringExtra("TANGGAL");
                if (TANGGAL == null) {
                    etDate.setText("From Date");
                } else {
                    String XFROMDATE = data.getStringExtra("TANGGAL").substring(0, 10);
                    etDate.setText(XFROMDATE);
                }
            }
        } else if (requestCode == PRODUCTLIST) {
            if (resultCode == RESULT_OK) {
                etProduct.setText(data.getStringExtra("DESC"));
                productCode = data.getStringExtra("CODE");
            }
        } else if (requestCode == COMPANYLIST) {
            if (resultCode == RESULT_OK) {
                etCompany.setText(data.getStringExtra("DESC"));
                companyCode = data.getStringExtra("CODE");
            }
        }
    }

    private boolean deleteDataStock() {
        boolean retVal = false;

        try {
            database.openTransaction();
            database.deleteDataFromTable(SalesStockModel.TABLE_NAME, null, null);
            database.commitTransaction();
            database.closeTransaction();
            retVal = true;
        } catch (Exception e) {
            retVal = false;
        }

        return retVal;
    }

    private void getDataStock() {
        if (!pDialog.isShowing()) {
            pDialog.setMessage("Loading");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        GetStock().enqueue(new Callback<StockResponse>() {
            @Override
            public void onResponse(Call<StockResponse> call, Response<StockResponse> response) {
                if (response.code() == 200) {
                    StockResponse stockResponse = StockCurrent(response);
                    salesStockRespons = stockResponse.getSalesStock();
                    setupInsertStock();

                } else {
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Response Gagal", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StockResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private StockResponse StockCurrent(Response<StockResponse> response) {
        StockResponse priceResponse = response.body();
        return priceResponse;
    }

    private Call<StockResponse> GetStock() {
        return apiServices.getDataStockCurrent();
    }

    public void setupInsertStock() {
        database.openTransaction();
        for (SalesStockModel s : salesStockRespons) {
            ContentValues values = new ContentValues();
            values.put(SalesStockModel.XML_ID, s.getId());
            values.put(SalesStockModel.XML_COMPANY, s.getCompany());
            values.put(SalesStockModel.XML_AREA, s.getArea());
            values.put(SalesStockModel.XML_PLANT, s.getPlant());
            values.put(SalesStockModel.XML_MATERIAL, s.getMaterial());
            values.put(SalesStockModel.XML_COMPANYNAME, s.getCompanyname());
            values.put(SalesStockModel.XML_PLANTNAME, s.getPlantname());
            values.put(SalesStockModel.XML_BEGINSTOCK, s.getBegiNSTOCK());
            values.put(SalesStockModel.XML_RECEIVESTOCK, s.getReceivESTOCK());
            values.put(SalesStockModel.XML_DELVSTOCK, s.getDelVSTOCK());
            values.put(SalesStockModel.XML_INTRANSIT, s.getINTRANSIT());
            values.put(SalesStockModel.XML_OTHERSTOCK, s.getOtheRSTOCK());
            values.put(SalesStockModel.XML_ENDINGSTOCK, s.getEndinGSTOCK());
            values.put(SalesStockModel.XML_PABRIK, s.getPabrik());
            values.put(SalesStockModel.XML_OUTSTANDING, s.getOutstanding());
            values.put(SalesStockModel.XML_CREATEDDATE, s.getCreateddate());
            database.insertDataSQL(SalesStockModel.TABLE_NAME, values);
        }
        database.commitTransaction();
        database.closeTransaction();

        if (pDialog.isShowing()) {
            pDialog.dismiss();
        }


        getLastUpdate();

//        DataSourceProduct();
//        DataSourceCompany();
    }

    private void loadData(List<Object> objectList) {
        int count = tableStock.getChildCount();
        for (int i = 1; i < count; i++) {
            View child = tableStock.getChildAt(i);
            if (child instanceof TableRow) ((ViewGroup) child).removeAllViews();
        }

        TableRow.LayoutParams lpTv = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT);
        lpTv.setMargins(0, 0, 0, 1);

        double totalStock = 0;
        double totalOut = 0;

        for (Object o : objectList) {
            SalesStockModel s = (SalesStockModel) o;
            totalStock += Double.parseDouble(s.getEndinGSTOCK());
            totalOut += Double.parseDouble(s.getOutstanding());

            TableRow row = new TableRow(this);

            LinearLayout layArea = new LinearLayout(this);
            layArea.setOrientation(LinearLayout.VERTICAL);
            layArea.setGravity(Gravity.CENTER);
            layArea.setBackgroundColor(getResources().getColor(R.color.colorWhiteGray));

            TextView tvArea = new TextView(this);
            tvArea.setLayoutParams(lpTv);
            tvArea.setPadding(8, 8, 8, 8);
            tvArea.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvArea.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvArea.setText(s.getArea());
            tvArea.setGravity(Gravity.CENTER);
            layArea.addView(tvArea);

            LinearLayout layPlant = new LinearLayout(this);
            layPlant.setOrientation(LinearLayout.VERTICAL);
            layPlant.setGravity(Gravity.CENTER);
            layPlant.setBackgroundColor(getResources().getColor(R.color.colorWhiteGray));

            TextView tvPlant = new TextView(this);
            tvPlant.setLayoutParams(lpTv);
            tvPlant.setPadding(8, 8, 8, 8);
            tvPlant.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvPlant.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvPlant.setText(s.getPlantname());
            tvPlant.setGravity(Gravity.CENTER);
            layPlant.addView(tvPlant);

            LinearLayout layStock = new LinearLayout(this);
            layStock.setOrientation(LinearLayout.VERTICAL);
            layStock.setGravity(Gravity.RIGHT);
            layStock.setBackgroundColor(getResources().getColor(R.color.colorWhiteGray));

            TextView tvStock = new TextView(this);
            tvStock.setLayoutParams(lpTv);
            tvStock.setPadding(8, 8, 8, 8);
            tvStock.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvStock.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvStock.setText(thousandFormat(s.getEndinGSTOCK()));
            tvStock.setGravity(Gravity.RIGHT);
            layStock.addView(tvStock);


            LinearLayout layOutStanding = new LinearLayout(this);
            layOutStanding.setOrientation(LinearLayout.VERTICAL);
            layOutStanding.setGravity(Gravity.RIGHT);
            layOutStanding.setBackgroundColor(getResources().getColor(R.color.colorWhiteGray));

            TextView tvOutStanding = new TextView(this);
            tvOutStanding.setLayoutParams(lpTv);
            tvOutStanding.setPadding(8, 8, 8, 8);
            tvOutStanding.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvOutStanding.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvOutStanding.setText(thousandFormat(s.getOutstanding()));
            tvOutStanding.setGravity(Gravity.RIGHT);
            layOutStanding.addView(tvOutStanding);


            row.addView(layArea);
            row.addView(layPlant);
            row.addView(layStock);
            row.addView(layOutStanding);
            tableStock.addView(row);
        }


        TableRow rowTotal = new TableRow(this);
        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
        rowTotal.setLayoutParams(lp);

        LinearLayout layTotalStock = new LinearLayout(this);
        layTotalStock.setOrientation(LinearLayout.VERTICAL);
        layTotalStock.setGravity(Gravity.RIGHT);
        layTotalStock.setBackgroundColor(getResources().getColor(R.color.colorWhiteGray));
        layTotalStock.setLayoutParams(new TableRow.LayoutParams(2));

        TextView tvTotalStock = new TextView(this);
        tvTotalStock.setLayoutParams(lpTv);
        tvTotalStock.setPadding(8, 8, 8, 8);
        tvTotalStock.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        tvTotalStock.setTextColor(getResources().getColor(R.color.colorBlackGray));
        tvTotalStock.setText(thousandFormat(String.valueOf(totalStock)));
        tvTotalStock.setGravity(Gravity.RIGHT);
        layTotalStock.addView(tvTotalStock);

        LinearLayout layTotalOutStanding = new LinearLayout(this);
        layTotalOutStanding.setOrientation(LinearLayout.VERTICAL);
        layTotalOutStanding.setGravity(Gravity.RIGHT);
        layTotalOutStanding.setBackgroundColor(getResources().getColor(R.color.colorWhiteGray));
        layTotalOutStanding.setLayoutParams(new TableRow.LayoutParams(3));

        TextView tvTotalOutStanding = new TextView(this);
        tvTotalOutStanding.setLayoutParams(lpTv);
        tvTotalOutStanding.setPadding(8, 8, 8, 8);
        tvTotalOutStanding.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        tvTotalOutStanding.setTextColor(getResources().getColor(R.color.colorBlackGray));
        tvTotalOutStanding.setText(thousandFormat(String.valueOf(totalOut)));
        tvTotalOutStanding.setGravity(Gravity.RIGHT);
        layTotalOutStanding.addView(tvTotalOutStanding);

        rowTotal.addView(layTotalStock);
        rowTotal.addView(layTotalOutStanding);
        tableStock.addView(rowTotal);

    }

    private String thousandFormat(String number) {
        NumberFormat PriceFormat = new DecimalFormat("#,###,###,###,###,###");
        double formatdouble = Double.parseDouble(number) / 1000;
        return PriceFormat.format(formatdouble);
    }


    @Override
    public void onDayClick(String date) {

    }
}
