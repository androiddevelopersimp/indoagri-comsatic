package com.indoagri.comsatic.stock;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.indoagri.comsatic.R;
import com.indoagri.comsatic.Retrofit.Model.GroupCompanyModel;
import com.indoagri.comsatic.Retrofit.Model.GroupProductModel;

import java.util.ArrayList;
import java.util.List;

public class ListCompanyAdapter extends ArrayAdapter<GroupCompanyModel> {

    List<GroupCompanyModel> productModelList,lst_temp;
    Context context;
    int layout;

    public ListCompanyAdapter(Context context, int layout, List<GroupCompanyModel> uadEstateModelList) {
        super(context, layout, uadEstateModelList);
        this.context=context;
        this.layout=layout;
        this.productModelList=uadEstateModelList;
    }


    @Override
    public View getView(int position, View v, ViewGroup parent) {
        GroupCompanyModel student=productModelList.get(position);
        StudentHolder holder;
        if(v==null){
            LayoutInflater vi=((Activity)context).getLayoutInflater();
            //  v=vi.inflate(layout, parent,false);
            v = vi.inflate(layout, null);
            holder=new StudentHolder();
            holder.imageView=(ImageView) v.findViewById(R.id.imgClick);
            holder.code=(TextView) v.findViewById(R.id.txtestateCode);
            holder.name=(TextView) v.findViewById(R.id.txtestateName);
            v.setTag(holder);
        }
        else{
            holder=(StudentHolder) v.getTag();
            holder.imageView=(ImageView) v.findViewById(R.id.imgClick);
            holder.code=(TextView) v.findViewById(R.id.txtestateCode);
            holder.name=(TextView) v.findViewById(R.id.txtestateName);
        }
        holder.code.setText(student.getCompanycode()+" | ");
        holder.name.setText(student.getDescription());
        return v;
    }

    static class StudentHolder{
        ImageView imageView;
        TextView code;
        TextView name;
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    productModelList = lst_temp;
                } else {
                    if(lst_temp == null){
                        lst_temp = new ArrayList<GroupCompanyModel>(productModelList);
                    }
                    List<GroupCompanyModel> filteredList = new ArrayList<>();
                    for (GroupCompanyModel row : lst_temp) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getCompanycode().toLowerCase().contains(charString.toLowerCase()) || row.getDescription().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    productModelList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = productModelList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                productModelList = (ArrayList<GroupCompanyModel>) filterResults.values;

                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getCount() {
        return productModelList.size();
    }


    @Override
    public long getItemId(int pos) {
        return pos;
    }

}