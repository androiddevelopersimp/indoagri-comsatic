package com.indoagri.comsatic.stock;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.indoagri.comsatic.R;
import com.indoagri.comsatic.Retrofit.DB.DatabaseQuery;
import com.indoagri.comsatic.Retrofit.Model.GroupCompanyModel;
import com.indoagri.comsatic.Retrofit.Model.GroupProductModel;
import com.indoagri.comsatic.Retrofit.SharePreference;
import com.indoagri.comsatic.price.ListProductAdapter;

import java.util.ArrayList;
import java.util.List;

public class ListCompanyStockActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, TextWatcher {

    Bundle extras;
    String extrasString;
    Toolbar toolbar;
    TextView mTextToolbar, txtSetAll;
    ListView listView;
    EditText etSearch;
    DatabaseQuery query;
    SharePreference sharedPreferences;
    List<GroupCompanyModel> productModelList;
    ListCompanyAdapter listSubProductAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_sub_product);
        setUpToolbar();
        sharedPreferences = new SharePreference(ListCompanyStockActivity.this);
        query = new DatabaseQuery(ListCompanyStockActivity.this);
        listView = (ListView) findViewById(R.id.listView);
        etSearch = findViewById(R.id.et_search);
        etSearch.setVisibility(View.VISIBLE);
        txtSetAll = (findViewById(R.id.txt_setAll));
        txtSetAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String subproduct = "ALL";
                Intent intent = new Intent();
                intent.putExtra("CODE", subproduct);
                intent.putExtra("DESC", subproduct);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        extras = getIntent().getExtras();
        if (extras != null) {
            extrasString = extras.getString("Group");
            init();
            // and get whatever type user account id is
            //Toast.makeText(this, extrasString+" || "+new SharePreference(ListSubProduct.this).isFormProduct(), Toast.LENGTH_SHORT).show();
        } else {
            init();
        }

    }

    private void setUpToolbar() {
        toolbar = (findViewById(R.id.toolbar));
        mTextToolbar = (findViewById(R.id.txt_Toolbar));
        mTextToolbar.setText("List Material");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    void init() {
        productModelList = new ArrayList<GroupCompanyModel>();
        etSearch.addTextChangedListener(this);
        listView.setOnItemClickListener(this);
        listSubProductAdapter = new ListCompanyAdapter(this, R.layout.item_listcontent, productModelList);
        listView.setAdapter(listSubProductAdapter);
        listView.setScrollingCacheEnabled(false);
        setHalaman();

    }

    private void setHalaman() {
        etSearch.addTextChangedListener(this);
        List<Object> listObject;
        query.openTransaction();
        String sqldb_query = "SELECT groupCompany,companyCode,description " +
                "FROM T_GROUPCOMPANY a INNER JOIN Table_Stock b on a.companyCode = b.company  group by companyCode order by companyCode desc ";
        listObject = query.getListDataRawQuery(sqldb_query, GroupCompanyModel.TABLE_NAME, null);
//        String sqldb_query = "select groupCompany,companyCode,description from T_GROUPCOMPANY where groupCompany=?";
//        String isAD = sharedPreferences.isFormGroupCompany();
//        String[] a = new String[1];
//        a[0] = isAD;
//        Log.e(ContentValues.TAG, "setHalaman: " + isAD);
//        listObject = query.getListDataRawQuery(sqldb_query, GroupCompanyModel.TABLE_NAME, a);
        query.closeTransaction();
        if (listObject.size() > 0) {
            for (int i = 0; i < listObject.size(); i++) {
                GroupCompanyModel hasil = (GroupCompanyModel) listObject.get(i);
                productModelList.add(hasil);
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        listSubProductAdapter.getFilter().filter(s);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
        GroupCompanyModel subProductAdapterItem = (GroupCompanyModel) listSubProductAdapter.getItem(i);
        String CODE = subProductAdapterItem.getCompanycode();
        String DESC = subProductAdapterItem.getDescription();
        Intent intent = new Intent();
        intent.putExtra("CODE", CODE);
        intent.putExtra("DESC", DESC);
        setResult(RESULT_OK, intent);
        finish();
    }
}
