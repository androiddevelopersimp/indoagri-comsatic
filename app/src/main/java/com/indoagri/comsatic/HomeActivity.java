package com.indoagri.comsatic;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.indoagri.comsatic.Retrofit.ApiServices;
import com.indoagri.comsatic.Retrofit.GroupCompanyResponse;
import com.indoagri.comsatic.Retrofit.GroupProductResponse;
import com.indoagri.comsatic.Retrofit.Model.GroupCompanyModel;
import com.indoagri.comsatic.Retrofit.NetClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {
    private static final String TAGCOMPANY = "GET COMPANY GROUP";
    ProgressBar progressBar;
    ApiServices apiServices;
    GroupCompanyResponse groupCompanyResponse;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        apiServices = NetClient.DataProduction().create(ApiServices.class);
        GetData();
    }



    private void GetData(){
        if (progressBar.getVisibility() == View.GONE) {
            progressBar.setVisibility(View.VISIBLE);
        }
        Log.d(TAGCOMPANY, "LOADAGE: ");

        GetFleets().enqueue(new Callback<GroupCompanyResponse>() {
            @Override
            public void onResponse(Call<GroupCompanyResponse> call, Response<GroupCompanyResponse> response) {
                // Got data. Send it to adapter
                progressBar.setVisibility(View.GONE);
                if(response.code()==200){
                    groupCompanyResponse = fetchResults(response);
                    List<GroupCompanyModel> groupCompanyModels = groupCompanyResponse.getCompanies();
                    for(int i=0;i<groupCompanyModels.size();i++){
                        GroupCompanyModel groupCompanyModel = groupCompanyModels.get(i);
                        Toast.makeText(HomeActivity.this,"Data "+groupCompanyModel.getDescription(), Toast.LENGTH_SHORT).show();
                    }

                }else{
                    Toast.makeText(getApplicationContext(),"Toast Gagal", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GroupCompanyResponse> call, Throwable t) {
                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
                // TODO: 08/11/16 handle failure
                Toast.makeText(getApplicationContext(),t.toString(),Toast.LENGTH_SHORT).show();
            }
        });

    }
    private GroupCompanyResponse fetchResults(Response<GroupCompanyResponse> response) {
        GroupCompanyResponse fleetsResponse = response.body();
        return fleetsResponse;
    }

    private Call<GroupCompanyResponse> GetFleets() {
        return apiServices.getCompany();
    }
}
