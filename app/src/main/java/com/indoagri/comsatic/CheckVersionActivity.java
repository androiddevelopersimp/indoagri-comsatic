package com.indoagri.comsatic;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.indoagri.comsatic.Common.DeviceUtils;
import com.indoagri.comsatic.Retrofit.ApiServices;
import com.indoagri.comsatic.Retrofit.AppDetailResponse;
import com.indoagri.comsatic.Retrofit.DB.DatabaseQuery;
import com.indoagri.comsatic.Retrofit.Model.AppDetailModel;
import com.indoagri.comsatic.Retrofit.Model.UserModel;
import com.indoagri.comsatic.Retrofit.NetClient;
import com.indoagri.comsatic.Retrofit.SharePreference;

public class CheckVersionActivity extends AppCompatActivity {
    private static final String TAGMAIN = "MAIN CHECK VERSION";
    private String SFTPHOST, SFTPUSER, SFTPPASS, SFTPWORKINGDIR;
    private int SFTPPORT;

    private boolean statusUpdate;
    String VersionNow;
    AppDetailModel appDetailModel;
    AppDetailResponse appDetailResponse;
    ApiServices apiServices;
    DatabaseQuery query;
    String AD;
    String AppsName;
    int VNow = 0;
    int VServer = 0;
    String APKNAAME;
    File Path;
    Context context;
    ProgressDialog pDialog;
    String useremail,userpassword,userad,userdomain;
    SharePreference sharePreference;
    UserModel userModel;
    ProgressBar progressBarDownload;
    boolean isShowing;
    TextView txtProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_version);
        apiServices = NetClient.DataProduction().create(ApiServices.class);
        Path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        query = new DatabaseQuery(CheckVersionActivity.this);
        pDialog = new ProgressDialog(CheckVersionActivity.this);
        isShowing = pDialog.isShowing();
        sharePreference = new SharePreference(CheckVersionActivity.this);
        AD = new String(new SharePreference(CheckVersionActivity.this).isFormLoginUsername());
        AppsName = new String(DeviceUtils.getAppName(CheckVersionActivity.this));
        progressBarDownload = findViewById(R.id.progresdownload);
        txtProgress = findViewById(R.id.txtPercent);
        txtProgress.setText("Connection Initialitation");
        isShowing = pDialog.isShowing();
        if(haveNetworkConnection()){
            Load();
        }else{
            showDialogConnection();
        }
    }
    private void Load(){
        String Message = getResources().getString(R.string.msg_check_update_version);
        if (!isShowing)
            pDialog.setMessage(Message);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setCancelable(false);
        pDialog.show();
        TextView txtUpdate = (findViewById(R.id.txtUpdate));
        txtUpdate.setText(DeviceUtils.getAppVersion(CheckVersionActivity.this));
        txtUpdate.setVisibility(View.GONE);
        VersionNow = new String(DeviceUtils.getAppVersion(CheckVersionActivity.this));

        VNow = Integer.valueOf(VersionNow.replaceAll("[\\s.]", ""));
        pDialog.dismiss();
        getAppDetail();
    }
    private void Process(){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if(!isFinishing()){
                    Login();
                }
            }
        }, 3000);
    }
    // AppDetail //
    private void getAppDetail(){
        String Message = getResources().getString(R.string.check_version);
        if (!isShowing)
            pDialog.setMessage(Message);
        pDialog.setCancelable(false);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.show();
        getApp().enqueue(new Callback<AppDetailResponse>() {
            @Override
            public void onResponse(Call<AppDetailResponse> call, Response<AppDetailResponse> response) {
                // Got data. Send it to adapter
                pDialog.dismiss();
                if(response.code()==200){
                    Log.e("RESPONSE","SUKSES");
                    appDetailResponse = getAPpDetail(response);
                    appDetailModel = appDetailResponse.getAPP();
                    SFTPUSER = appDetailResponse.getAPP().getUser();
                    SFTPPASS = appDetailResponse.getAPP().getPass();
                    String[] url = appDetailResponse.getAPP().getAPKURL().split("-");
                    SFTPHOST = url[0];
                    SFTPPORT = Integer.valueOf(url[1]);
                    SFTPWORKINGDIR = url[2];
                    Bandingkan(appDetailModel);
                }else{
                    Log.e("RESPONSE","GAGAL");
                    showDialogNotPermitted();
                }
            }
            @Override
            public void onFailure(Call<AppDetailResponse> call, Throwable t) {
                t.printStackTrace();
                pDialog.dismiss();
                Log.e("RESPONSE","GAGAL");
                // TODO: 08/11/16 handle failure
                showResetDialogConnection();
            }
        });
    }
    private AppDetailResponse getAPpDetail(Response<AppDetailResponse> response) {
        AppDetailResponse detailResponse = response.body();
        return detailResponse;
    }
    private Call<AppDetailResponse> getApp() {
        return apiServices.getAppDetail(AD,AppsName);
    }
    private void Bandingkan(AppDetailModel appDetailModel){
        VServer = Integer.valueOf(appDetailModel.getVERSIONNAME().replaceAll("[\\s.]", ""));
        APKNAAME = new String(appDetailModel.getAPPNAME()+"-"+appDetailModel.getVERSIONNAME()+".apk");
        if(VServer>VNow){
            Log.d("Banding"," Versi Ada yang terbaru");
            showDialogVersion(appDetailModel);
        }if(VServer<VNow){
            Log.d("Banding"," Data dalam server tidak valid Server = "+appDetailModel.getVERSIONNAME()+" || Android = "+VersionNow);
            Process();
        }
        if(VServer==VNow){
            Log.d("Banding"," Versi Sama");
            Process();
        }
    }
    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
    private void showDialogConnection() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Connect to wifi or quit")
                .setCancelable(false)
                .setPositiveButton("Connect to WIFI", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                    }
                })
                .setNegativeButton("Quit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
    private void showResetDialogConnection() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Connection Error")
                .setCancelable(false)
                .setNegativeButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent a = new Intent(CheckVersionActivity.this, SplashScreenActivity.class);
                        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        finish();
                        startActivity(a);
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
    private void showDialogNotPermitted() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Permission Denied")
                .setCancelable(false)
                .setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
    private void showDialogVersion(AppDetailModel appDetailModel) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Get Ready News Version "+appDetailModel.APPNAME)
                .setCancelable(false)
                .setPositiveButton("Download & Update", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //finish();
                        if (Build.VERSION.SDK_INT >= 23)
                        {
                            if (checkPermission())
                            {
                                new DownloadTask(CheckVersionActivity.this,APKNAAME,Path,getResources().getString(R.string.msg_update_version)).execute();
                                dialog.cancel();
                            } else {
                                requestPermission(); // Code for permission
                                dialog.cancel();
                            }
                        }
                        else
                        {
                            new DownloadTask(CheckVersionActivity.this,APKNAAME,Path,getResources().getString(R.string.msg_update_version)).execute();
                            dialog.cancel();
                        }

                    }
                }).show();
    }
    public class DownloadTask extends AsyncTask<String, Integer, String> {
        private Context mContext;
        private PowerManager.WakeLock mWakeLock;
        private File mTargetFile;
        private String apkName;
        public DownloadTask(Context context,String APkName,File targetFile,String dialogMessage) {
            this.mContext = context;
            this.mTargetFile = targetFile;
            this.apkName = APkName;
            final DownloadTask me = this;
            String Message = getResources().getString(R.string.msg_update_version);
            progressBarDownload.setVisibility(View.VISIBLE);
            progressBarDownload.setProgress(0);

        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            PowerManager pm = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    getClass().getName());
            mWakeLock.acquire();

        }
        @Override
        protected String doInBackground(String... sUrl) {
            // Background Code
            Session session = null;
            Channel channel = null;
            ChannelSftp channelSftp = null;
            int count;
            OutputStream output = null;
            BufferedInputStream input = null;
            try {
                String apKName = apkName;
                JSch jsch = new JSch();
                session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
                session.setPassword(SFTPPASS);
                session.setConfig("StrictHostKeyChecking", "no");
                session.setTimeout(30000);
                session.connect();
                channel = session.openChannel("sftp");
                channel.connect();
                channelSftp = (ChannelSftp) channel;
                channelSftp.cd(SFTPWORKINGDIR);
                int filesize;
                input = new BufferedInputStream(channelSftp.get(SFTPWORKINGDIR+apKName));

                filesize = (int) (long) channelSftp.lstat(SFTPWORKINGDIR + apKName).getSize();
                File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                File file = new File(path, apKName);
                if (file.exists())
                {
                    file.delete();
                }
                file.createNewFile();
                output = new FileOutputStream(file);
                byte[] buffer = new byte[4096];
                float total = 0;
                while ((count = input.read(buffer)) != -1) {
                    if (isCancelled()) {
                        Log.i("DownloadTask","Cancelled");
                        input.close();
                        return null;
                    }
                    total += count;
                    // publishing the progress....
                    if (filesize > 0) // only if total length is known
                        publishProgress((int) (total * 100 / filesize));
                    output.write(buffer, 0, count);
                }
            } catch (Exception e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (channel != null)
                    channel.disconnect();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            txtProgress.setText("Please Wait "+String.valueOf(progress[0])+" % ");
            progressBarDownload.setProgress(progress[0]);
            // if we get here, length is known, now set indeterminate to false
        }
        @Override
        protected void onPostExecute(String result) {
            Log.i("DownloadTask", "Work Done! PostExecute");
            mWakeLock.release();
            if (result != null) {
                showResetDialogConnection();
            }else {
                finish();
                if(isFinishing()){
                    InstallAPK(mTargetFile, apkName);
                }

            }
        }
    }
    private void InstallAPK(File TargetFile,String APKNAME){
        File file = new File(TargetFile, APKNAME);
        Intent intent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Uri apkUri = FileProvider.getUriForFile(CheckVersionActivity.this, BuildConfig.APPLICATION_ID + ".fileprovider", file);
            intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
            intent.setData(apkUri);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        } else {
            Uri apkUri = Uri.fromFile(file);
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(apkUri,
                    "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        sharePreference.setKonfirmationUpdate(false);
        startActivity(intent);
    }
    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(CheckVersionActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }
    private static final int PERMISSION_REQUEST_CODE = 1;
    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(CheckVersionActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(CheckVersionActivity.this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(CheckVersionActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("value", "Permission Granted, Now you can use local drive .");
                    new DownloadTask(getApplicationContext(),APKNAAME,Path,getResources().getString(R.string.msg_update_version)).execute();
                } else {
                    Log.e("value", "Permission Denied, You cannot use local drive .");
                }
                break;
        }
    }
    private void Login(){
        userad = sharePreference.isUserName().toLowerCase();
        useremail = sharePreference.isEmail();
        userdomain = sharePreference.isDomain();
        userpassword = sharePreference.isPassword();
        setInisialisasi(useremail,userpassword,userad,userdomain);
    }

    private void setInisialisasi(String useremail,String userpassword,String userad,String userdomain){
        query.openTransaction();
        String sqldb_query = "SELECT * " +
                "FROM Table_User where userName = ? ";
        String[] a = new String[1];
        a[0] = userad;
        Object object = query.getDataFirstRaw(sqldb_query, UserModel.TABLE_NAME,a);
        userModel = (UserModel)object;
        //  Toast.makeText(MainActivity.this, hasil.ESTATE, Toast.LENGTH_SHORT).show();
        query.closeTransaction();
        if(userModel!=null){
            if(query.COUNT(UserModel.TABLE_NAME,"Select * from "+UserModel.TABLE_NAME)==0){
                query.INSERTQUERY(UserModel.TABLE_NAME,"INSERT INTO "+UserModel.TABLE_NAME+" ( "+
                        UserModel.XML_USERID+","+
                        UserModel.XML_USERNAME+","+
                        UserModel.XML_SURNAME+","+
                        UserModel.XML_EMAIL+","+
                        UserModel.XML_LDAP+","+
                        UserModel.XML_DOMAIN+","+
                        UserModel.XML_USERDESCRIPTION+","+
                        UserModel.XML_COMPANY+","+
                        UserModel.XML_PROVINCE+","+
                        UserModel.XML_DEPARTMENT+","+
                        UserModel.XML_EMPLOYEEID+","+
                        UserModel.XML_USERFULLNAME+","+
                        UserModel.XML_USERPHONE+","+
                        UserModel.XML_USERPASSWORD+","+
                        UserModel.XML_USERKEY+" ) VALUES ( "+
                        "'"+userModel.getUSERID()+"',"+
                        "'"+userModel.getUSERNAME().toLowerCase()+"',"+
                        "'"+userModel.getSURNAME()+"',"+
                        "'"+userModel.getEMAIL()+"',"+
                        "'"+userModel.getLDAP()+"',"+
                        "'"+userModel.getDOMAIN()+"',"+
                        "'"+userModel.getDESCRIPTION()+"',"+
                        "'"+userModel.getCOMPANY()+"',"+
                        "'"+userModel.getPROVINCE()+"',"+
                        "'"+userModel.getDEPARTMENT()+"',"+
                        "'"+userModel.getEMPLOYEEID()+"',"+
                        "'"+userModel.getFULLNAME()+"',"+
                        "'"+userModel.getPHONE()+"',"+
                        "'"+userModel.getPASSWORD()+"',"+
                        "'"+userModel.getKEY()+"')");
            }
            else{
                if(query.DELETEEXISTING(UserModel.TABLE_NAME,"DELETE FROM "+UserModel.TABLE_NAME)){
                    query.INSERTQUERY(UserModel.TABLE_NAME,"INSERT INTO "+UserModel.TABLE_NAME+" ( "+
                            UserModel.XML_USERID+","+
                            UserModel.XML_USERNAME+","+
                            UserModel.XML_SURNAME+","+
                            UserModel.XML_EMAIL+","+
                            UserModel.XML_LDAP+","+
                            UserModel.XML_DOMAIN+","+
                            UserModel.XML_USERDESCRIPTION+","+
                            UserModel.XML_COMPANY+","+
                            UserModel.XML_PROVINCE+","+
                            UserModel.XML_DEPARTMENT+","+
                            UserModel.XML_EMPLOYEEID+","+
                            UserModel.XML_USERFULLNAME+","+
                            UserModel.XML_USERPHONE+","+
                            UserModel.XML_USERPASSWORD+","+
                            UserModel.XML_USERKEY+" ) VALUES ( "+
                            "'"+userModel.getUSERID()+"',"+
                            "'"+userModel.getUSERNAME().toLowerCase()+"',"+
                            "'"+userModel.getSURNAME()+"',"+
                            "'"+userModel.getEMAIL()+"',"+
                            "'"+userModel.getLDAP()+"',"+
                            "'"+userModel.getDOMAIN()+"',"+
                            "'"+userModel.getDESCRIPTION()+"',"+
                            "'"+userModel.getCOMPANY()+"',"+
                            "'"+userModel.getPROVINCE()+"',"+
                            "'"+userModel.getDEPARTMENT()+"',"+
                            "'"+userModel.getEMPLOYEEID()+"',"+
                            "'"+userModel.getFULLNAME()+"',"+
                            "'"+userModel.getPHONE()+"',"+
                            "'"+userModel.getPASSWORD()+"',"+
                            "'"+userModel.getKEY()+"')");
                }
            }
            sharePreference.setKeyEmailuser(userModel.getEMAIL());
            sharePreference.setKeyUserdomain(userModel.getDOMAIN());
            sharePreference.setKeyPhoneuser(userModel.getPHONE());
            sharePreference.setKeyUsername(userModel.getUSERNAME().toLowerCase());
            sharePreference.setKeyUserfullname(userModel.getFULLNAME());
            sharePreference.setKeyUserdepartment(userModel.getDEPARTMENT());
            sharePreference.setFORMLoginEmail(userModel.getEMAIL());
            sharePreference.setFormLoginUsername(userModel.getUSERNAME().toLowerCase());
            sharePreference.setAggree(true);
            sharePreference.setLogin(true);
            Intent i = new Intent(CheckVersionActivity.this, MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(i);
        }else{
            sharePreference.setLogin(false);
            Intent logout = new Intent(CheckVersionActivity.this, SplashScreenActivity.class);
            logout.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            finish();
            startActivity(logout);
        }

    }
    @Override
    public void onBackPressed() {
        // super.onBackPressed(); commented this line in order to disable back press
        //Write your code here
        //    Toast.makeText(getApplicationContext(), "Back press disabled!", Toast.LENGTH_SHORT).show();
    }
    @Override
    public boolean onKeyDown(int key_code, KeyEvent key_event) {
        if (key_code== KeyEvent.KEYCODE_BACK) {
            super.onKeyDown(key_code, key_event);
            return true;
        }
        return false;
    }
}
