package com.indoagri.comsatic.Retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indoagri.comsatic.Retrofit.Model.PriceDataModel;

import java.util.List;

public class PriceResponse {
    @SerializedName("salesPrice")
    @Expose
    List<PriceDataModel> priceDataModelList;

    public List getPrice() {
        return priceDataModelList;
    }
}
