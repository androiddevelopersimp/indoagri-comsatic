package com.indoagri.comsatic.Retrofit.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GroupCompanyModel {

    public static String TABLE_NAME = "T_GROUPCOMPANY";
    public static String XML_id = "id";
    public static String XML_groupcompany = "groupCompany";
    public static String XML_companycode= "companyCode";
    public static String XML_description = "description";

    @SerializedName("groupCompany")
    @Expose
    String groupcompany;
    @SerializedName("companyCode")
    @Expose
    String companycode;
    @SerializedName("description")
    @Expose
    String description;

    public GroupCompanyModel() {
    }

    public GroupCompanyModel(String groupcompany, String companycode, String description) {
        this.groupcompany = groupcompany;
        this.companycode = companycode;
        this.description = description;
    }

    public String getGroupcompany() {
        return groupcompany;
    }

    public void setGroupcompany(String groupcompany) {
        this.groupcompany = groupcompany;
    }

    public String getCompanycode() {
        return companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
