package com.indoagri.comsatic.Retrofit;

import com.indoagri.comsatic.Common.Constants;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetClient {

    public static Retrofit retrofit;
    public static Retrofit retrofit2;

    public static Retrofit AuthProduction() {
        OkHttpClient client = UnsafeOkHttpClient.getUnsafeOkHttpClient();
//        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        OkHttpClient client = new OkHttpClient
//                .Builder()
//                .addInterceptor(interceptor)
//                .readTimeout(50, TimeUnit.SECONDS)
//                .connectTimeout(200, TimeUnit.SECONDS)
//                .build();
        //If condition to ensure we don't create multiple retrofit instances in a single application
        if (retrofit == null) {
            //Defining the Retrofit using Builder
            retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.PRODUCTIONAPI) //This is the only mandatory call on Builder object.
                    .addConverterFactory(GsonConverterFactory.create()) // Convertor library used to convert response into POJO
                    .client(client)
                    .build();
        }
        return retrofit;
    }

    public static Retrofit AuthDevelopment() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient
                .Builder()
                .addInterceptor(interceptor)
                .readTimeout(50, TimeUnit.SECONDS)
                .connectTimeout(200, TimeUnit.SECONDS)
                .build();
        //If condition to ensure we don't create multiple retrofit instances in a single application
        if (retrofit == null) {
            //Defining the Retrofit using Builder
            retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.DEVELOPMENTAPI) //This is the only mandatory call on Builder object.
                    .addConverterFactory(GsonConverterFactory.create()) // Convertor library used to convert response into POJO
                    .client(client)
                    .build();
        }
        return retrofit;
    }

    public static Retrofit DataProduction() {
        OkHttpClient client = UnsafeOkHttpClient.getUnsafeOkHttpClient();
//        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        OkHttpClient client = new OkHttpClient
//                .Builder()
//                .addInterceptor(interceptor)
//                .readTimeout(30, TimeUnit.SECONDS)
//                .connectTimeout(300, TimeUnit.SECONDS)
//                .build();
        //If condition to ensure we don't create multiple retrofit instances in a single application
        if (retrofit2 == null) {
            //Defining the Retrofit using Builder
            retrofit2 = new Retrofit.Builder()
                    .baseUrl(Constants.PRODUCTIONAPI) //This is the only mandatory call on Builder object.
                    .addConverterFactory(GsonConverterFactory.create()) // Convertor library used to convert response into POJO
                    .client(client)
                    .build();
        }

        return retrofit2;
    }

    public static Retrofit DataDevelopment() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient
                .Builder()
                .addInterceptor(interceptor)
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(300, TimeUnit.SECONDS)
                .build();
        //If condition to ensure we don't create multiple retrofit instances in a single application
        if (retrofit2 == null) {
            //Defining the Retrofit using Builder
            retrofit2 = new Retrofit.Builder()
                    .baseUrl(Constants.DEVELOPMENTAPI) //This is the only mandatory call on Builder object.
                    .addConverterFactory(GsonConverterFactory.create()) // Convertor library used to convert response into POJO
                    .client(client)
                    .build();
        }

        return retrofit2;
    }

}
