package com.indoagri.comsatic.Retrofit.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppDetailModel {
    public static final String TABLE_NAME = "Table_AppDetail";
    public static final String XML_APPID= "APPID";
    public static final String XML_APPNAME= "APPNAME";
    public static final String XML_APPDESC= "APPDESCRIPTION";
    public static final String XML_APPPACKAGE= "APPPACKAGE";
    public static final String XML_VERSIONCODE= "VERSIONCODE";
    public static final String XML_VERSIONNAME= "VERSIONNAME";
    public static final String XML_APKURL= "APKURL";
    public static final String XML_ASSETSICON= "ASSETSICON";
    public static final String XML_ASSETSICONURL= "ASSETSICONURL";
    public static final String XML_ACTIVE= "ACTIVE";
    public static final String XML_GROUPLEVEL= "GROUPLEVEL";
    public static final String XML_USERAD= "USERAD";

    @SerializedName("id")
    public String ID;
    @SerializedName("appName")
    public String APPNAME;
    @SerializedName("appDescription")
    public String APPDESCRIPTION;
    @SerializedName("appPackage")
    public String APPPACKAGE;
    @SerializedName("versionCode")
    public String VERSIONCODE;
    @SerializedName("versionName")
    public String VERSIONNAME;
    @SerializedName("apkUrl")
    public String APKURL;
    @SerializedName("assetsIcon")
    public String ASSETSICON;
    @SerializedName("assetsIconUrl")
    public String ASSETSICONURL;
    @SerializedName("active")
    public String ACTIVE;
    @SerializedName("groupLevel")
    public String GROUPLEVEL;
    @SerializedName("userAD")
    public String USERAD;
    @SerializedName("user")
    @Expose
    private String user;
    @SerializedName("pass")
    @Expose
    private String pass;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getAPPNAME() {
        return APPNAME;
    }

    public void setAPPNAME(String APPNAME) {
        this.APPNAME = APPNAME;
    }

    public String getAPPDESCRIPTION() {
        return APPDESCRIPTION;
    }

    public void setAPPDESCRIPTION(String APPDESCRIPTION) {
        this.APPDESCRIPTION = APPDESCRIPTION;
    }

    public String getAPPPACKAGE() {
        return APPPACKAGE;
    }

    public void setAPPPACKAGE(String APPPACKAGE) {
        this.APPPACKAGE = APPPACKAGE;
    }

    public String getVERSIONCODE() {
        return VERSIONCODE;
    }

    public void setVERSIONCODE(String VERSIONCODE) {
        this.VERSIONCODE = VERSIONCODE;
    }

    public String getVERSIONNAME() {
        return VERSIONNAME;
    }

    public void setVERSIONNAME(String VERSIONNAME) {
        this.VERSIONNAME = VERSIONNAME;
    }

    public String getAPKURL() {
        return APKURL;
    }

    public void setAPKURL(String APKURL) {
        this.APKURL = APKURL;
    }

    public String getASSETSICON() {
        return ASSETSICON;
    }

    public void setASSETSICON(String ASSETSICON) {
        this.ASSETSICON = ASSETSICON;
    }

    public String getASSETSICONURL() {
        return ASSETSICONURL;
    }

    public void setASSETSICONURL(String ASSETSICONURL) {
        this.ASSETSICONURL = ASSETSICONURL;
    }

    public String getACTIVE() {
        return ACTIVE;
    }

    public void setACTIVE(String ACTIVE) {
        this.ACTIVE = ACTIVE;
    }

    public String getGROUPLEVEL() {
        return GROUPLEVEL;
    }

    public void setGROUPLEVEL(String GROUPLEVEL) {
        this.GROUPLEVEL = GROUPLEVEL;
    }

    public String getUSERAD() {
        return USERAD;
    }

    public void setUSERAD(String USERAD) {
        this.USERAD = USERAD;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
