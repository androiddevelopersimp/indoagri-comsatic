package com.indoagri.comsatic.Retrofit.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalesDataModel {

    public static String TABLE_NAME = "T_SalesData";
    public static String XML_id = "id";
    public static String XML_idx = "idx";
    public static String XML_source= "source";
    public static String XML_vbeln= "vbeln";
    public static String XML_posnr= "posnr";
    public static String XML_vkorg= "vkorg";
    public static String XML_vtweg= "vtweg";
    public static String XML_spart= "spart";
    public static String XML_erdat= "erdat";
    public static String XML_erzet= "erzet";
    public static String XML_audat= "audat";
    public static String XML_auart= "auart";
    public static String XML_kunnr= "kunnr";
    public static String XML_namE1= "nameE1";
    public static String XML_zterm= "zterm";
    public static String XML_incO1= "incO1";
    public static String XML_incO2= "incO2";
    public static String XML_werks= "werks";
    public static String XML_vstel= "vstel";
    public static String XML_matnr= "matnr";
    public static String XML_arktx= "arktx";
    public static String XML_kwmeng= "kwmeng";
    public static String XML_vrkme= "vrkme";
    public static String XML_netwr= "netwr";
    public static String XML_waerk= "waerk";
    public static String XML_gjahr= "gjahr";
    public static String XML_monat= "monat";

    @SerializedName("idx")
    @Expose
    String idx;
    @SerializedName("source")
    @Expose
    String source;
    @SerializedName("vbeln")
    @Expose
    String vbeln;
    @SerializedName("posnr")
    @Expose
    String posnr;
    @SerializedName("vkorg")
    @Expose
    String vkorg;
    @SerializedName("vtweg")
    @Expose
    String vtweg;
    @SerializedName("spart")
    @Expose
    String spart;
    @SerializedName("erdat")
    @Expose
    String erdat;
    @SerializedName("erzet")
    @Expose
    String erzet;
    @SerializedName("audat")
    @Expose
    String audat;
    @SerializedName("auart")
    @Expose
    String auart;
    @SerializedName("kunnr")
    @Expose
    String kunnr;
    @SerializedName("namE1")
    @Expose
    String nameE1;
    @SerializedName("zterm")
    @Expose
    String zterm;
    @SerializedName("incO1")
    @Expose
    String inco1;
    @SerializedName("incO2")
    @Expose
    String inco2;
    @SerializedName("werks")
    @Expose
    String werks;
    @SerializedName("vstel")
    @Expose
    String vstel;
    @SerializedName("matnr")
    @Expose
    String matnr;
    @SerializedName("arktx")
    @Expose
    String arktx;
    @SerializedName("kwmeng")
    @Expose
    double kwmeng;
    @SerializedName("vrkme")
    @Expose
    String vrkme;
    @SerializedName("netwr")
    @Expose
    double netwr;
    @SerializedName("waerk")
    @Expose
    String waerk;
    @SerializedName("gjahr")
    @Expose
    String gjahr;
    @SerializedName("monat")
    @Expose
    String monat;



    public SalesDataModel() {
    }


    public SalesDataModel(String idx, String source, String vbeln, String posnr, String vkorg, String vtweg, String spart, String erdat, String erzet, String audat, String auart, String kunnr, String nameE1, String zterm, String inco1, String inco2, String werks, String vstel, String matnr, String arktx, double kwmeng, String vrkme, double netwr, String waerk,String gjahr,String monat) {
        this.idx = idx;
        this.source = source;
        this.vbeln = vbeln;
        this.posnr = posnr;
        this.vkorg = vkorg;
        this.vtweg = vtweg;
        this.spart = spart;
        this.erdat = erdat;
        this.erzet = erzet;
        this.audat = audat;
        this.auart = auart;
        this.kunnr = kunnr;
        this.nameE1 = nameE1;
        this.zterm = zterm;
        this.inco1 = inco1;
        this.inco2 = inco2;
        this.werks = werks;
        this.vstel = vstel;
        this.matnr = matnr;
        this.arktx = arktx;
        this.kwmeng = kwmeng;
        this.vrkme = vrkme;
        this.netwr = netwr;
        this.waerk = waerk;
        this.gjahr = gjahr;
        this.monat= monat;
    }

    public String getIdx() {
        return idx;
    }

    public void setIdx(String idx) {
        this.idx = idx;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getVbeln() {
        return vbeln;
    }

    public void setVbeln(String vbeln) {
        this.vbeln = vbeln;
    }

    public String getPosnr() {
        return posnr;
    }

    public void setPosnr(String posnr) {
        this.posnr = posnr;
    }

    public String getVkorg() {
        return vkorg;
    }

    public void setVkorg(String vkorg) {
        this.vkorg = vkorg;
    }

    public String getVtweg() {
        return vtweg;
    }

    public void setVtweg(String vtweg) {
        this.vtweg = vtweg;
    }

    public String getSpart() {
        return spart;
    }

    public void setSpart(String spart) {
        this.spart = spart;
    }

    public String getErdat() {
        return erdat;
    }

    public void setErdat(String erdat) {
        this.erdat = erdat;
    }

    public String getErzet() {
        return erzet;
    }

    public void setErzet(String erzet) {
        this.erzet = erzet;
    }

    public String getAudat() {
        return audat;
    }

    public void setAudat(String audat) {
        this.audat = audat;
    }

    public String getAuart() {
        return auart;
    }

    public void setAuart(String auart) {
        this.auart = auart;
    }

    public String getKunnr() {
        return kunnr;
    }

    public void setKunnr(String kunnr) {
        this.kunnr = kunnr;
    }

    public String getNameE1() {
        return nameE1;
    }

    public void setNameE1(String nameE1) {
        this.nameE1 = nameE1;
    }

    public String getZterm() {
        return zterm;
    }

    public void setZterm(String zterm) {
        this.zterm = zterm;
    }

    public String getInco1() {
        return inco1;
    }

    public void setInco1(String inco1) {
        this.inco1 = inco1;
    }

    public String getInco2() {
        return inco2;
    }

    public void setInco2(String inco2) {
        this.inco2 = inco2;
    }

    public String getWerks() {
        return werks;
    }

    public void setWerks(String werks) {
        this.werks = werks;
    }

    public String getVstel() {
        return vstel;
    }

    public void setVstel(String vstel) {
        this.vstel = vstel;
    }

    public String getMatnr() {
        return matnr;
    }

    public void setMatnr(String matnr) {
        this.matnr = matnr;
    }

    public String getArktx() {
        return arktx;
    }

    public void setArktx(String arktx) {
        this.arktx = arktx;
    }

    public double getKwmeng() {
        return kwmeng;
    }

    public void setKwmeng(double kwmeng) {
        this.kwmeng = kwmeng;
    }

    public String getVrkme() {
        return vrkme;
    }

    public void setVrkme(String vrkme) {
        this.vrkme = vrkme;
    }

    public double getNetwr() {
        return netwr;
    }

    public void setNetwr(double netwr) {
        this.netwr = netwr;
    }

    public String getWaerk() {
        return waerk;
    }

    public void setWaerk(String waerk) {
        this.waerk = waerk;
    }

    public String getGjahr() {
        return gjahr;
    }

    public void setGjahr(String gjahr) {
        this.gjahr = gjahr;
    }

    public String getMonat() {
        return monat;
    }

    public void setMonat(String monat) {
        this.monat = monat;
    }
}
