package com.indoagri.comsatic.Retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indoagri.comsatic.Retrofit.Model.GroupProductModel;
import com.indoagri.comsatic.Retrofit.Model.UserModel;

import java.util.List;

public class UsersLoginResponse {
        @SerializedName("userDetail")
        @Expose
        UserModel userModel;

        public UserModel getUserLogin() {
            return userModel;
        }
        public UserModel getUserLogout() {
            return userModel;
        }
}
