package com.indoagri.comsatic.Retrofit;

import android.content.Context;

import com.indoagri.comsatic.Apps;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface  ApiServices {
    Context context = Apps.getAppContext();
    @FormUrlEncoded
    @POST("account/loginpermission")
    Call<UsersLoginResponse> LoginPermission(@Header("X-API-KEY") String xapikey,
                             @Field("userName") String username,
                             @Field("userPassword") String password,
                             @Field("Domain") String domain,
                             @Field("DeviceID") String deviceid);

    @FormUrlEncoded
    @POST("account/logoutpermission")
    Call<UsersLoginResponse> LogoutPermission(@Header("X-API-KEY") String xapikey,
                                             @Field("userName") String username,
                                             @Field("userPassword") String password,
                                             @Field("Domain") String domain,
                                             @Field("DeviceID") String deviceid);
    @GET("comsaticgroupcompany/all")
    Call<GroupCompanyResponse> getCompany();
    @GET("comsaticgroupproduct/all")
    Call<GroupProductResponse> getProducts();
    @GET("comsaticsales/all")
    Call<SalesDataResponse> getDataSales();
    @GET("comsaticsales/current")
    Call<SalesDataResponse> getDataSalesThisMonth();
    @GET("comsaticprice/all")
    Call<PriceResponse> getDataPriceCurrent();
    @GET("comsaticsales/getmonth")
    Call<SalesDataResponse> getDataSalesMonth(@Query("month") String month);
    @GET("comsaticsales/getmonthyear")
    Call<SalesDataResponse> getDataSalesMonthYear(@Query("month") String month,
                                              @Query("year") String year);
    @GET("comsaticsales/getyear")
    Call<SalesDataResponse> getDataSalesYear(@Query("year") String year);
    @GET("permission/checkapp") //i.e https://api.test.com/Search?
    Call<AppDetailResponse> getAppDetail(@Query("userad") String userad,
                                         @Query("app") String app);
    @GET("comsaticstock/all")
    Call<StockResponse> getDataStockCurrent();
}
