package com.indoagri.comsatic.Retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indoagri.comsatic.Retrofit.Model.AppDetailModel;

public class AppDetailResponse {
    @SerializedName("appDetail")
    @Expose
    AppDetailModel appDetailModel;

    public AppDetailModel getAPP() {
        return appDetailModel;
    }
}
