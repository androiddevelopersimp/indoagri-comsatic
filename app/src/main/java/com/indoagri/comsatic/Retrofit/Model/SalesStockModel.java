package com.indoagri.comsatic.Retrofit.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalesStockModel {
    public static final String TABLE_NAME = "Table_Stock";
    public static final String XML_ID = "ID";
    public static final String XML_COMPANY = "COMPANY";
    public static final String XML_AREA = "AREA";
    public static final String XML_PLANT = "PLANT";
    public static final String XML_MATERIAL = "MATERIAL";
    public static final String XML_COMPANYNAME = "COMPANYNAME";
    public static final String XML_PLANTNAME = "PLANTNAME";
    public static final String XML_BEGINSTOCK = "BEGINSTOCK";
    public static final String XML_RECEIVESTOCK = "RECEIVESTOCK";
    public static final String XML_DELVSTOCK = "DELVSTOCK";
    public static final String XML_INTRANSIT = "INTRANSIT";
    public static final String XML_OTHERSTOCK = "OTHERSTOCK";
    public static final String XML_ENDINGSTOCK = "ENDINGSTOCKS";
    public static final String XML_PABRIK = "PABRIK";
    public static final String XML_OUTSTANDING = "OUTSTANDING";
    public static final String XML_CREATEDDATE = "CREATEDDATE";


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("plant")
    @Expose
    private String plant;
    @SerializedName("material")
    @Expose
    private String material;
    @SerializedName("companyname")
    @Expose
    private String companyname;
    @SerializedName("plantname")
    @Expose
    private String plantname;
    @SerializedName("begiN_STOCK")
    @Expose
    private String begiNSTOCK;
    @SerializedName("receivE_STOCK")
    @Expose
    private String receivESTOCK;
    @SerializedName("delV_STOCK")
    @Expose
    private String delVSTOCK;
    @SerializedName("iN_TRANSIT")
    @Expose
    private String iNTRANSIT;
    @SerializedName("otheR_STOCK")
    @Expose
    private String otheRSTOCK;
    @SerializedName("endinG_STOCK")
    @Expose
    private String endinGSTOCK;
    @SerializedName("pabrik")
    @Expose
    private String pabrik;
    @SerializedName("outstanding")
    @Expose
    private String outstanding;
    @SerializedName("createddate")
    @Expose
    private String createddate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getPlant() {
        return plant;
    }

    public void setPlant(String plant) {
        this.plant = plant;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getPlantname() {
        return plantname;
    }

    public void setPlantname(String plantname) {
        this.plantname = plantname;
    }

    public String getBegiNSTOCK() {
        return begiNSTOCK;
    }

    public void setBegiNSTOCK(String begiNSTOCK) {
        this.begiNSTOCK = begiNSTOCK;
    }

    public String getReceivESTOCK() {
        return receivESTOCK;
    }

    public void setReceivESTOCK(String receivESTOCK) {
        this.receivESTOCK = receivESTOCK;
    }

    public String getDelVSTOCK() {
        return delVSTOCK;
    }

    public void setDelVSTOCK(String delVSTOCK) {
        this.delVSTOCK = delVSTOCK;
    }

    public String getINTRANSIT() {
        return iNTRANSIT;
    }

    public void setINTRANSIT(String iNTRANSIT) {
        this.iNTRANSIT = iNTRANSIT;
    }

    public String getOtheRSTOCK() {
        return otheRSTOCK;
    }

    public void setOtheRSTOCK(String otheRSTOCK) {
        this.otheRSTOCK = otheRSTOCK;
    }

    public String getEndinGSTOCK() {
        return endinGSTOCK;
    }

    public void setEndinGSTOCK(String endinGSTOCK) {
        this.endinGSTOCK = endinGSTOCK;
    }

    public String getPabrik() {
        return pabrik;
    }

    public void setPabrik(String pabrik) {
        this.pabrik = pabrik;
    }

    public String getOutstanding() {
        return outstanding;
    }

    public void setOutstanding(String outstanding) {
        this.outstanding = outstanding;
    }

    public String getCreateddate() {
        return createddate;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }
}
