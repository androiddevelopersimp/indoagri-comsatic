package com.indoagri.comsatic.Retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indoagri.comsatic.Retrofit.Model.SalesDataModel;

import java.util.List;

public class SalesDataResponse {

    @SerializedName("salesData")
    @Expose
    List<SalesDataModel> salesDataModels;

    public List getSales() {
        return salesDataModels;
    }
}
