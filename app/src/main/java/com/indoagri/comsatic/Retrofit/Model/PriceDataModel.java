package com.indoagri.comsatic.Retrofit.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class PriceDataModel implements Parcelable {
    public static final String TABLE_NAME = "Table_Price";
    public static final String XML_ID = "ID";
    public static final String XML_MATERIAL = "MATERIAL";
    public static final String XML_DESCRIPTION = "DESCRIPTION";
    public static final String XML_PRICE = "PRICE";
    public static final String XML_DATE = "DATE";
    public static final String XML_GROUPPRODUCT = "GROUPPRODUCT";
    public static final String XML_SUBGROUP = "SUBGROUP";
    public static final String XML_PRICETYPE = "PRICETYPE";
    public static final String XML_CURRENCY = "CURRENCY";
    @SerializedName("id")
    String id;
    @SerializedName("material")
    String material;
    @SerializedName("description")
    String description;
    @SerializedName("price")
    String price;
    @SerializedName("date")
    String date;
    @SerializedName("groupproduct")
    String grouproduct;
    @SerializedName("subGroup")
    String subgroup;
    @SerializedName("pricetype")
    String pricetype;
    @SerializedName("currency")
    String currency;

    public PriceDataModel() {
    }

    public String getPricetype() {
        return pricetype;
    }

    public void setPricetype(String pricetype) {
        this.pricetype = pricetype;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getGrouproduct() {
        return grouproduct;
    }

    public void setGrouproduct(String grouproduct) {
        this.grouproduct = grouproduct;
    }

    public String getSubgroup() {
        return subgroup;
    }

    public void setSubgroup(String subgroup) {
        this.subgroup = subgroup;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.material);
        dest.writeString(this.description);
        dest.writeString(this.price);
        dest.writeString(this.date);
        dest.writeString(this.grouproduct);
        dest.writeString(this.subgroup);
    }

    protected PriceDataModel(Parcel in) {
        this.id = in.readString();
        this.material = in.readString();
        this.description = in.readString();
        this.price = in.readString();
        this.date = in.readString();
        this.grouproduct = in.readString();
        this.subgroup = in.readString();
    }

    public static final Creator<PriceDataModel> CREATOR = new Creator<PriceDataModel>() {
        @Override
        public PriceDataModel createFromParcel(Parcel source) {
            return new PriceDataModel(source);
        }

        @Override
        public PriceDataModel[] newArray(int size) {
            return new PriceDataModel[size];
        }
    };
}
