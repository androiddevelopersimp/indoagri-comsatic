package com.indoagri.comsatic.Retrofit.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class SalesBilling implements Parcelable {

    @SerializedName("id")
    int id;
    @SerializedName("Company")
    String Company;
    @SerializedName("Product")
    String Product;
    @SerializedName("Year")
    String Year;
    @SerializedName("Periode")
    String Periode;
    @SerializedName("Tipe")
    String Tipe;
    @SerializedName("Qty")
    String Qty;
    @SerializedName("Value")
    String Value;
    @SerializedName("Price")
    String Price;

    public final static String TABLE_NAME = "T_SalesBilling";
    public static final String ALIAS = "SalesBill";
    public final static String XML_ID = "id";
    public final static String XML_COMPANY= "Company";
    public final static String XML_PRODUCT = "Product";
    public final static String XML_YEAR = "Year";
    public final static String XML_PERIODE = "Periode";
    public final static String XML_TIPE = "Tipe";
    public final static String XML_QTY= "Qty";
    public final static String XML_VALUE = "Value";
    public final static String XML_PRICE = "Price";

    public SalesBilling(){}


    public SalesBilling(int id, String company, String product, String year, String periode, String tipe, String qty, String value, String price) {
        this.id = id;
        Company = company;
        Product = product;
        Year = year;
        Periode = periode;
        Tipe = tipe;
        Qty = qty;
        Value = value;
        Price = price;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }

    public String getProduct() {
        return Product;
    }

    public void setProduct(String product) {
        Product = product;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getPeriode() {
        return Periode;
    }

    public void setPeriode(String periode) {
        Periode = periode;
    }

    public String getTipe() {
        return Tipe;
    }

    public void setTipe(String tipe) {
        Tipe = tipe;
    }

    public String getQty() {
        return Qty;
    }

    public void setQty(String qty) {
        Qty = qty;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public static String getTableName() {
        return TABLE_NAME;
    }

    public static String getALIAS() {
        return ALIAS;
    }

    public static String getXmlId() {
        return XML_ID;
    }

    public static String getXmlCompany() {
        return XML_COMPANY;
    }

    public static String getXmlProduct() {
        return XML_PRODUCT;
    }

    public static String getXmlYear() {
        return XML_YEAR;
    }

    public static String getXmlPeriode() {
        return XML_PERIODE;
    }

    public static String getXmlTipe() {
        return XML_TIPE;
    }

    public static String getXmlQty() {
        return XML_QTY;
    }

    public static String getXmlValue() {
        return XML_VALUE;
    }

    public static String getXmlPrice() {
        return XML_PRICE;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.Company);
        dest.writeString(this.Product);
        dest.writeString(this.Year);
        dest.writeString(this.Periode);
        dest.writeString(this.Tipe);
        dest.writeString(this.Qty);
        dest.writeString(this.Value);
        dest.writeString(this.Price);
    }

    protected SalesBilling(Parcel in) {
        this.id = in.readInt();
        this.Company = in.readString();
        this.Product = in.readString();
        this.Year = in.readString();
        this.Periode = in.readString();
        this.Tipe = in.readString();
        this.Qty = in.readString();
        this.Value = in.readString();
        this.Price = in.readString();
    }

    public static final Parcelable.Creator<SalesBilling> CREATOR = new Parcelable.Creator<SalesBilling>() {
        @Override
        public SalesBilling createFromParcel(Parcel source) {
            return new SalesBilling(source);
        }

        @Override
        public SalesBilling[] newArray(int size) {
            return new SalesBilling[size];
        }
    };
}
