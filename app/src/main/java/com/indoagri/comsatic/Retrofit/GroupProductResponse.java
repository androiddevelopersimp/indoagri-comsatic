package com.indoagri.comsatic.Retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indoagri.comsatic.Retrofit.Model.GroupCompanyModel;
import com.indoagri.comsatic.Retrofit.Model.GroupProductModel;

import java.util.List;

public class GroupProductResponse {
    @SerializedName("groupProduct")
    @Expose
    List<GroupProductModel> groupProductModels;

    public List getProducts() {
        return groupProductModels;
    }
}
