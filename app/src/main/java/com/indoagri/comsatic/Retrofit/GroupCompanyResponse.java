package com.indoagri.comsatic.Retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indoagri.comsatic.Retrofit.Model.GroupCompanyModel;
import com.indoagri.comsatic.Retrofit.Model.SalesDataModel;

import java.util.List;

public class GroupCompanyResponse {
        @SerializedName("groupCompany")
        @Expose
        List<GroupCompanyModel> groupCompanyModels;

        public List getCompanies() {
            return groupCompanyModels;
        }

}
