package com.indoagri.comsatic.Retrofit.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class UserModel implements Parcelable {

    public static final String TABLE_NAME = "Table_User";
    public static final String XML_ID= "Id";
    public static final String XML_USERID = "userId";
    public static final String XML_USERNAME= "userName";
    public static final String XML_SURNAME= "userSurname";
    public static final String XML_EMAIL= "userEmail";
    public static final String XML_LDAP= "emailLDAP";
    public static final String XML_DOMAIN= "domain";
    public static final String XML_USERDESCRIPTION= "userDescription";
    public static final String XML_COMPANY= "company";
    public static final String XML_PROVINCE= "userProvince";
    public static final String XML_DEPARTMENT= "department";
    public static final String XML_EMPLOYEEID= "employeeID";
    public static final String XML_USERFULLNAME= "fullname";
    public static final String XML_USERPHONE= "userPhone";
    public static final String XML_USERPASSWORD= "userPassword";
    public static final String XML_USERKEY= "userKey";

    @SerializedName("userId")
    public String USERID;
    @SerializedName("userName")
    public String USERNAME;
    @SerializedName("userSurname")
    public String SURNAME;
    @SerializedName("userEmail")
    public String EMAIL;
    @SerializedName("emailLDAP")
    public String LDAP;
    @SerializedName("domain")
    public String DOMAIN;
    @SerializedName("userDescription")
    public String DESCRIPTION;
    @SerializedName("company")
    public String COMPANY;
    @SerializedName("userProvince")
    public String PROVINCE;
    @SerializedName("department")
    public String DEPARTMENT;
    @SerializedName("employeeID")
    public String EMPLOYEEID;
    @SerializedName("fullname")
    public String FULLNAME;
    @SerializedName("userPhone")
    public String PHONE;
    @SerializedName("userPassword")
    public String PASSWORD;
    @SerializedName("userKey")
    public String KEY;


    public UserModel(){

    }
    public UserModel(String USERID, String USERNAME, String SURNAME, String EMAIL, String LDAP, String DOMAIN, String DESCRIPTION, String COMPANY, String PROVINCE, String DEPARTMENT, String EMPLOYEEID, String FULLNAME, String PHONE, String PASSWORD, String KEY) {
        this.USERID = USERID;
        this.USERNAME = USERNAME;
        this.SURNAME = SURNAME;
        this.EMAIL = EMAIL;
        this.LDAP = LDAP;
        this.DOMAIN = DOMAIN;
        this.DESCRIPTION = DESCRIPTION;
        this.COMPANY = COMPANY;
        this.PROVINCE = PROVINCE;
        this.DEPARTMENT = DEPARTMENT;
        this.EMPLOYEEID = EMPLOYEEID;
        this.FULLNAME = FULLNAME;
        this.PHONE = PHONE;
        this.PASSWORD = PASSWORD;
        this.KEY = KEY;
    }

    public static String getTableName() {
        return TABLE_NAME;
    }

    public static String getXmlId() {
        return XML_ID;
    }

    public static String getXmlUserid() {
        return XML_USERID;
    }

    public static String getXmlUsername() {
        return XML_USERNAME;
    }

    public static String getXmlSurname() {
        return XML_SURNAME;
    }

    public static String getXmlEmail() {
        return XML_EMAIL;
    }

    public static String getXmlLdap() {
        return XML_LDAP;
    }

    public static String getXmlDomain() {
        return XML_DOMAIN;
    }

    public static String getXmlUserdescription() {
        return XML_USERDESCRIPTION;
    }

    public static String getXmlCompany() {
        return XML_COMPANY;
    }

    public static String getXmlProvince() {
        return XML_PROVINCE;
    }

    public static String getXmlDepartment() {
        return XML_DEPARTMENT;
    }

    public static String getXmlEmployeeid() {
        return XML_EMPLOYEEID;
    }

    public static String getXmlUserfullname() {
        return XML_USERFULLNAME;
    }

    public static String getXmlUserphone() {
        return XML_USERPHONE;
    }

    public static String getXmlUserpassword() {
        return XML_USERPASSWORD;
    }

    public static String getXmlUserkey() {
        return XML_USERKEY;
    }

    public String getUSERID() {
        return USERID;
    }

    public void setUSERID(String USERID) {
        this.USERID = USERID;
    }

    public String getUSERNAME() {
        return USERNAME;
    }

    public void setUSERNAME(String USERNAME) {
        this.USERNAME = USERNAME;
    }

    public String getSURNAME() {
        return SURNAME;
    }

    public void setSURNAME(String SURNAME) {
        this.SURNAME = SURNAME;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getLDAP() {
        return LDAP;
    }

    public void setLDAP(String LDAP) {
        this.LDAP = LDAP;
    }

    public String getDOMAIN() {
        return DOMAIN;
    }

    public void setDOMAIN(String DOMAIN) {
        this.DOMAIN = DOMAIN;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public String getCOMPANY() {
        return COMPANY;
    }

    public void setCOMPANY(String COMPANY) {
        this.COMPANY = COMPANY;
    }

    public String getPROVINCE() {
        return PROVINCE;
    }

    public void setPROVINCE(String PROVINCE) {
        this.PROVINCE = PROVINCE;
    }

    public String getDEPARTMENT() {
        return DEPARTMENT;
    }

    public void setDEPARTMENT(String DEPARTMENT) {
        this.DEPARTMENT = DEPARTMENT;
    }

    public String getEMPLOYEEID() {
        return EMPLOYEEID;
    }

    public void setEMPLOYEEID(String EMPLOYEEID) {
        this.EMPLOYEEID = EMPLOYEEID;
    }

    public String getFULLNAME() {
        return FULLNAME;
    }

    public void setFULLNAME(String FULLNAME) {
        this.FULLNAME = FULLNAME;
    }

    public String getPHONE() {
        return PHONE;
    }

    public void setPHONE(String PHONE) {
        this.PHONE = PHONE;
    }

    public String getPASSWORD() {
        return PASSWORD;
    }

    public void setPASSWORD(String PASSWORD) {
        this.PASSWORD = PASSWORD;
    }

    public String getKEY() {
        return KEY;
    }

    public void setKEY(String KEY) {
        this.KEY = KEY;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.USERID);
        dest.writeString(this.USERNAME);
        dest.writeString(this.SURNAME);
        dest.writeString(this.EMAIL);
        dest.writeString(this.LDAP);
        dest.writeString(this.DOMAIN);
        dest.writeString(this.DESCRIPTION);
        dest.writeString(this.COMPANY);
        dest.writeString(this.PROVINCE);
        dest.writeString(this.DEPARTMENT);
        dest.writeString(this.EMPLOYEEID);
        dest.writeString(this.FULLNAME);
        dest.writeString(this.PHONE);
        dest.writeString(this.PASSWORD);
        dest.writeString(this.KEY);
    }

    protected UserModel(Parcel in) {
        this.USERID = in.readString();
        this.USERNAME = in.readString();
        this.SURNAME = in.readString();
        this.EMAIL = in.readString();
        this.LDAP = in.readString();
        this.DOMAIN = in.readString();
        this.DESCRIPTION = in.readString();
        this.COMPANY = in.readString();
        this.PROVINCE = in.readString();
        this.DEPARTMENT = in.readString();
        this.EMPLOYEEID = in.readString();
        this.FULLNAME = in.readString();
        this.PHONE = in.readString();
        this.PASSWORD = in.readString();
        this.KEY = in.readString();
    }

    public static final Parcelable.Creator<UserModel> CREATOR = new Parcelable.Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel source) {
            return new UserModel(source);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };
}
