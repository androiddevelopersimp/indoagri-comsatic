package com.indoagri.comsatic.Retrofit;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.indoagri.comsatic.Common.Constants;

public class SharePreference{
    Context _context;
    private SharedPreferences shared;
    private Editor editor;
    int PRIVATE_MODE = 0;
    private static String TAG = SharePreference.class.getSimpleName();
    public final static String SHARED_NAME = "IMOBILE";
    public final static String SHARED_COUNTRY_CODE = "COUNTRY_CODE";
    public final static String SHARED_QR = "QR";
    public final static String SHARED_DEVICEIMEI= "IMEI";
    public final static String FORM_YEAR = "YEAR";
    public final static String FORM_GROUPCOMPANY = "GROUPCOMPANY";
    public final static String FORM_COMPANY = "COMPANY";
    public final static String FORM_COMPANY_DESC= "COMPANYDESC";
    public final static String FORM_PRODUCT = "PRODUCT";
    public final static String FORM_SUBPRODUCT = "SUBPRODUCT";
    public final static String FORM_UPDATESYNCDATE= "SYNCDATE";
    public final static String FORM_LoginUserName = "LoginUserName";
    public final static String FORM_LoginEmail= "LoginEmail";
    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";
    private static final String USER_AGGREE = "Aggree";
    private static final String KEY_USERNAME= "userName";
    private static final String KEY_USERSURNAME= "userSurname";
    private static final String KEY_USEREMAIL= "userEmail";
    private static final String KEY_USERLDAP= "emailLDAP";
    private static final String KEY_USERDOMAIN= "domain";
    private static final String KEY_USERDESCRIPTION= "userDescription";
    private static final String KEY_USERCOMPANY= "company";
    private static final String KEY_USERPROVINCE= "userProvince";
    private static final String KEY_USERDEPARTMENT= "department";
    private static final String KEY_USEREMPLOYEE= "employeeID";
    private static final String KEY_USERFULLNAME= "fullname";
    private static final String KEY_USERPHONE= "userPhone";
    private static final String KEY_USERPASSWORD= "userPassword";
    private static final String KEY_USERKEY= "userKey";
    private static final String KEY_UPDATECONFIRMATION = "confirmationUpdate";

    //
    public final static String FORM_REFRESHMONTH= "REFRESHMONTHS";
    public final static String FORM_REFRESHYEAR= "REFRESHYEARS";
    //

    public SharePreference(Context context){
        /*shared = context.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE);*/
        this._context = context;
        shared = _context.getSharedPreferences(Constants.shared_name, PRIVATE_MODE);
        editor = shared.edit();
    }

    public void setLogin(boolean isLoggedIn) {
        editor.putBoolean(KEY_IS_LOGGED_IN, isLoggedIn);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }
    public void setAggree(boolean isAggree) {
        editor.putBoolean(USER_AGGREE, isAggree);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }
    public void setKeyEmailuser(String EmailUser) {
        editor.putString(KEY_USEREMAIL, EmailUser);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }
    public void setKeyUsername(String use) {
        editor.putString(KEY_USERNAME, use);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }
    public void setKeyPhoneuser(String phone) {
        editor.putString(KEY_USERPHONE, phone);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }
    public void setKeyUsersurname(String surname) {
        editor.putString(KEY_USERSURNAME, surname);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }
    public void setKeyUserldap(String ldap) {
        editor.putString(KEY_USERLDAP, ldap);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }
    public void setKeyUserdomain(String domain) {
        editor.putString(KEY_USERDOMAIN, domain);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }
    public void setKeyUserprovince(String province) {
        editor.putString(KEY_USERPROVINCE, province);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }
    public void setKeyUserdepartment(String depart) {
        editor.putString(KEY_USERDEPARTMENT, depart);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }
    public void setKeyUserdescription(String desc) {
        editor.putString(KEY_USERDESCRIPTION, desc);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }
    public void setKeyUsercompany(String company) {
        editor.putString(KEY_USERCOMPANY, company);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }
    public void setKeyUseremployee(String employee) {
        editor.putString(KEY_USEREMPLOYEE, employee);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }

    public void setKeyUserfullname(String fullname) {
        editor.putString(KEY_USERFULLNAME, fullname);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }

    public void setKeyUserkey(String key) {
        editor.putString(KEY_USERKEY, key);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }
    public void setKeyUserpassword(String pass) {
        editor.putString(KEY_USERPASSWORD, pass);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }
    public void setIMEI(String imei){
        editor.putString(SHARED_DEVICEIMEI, imei);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }

    public void setFormYear(String formYear) {
        editor.putString(FORM_YEAR, formYear);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }

    public void setFormCompany(String formCompany){
        editor.putString(FORM_COMPANY, formCompany);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }
    public void setFormCompanyDesc(String formCompanyDesc){
        editor.putString(FORM_COMPANY_DESC, formCompanyDesc);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }
    public void setFormGroupcompany(String formGroupcompany) {
        editor.putString(FORM_GROUPCOMPANY, formGroupcompany);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }
    public void setFormProduct(String formProduct){
        editor.putString(FORM_PRODUCT, formProduct);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }
    public void setFormSubproduct(String subproduct){
        editor.putString(FORM_SUBPRODUCT, subproduct);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }
    public void setUpdateSyncDateSales(String updatesyncdate){
        editor.putString(FORM_UPDATESYNCDATE, updatesyncdate);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }
    public void setFormLoginUsername(String loginUsername){
        editor.putString(FORM_LoginUserName, loginUsername);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }
    public void setFORMLoginEmail(String loginEmail){
        editor.putString(FORM_LoginEmail, loginEmail);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }
    public void setKonfirmationUpdate(boolean confirmUpdate){
        editor.putBoolean(KEY_UPDATECONFIRMATION, confirmUpdate);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }
    public boolean isConfirmationUpdate(){
        return shared.getBoolean(KEY_UPDATECONFIRMATION, false);
    }

    public void setRefreshMonth(String refreshMonth) {
        editor.putString(FORM_REFRESHMONTH, refreshMonth);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }
    public void setRefreshYear(String refreshYear) {
        editor.putString(FORM_REFRESHYEAR, refreshYear);
        // commit changes
        editor.commit();
        Log.d(TAG, "UserConfig content_login session modified!");
    }
    public boolean isLoggedIn(){
        return shared.getBoolean(KEY_IS_LOGGED_IN, false);
    }
    public boolean isAggree(){
        return shared.getBoolean(USER_AGGREE, false);
    }
    public String isUserName(){
        return shared.getString(KEY_USERNAME, "0");
    }
    public String isSurName(){
        return shared.getString(KEY_USERSURNAME, "0");
    }
    public String isEmail(){
        return shared.getString(KEY_USEREMAIL, "0");
    }
    public String isLDAP(){
        return shared.getString(KEY_USERLDAP, "0");
    }
    public String isDomain(){
        return shared.getString(KEY_USERDOMAIN, "0");
    }
    public String isDescription(){
        return shared.getString(KEY_USERDESCRIPTION, "0");
    }
    public String isCompany(){
        return shared.getString(KEY_USERCOMPANY, "0");
    }
    public String isProvince(){
        return shared.getString(KEY_USERPROVINCE, "0");
    }
    public String isDepartment(){
        return shared.getString(KEY_USERDEPARTMENT, "0");
    }
    public String isEmployeeID(){
        return shared.getString(KEY_USEREMPLOYEE, "0");
    }
    public String isFullname(){
        return shared.getString(KEY_USERFULLNAME, "0");
    }
    public String isPhone(){
        return shared.getString(KEY_USERPHONE, "0");
    }
    public String isPassword(){
        return shared.getString(KEY_USERPASSWORD, "0");
    }
    public String isKey(){
        return shared.getString(KEY_USERKEY, "0");
    }

    public String isFormGroupCompany(){
        return shared.getString(FORM_GROUPCOMPANY, "");
    }
    public String isFormCompany(){
        return shared.getString(FORM_COMPANY, "");
    }
    public String isFormCompanyDesc(){
        return shared.getString(FORM_COMPANY_DESC, "");
    }
    public String isFormYear(){
        return shared.getString(FORM_YEAR, "");
    }
    public String isFormProduct(){
        return shared.getString(FORM_PRODUCT, "");
    }
    public String isFormSubProduct(){
        return shared.getString(FORM_SUBPRODUCT, "");
    }
    public String isUpdateSyncSales(){
        return shared.getString(FORM_UPDATESYNCDATE, "");
    }
    public String isFormLoginUsername(){
        return shared.getString(FORM_LoginUserName, null);
    }
    public String isFormLoginEmail(){
        return shared.getString(FORM_LoginEmail, null);
    }
    public String isRefreshMonth(){
        return shared.getString(FORM_REFRESHMONTH, null);
    }
    public String isRefreshYear(){
        return shared.getString(FORM_REFRESHYEAR, null);
    }

    public String getQR(){
        return shared.getString(SHARED_QR,"");
    }
    public String getIMEI(){
        return shared.getString(SHARED_DEVICEIMEI,"");
    }
}