package com.indoagri.comsatic.Retrofit.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GroupProductModel {

    public static String TABLE_NAME = "T_GROUPPRODUCT";
    public static String XML_id = "id";
    public static String XML_groupproduct= "groupProduct";
    public static String XML_subgroup= "subGroup";
    public static String XML_material = "material";
    public static String XML_materialDescription= "materialDescription";

    @SerializedName("groupProduct")
    @Expose
    String groupproduct;
    @SerializedName("subGroup")
    @Expose
    String subgroup;
    @SerializedName("material")
    @Expose
    String material;
    @SerializedName("materialDescription")
    @Expose
    String materialdescription;

    public GroupProductModel() {
    }

    public GroupProductModel(String groupproduct, String subgroup, String material, String materialdescription) {
        this.groupproduct = groupproduct;
        this.subgroup = subgroup;
        this.material = material;
        this.materialdescription = materialdescription;
    }

    public static String getTableName() {
        return TABLE_NAME;
    }

    public static void setTableName(String tableName) {
        TABLE_NAME = tableName;
    }

    public static String getXML_id() {
        return XML_id;
    }

    public static void setXML_id(String XML_id) {
        GroupProductModel.XML_id = XML_id;
    }

    public static String getXML_groupproduct() {
        return XML_groupproduct;
    }

    public static void setXML_groupproduct(String XML_groupproduct) {
        GroupProductModel.XML_groupproduct = XML_groupproduct;
    }

    public static String getXML_subgroup() {
        return XML_subgroup;
    }

    public static void setXML_subgroup(String XML_subgroup) {
        GroupProductModel.XML_subgroup = XML_subgroup;
    }

    public static String getXML_material() {
        return XML_material;
    }

    public static void setXML_material(String XML_material) {
        GroupProductModel.XML_material = XML_material;
    }

    public static String getXML_materialDescription() {
        return XML_materialDescription;
    }

    public static void setXML_materialDescription(String XML_materialDescription) {
        GroupProductModel.XML_materialDescription = XML_materialDescription;
    }

    public String getGroupproduct() {
        return groupproduct;
    }

    public void setGroupproduct(String groupproduct) {
        this.groupproduct = groupproduct;
    }

    public String getSubgroup() {
        return subgroup;
    }

    public void setSubgroup(String subgroup) {
        this.subgroup = subgroup;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getMaterialdescription() {
        return materialdescription;
    }

    public void setMaterialdescription(String materialdescription) {
        this.materialdescription = materialdescription;
    }
}
