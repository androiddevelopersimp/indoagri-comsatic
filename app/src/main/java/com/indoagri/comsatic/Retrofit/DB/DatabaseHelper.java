package com.indoagri.comsatic.Retrofit.DB;


import android.app.DownloadManager;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.indoagri.comsatic.Common.Constants;
import com.indoagri.comsatic.Retrofit.Model.AppDetailModel;
import com.indoagri.comsatic.Retrofit.Model.GroupCompanyModel;
import com.indoagri.comsatic.Retrofit.Model.GroupProductModel;
import com.indoagri.comsatic.Retrofit.Model.PriceDataModel;
import com.indoagri.comsatic.Retrofit.Model.SalesBilling;
import com.indoagri.comsatic.Retrofit.Model.SalesDataModel;
import com.indoagri.comsatic.Retrofit.Model.SalesStockModel;
import com.indoagri.comsatic.Retrofit.Model.UserModel;

/**
 * Created by Dwi.Fajar on 4/2/2018.
 */


public class DatabaseHelper extends SQLiteOpenHelper {

    public final static int dbVersion = Constants.db_version;
    public final static String dbName = Constants.db_name;
    Context context;
    SQLiteDatabase db;

    public DatabaseHelper(Context context) {
        super(context, dbName, null, dbVersion);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query;
        query = "create table " + UserModel.TABLE_NAME +
                "(" +
                UserModel.XML_ID + " text PRIMARY KEY, " +
                UserModel.XML_USERID + " text, " +
                UserModel.XML_USERNAME + " text, " +
                UserModel.XML_SURNAME + " text, " +
                UserModel.XML_EMAIL + " text, " +
                UserModel.XML_LDAP + " text, " +
                UserModel.XML_DOMAIN + " text, " +
                UserModel.XML_USERDESCRIPTION + " text, " +
                UserModel.XML_COMPANY + " text, " +
                UserModel.XML_PROVINCE + " text, " +
                UserModel.XML_DEPARTMENT + " text, " +
                UserModel.XML_EMPLOYEEID + " text, " +
                UserModel.XML_USERFULLNAME + " text," +
                UserModel.XML_USERPHONE + " text, " +
                UserModel.XML_USERPASSWORD + " text, " +
                UserModel.XML_USERKEY + " text " +
                ")";
        db.execSQL(query);
        query = "create table " + SalesBilling.TABLE_NAME +
                "(" +
                SalesBilling.XML_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                SalesBilling.XML_COMPANY + " text not null, " +
                SalesBilling.XML_PRODUCT + " text not null, " +
                SalesBilling.XML_YEAR + " text, " +
                SalesBilling.XML_PERIODE + " text, " +
                SalesBilling.XML_TIPE + " text, " +
                SalesBilling.XML_QTY + " text, " +
                SalesBilling.XML_VALUE + " text, " +
                SalesBilling.XML_PRICE + " text " +
                ")";
        db.execSQL(query);

        query = "create table " + GroupCompanyModel.TABLE_NAME +
                "(" +
                GroupCompanyModel.XML_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                GroupCompanyModel.XML_groupcompany + " text not null, " +
                GroupCompanyModel.XML_companycode + " text not null, " +
                GroupCompanyModel.XML_description + " text " +
                ")";
        db.execSQL(query);

        query = "create table " + GroupProductModel.TABLE_NAME +
                "(" +
                GroupProductModel.XML_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                GroupProductModel.XML_groupproduct + " text not null, " +
                GroupProductModel.XML_subgroup + " text not null, " +
                GroupProductModel.XML_material + " text, " +
                GroupProductModel.XML_materialDescription + " text " +
                ")";
        db.execSQL(query);
//
//
        query = "create table " + SalesDataModel.TABLE_NAME +
                "(" +
                SalesDataModel.XML_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                SalesDataModel.XML_idx + " text not null, " +
                SalesDataModel.XML_source + " text not null, " +
                SalesDataModel.XML_vbeln + " text, " +
                SalesDataModel.XML_posnr + " text, " +
                SalesDataModel.XML_vkorg + " text, " +
                SalesDataModel.XML_vtweg + " text, " +
                SalesDataModel.XML_spart + " text, " +
                SalesDataModel.XML_erdat + " text, " +
                SalesDataModel.XML_erzet + " text, " +
                SalesDataModel.XML_audat + " text, " +
                SalesDataModel.XML_auart + " text, " +
                SalesDataModel.XML_kunnr + " text, " +
                SalesDataModel.XML_namE1 + " text, " +
                SalesDataModel.XML_zterm + " text, " +
                SalesDataModel.XML_incO1 + " text, " +
                SalesDataModel.XML_incO2 + " text, " +
                SalesDataModel.XML_werks + " text, " +
                SalesDataModel.XML_vstel + " text, " +
                SalesDataModel.XML_matnr + " text, " +
                SalesDataModel.XML_arktx + " text, " +
                SalesDataModel.XML_kwmeng + " numeric, " +
                SalesDataModel.XML_vrkme + " text, " +
                SalesDataModel.XML_netwr + " numeric, " +
                SalesDataModel.XML_waerk + " text, " +
                SalesDataModel.XML_gjahr + " text, " +
                SalesDataModel.XML_monat + " text " +
                ")";
        db.execSQL(query);

        query = "create table " + AppDetailModel.TABLE_NAME +
                "(" +
                AppDetailModel.XML_APPID + " text, " +
                AppDetailModel.XML_APPNAME + " text, " +
                AppDetailModel.XML_APPDESC + " text," +
                AppDetailModel.XML_APPPACKAGE + " text, " +
                AppDetailModel.XML_VERSIONCODE + " text," +
                AppDetailModel.XML_VERSIONNAME + " text, " +
                AppDetailModel.XML_APKURL + " text," +
                AppDetailModel.XML_ASSETSICON + " text, " +
                AppDetailModel.XML_ASSETSICONURL + " text," +
                AppDetailModel.XML_ACTIVE + " text, " +
                AppDetailModel.XML_GROUPLEVEL + " text," +
                AppDetailModel.XML_USERAD + " text " +
                ")";
        db.execSQL(query);

        query = "create table " + PriceDataModel.TABLE_NAME +
                "(" +
                PriceDataModel.XML_ID + " text, " +
                PriceDataModel.XML_MATERIAL + " text, " +
                PriceDataModel.XML_DESCRIPTION + " text, " +
                PriceDataModel.XML_PRICE + " text," +
                PriceDataModel.XML_DATE + " text, " +
                PriceDataModel.XML_GROUPPRODUCT + " text," +
                PriceDataModel.XML_SUBGROUP + " text, " +
                PriceDataModel.XML_PRICETYPE + " text, " +
                PriceDataModel.XML_CURRENCY + " text " +
                ")";
        db.execSQL(query);


        query = "create table " + SalesStockModel.TABLE_NAME +
                "(" +
                SalesStockModel.XML_ID + " INTEGER, " +
                SalesStockModel.XML_COMPANY + " text, " +
                SalesStockModel.XML_AREA + " text, " +
                SalesStockModel.XML_PLANT + " text, " +
                SalesStockModel.XML_MATERIAL + " text, " +
                SalesStockModel.XML_COMPANYNAME + " text, " +
                SalesStockModel.XML_PLANTNAME + " text, " +
                SalesStockModel.XML_BEGINSTOCK + " text, " +
                SalesStockModel.XML_RECEIVESTOCK + " text, " +
                SalesStockModel.XML_DELVSTOCK + " text, " +
                SalesStockModel.XML_INTRANSIT + " text, " +
                SalesStockModel.XML_OTHERSTOCK + " text, " +
                SalesStockModel.XML_ENDINGSTOCK + " text, " +
                SalesStockModel.XML_PABRIK + " text, " +
                SalesStockModel.XML_OUTSTANDING + " text, " +
                SalesStockModel.XML_CREATEDDATE + " text " +
                ")";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int versionOld, int versionNew) {
        String query;
        try {
            switch (versionOld) {
                case 14:
                    // TAMBAH APP DETAIL PERMISSION
                    query = "ALTER TABLE " + SalesDataModel.TABLE_NAME +
                            " ADD " +
                            SalesDataModel.XML_gjahr + " text ";
                    db.execSQL(query);
                    query = "ALTER TABLE " + SalesDataModel.TABLE_NAME +
                            " ADD " +
                            SalesDataModel.XML_monat + " text ";
                    db.execSQL(query);
                    break;
                case 15:
                    query = "create table " + PriceDataModel.TABLE_NAME +
                            "(" +
                            PriceDataModel.XML_ID + " text, " +
                            PriceDataModel.XML_MATERIAL + " text, " +
                            PriceDataModel.XML_DESCRIPTION + " text, " +
                            PriceDataModel.XML_PRICE + " text," +
                            PriceDataModel.XML_DATE + " text, " +
                            PriceDataModel.XML_GROUPPRODUCT + " text," +
                            PriceDataModel.XML_SUBGROUP + " text " +
                            ")";
                    db.execSQL(query);
                    break;
                case 16:
                    query = "create table " + SalesStockModel.TABLE_NAME +
                            "(" +
                            SalesStockModel.XML_ID + " INTEGER, " +
                            SalesStockModel.XML_COMPANY + " text, " +
                            SalesStockModel.XML_AREA + " text, " +
                            SalesStockModel.XML_PLANT + " text, " +
                            SalesStockModel.XML_MATERIAL + " text, " +
                            SalesStockModel.XML_COMPANYNAME + " text, " +
                            SalesStockModel.XML_PLANTNAME + " text, " +
                            SalesStockModel.XML_BEGINSTOCK + " text, " +
                            SalesStockModel.XML_RECEIVESTOCK + " text, " +
                            SalesStockModel.XML_DELVSTOCK + " text, " +
                            SalesStockModel.XML_INTRANSIT + " text, " +
                            SalesStockModel.XML_OTHERSTOCK + " text, " +
                            SalesStockModel.XML_ENDINGSTOCK + " text, " +
                            SalesStockModel.XML_PABRIK + " text, " +
                            SalesStockModel.XML_OUTSTANDING + " text, " +
                            SalesStockModel.XML_CREATEDDATE + " text " +
                            ")";
                    db.execSQL(query);
                case 17:
                    // TAMBAH APP DETAIL PERMISSION
                    query = "ALTER TABLE " + PriceDataModel.TABLE_NAME +
                            " ADD " +
                            PriceDataModel.XML_PRICETYPE + " text ";
                    db.execSQL(query);
                    break;
                case 18:
                    // TAMBAH CURRENCY pada Price
                    query = "ALTER TABLE " + PriceDataModel.TABLE_NAME +
                            " ADD " +
                            PriceDataModel.XML_CURRENCY + " text ";
                    db.execSQL(query);
                    break;
            }

        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }


}