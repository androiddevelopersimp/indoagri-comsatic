package com.indoagri.comsatic.Retrofit.DB;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.widget.Toast;


import com.indoagri.comsatic.Retrofit.Model.AppDetailModel;
import com.indoagri.comsatic.Retrofit.Model.GroupCompanyModel;
import com.indoagri.comsatic.Retrofit.Model.GroupProductModel;
import com.indoagri.comsatic.Retrofit.Model.PriceDataModel;
import com.indoagri.comsatic.Retrofit.Model.SalesBilling;
import com.indoagri.comsatic.Retrofit.Model.SalesStockModel;
import com.indoagri.comsatic.Retrofit.Model.UserModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DatabaseQuery {
    private Context context;
    private SQLiteDatabase db;
    private DatabaseHelper helper;

    public DatabaseQuery(Context context) {
        this.context = context;
        this.helper = new DatabaseHelper(context);
    }

    public void openTransaction() throws SQLException {
        try {
            db = helper.getWritableDatabase();
            db.beginTransaction();
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public void closeTransaction() {
        try {
            if (db.isOpen()) {
                db.endTransaction();
                helper.close();
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public void commitTransaction() throws SQLException {
        try {
            db.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public int COUNT(String TABLE, String statement) {
        int count = 0;
        db = helper.getReadableDatabase();
        Cursor cursor = null;
        String query = statement;

        cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        count = cursor.getCount();
        cursor.close();

        return count;
    }

    public void UPDATEQUERY(String TABLE, String statement) {
        db = helper.getWritableDatabase();
        db.beginTransaction();
        String query = statement;
        try {
            db.execSQL(query);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
            //Error in between database transaction
        } finally {
            db.endTransaction();
        }

    }

    public void INSERTQUERY(String TABLE, String statement) {
        db = helper.getWritableDatabase();
        db.beginTransaction();
        String query = statement;
        try {
            db.execSQL(query);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.getMessage();
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
            //Error in between database transaction
        } finally {
            db.endTransaction();
        }
    }

    public int insertDataSQL(String tableName, ContentValues values) {

        int result = 0;
        try {
            /*sqliteDatabase.rawQuery(query,bindArgs);*/
            db.insertOrThrow(tableName, null, values);
            result = 1;
        } catch (Exception e) {
            result = 0;
            e.printStackTrace();
        }

        return result;
    }

    public boolean DELETEEXISTING(String TABLE, String statement) {
        //openTransaction();
        db = helper.getWritableDatabase();
        db.beginTransaction();
        boolean deleteStatus = false;
        String query = statement;
        try {
            db.execSQL(query);
            deleteStatus = true;
        } catch (Exception e) {
            deleteStatus = false;
            e.getMessage();
        }
        try {
            db.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        try {
            if (db.isOpen()) {
                db.endTransaction();
                helper.close();
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        return deleteStatus;
    }

    public boolean DELETETABLE(String TABLE, String statement) {
        db = helper.getWritableDatabase();
        db.beginTransaction();
        boolean in = false;
        String query = statement;
        try {
            db.execSQL(query);
            in = true;
            db.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
            in = false;
        }
        try {
            if (db.isOpen()) {
                db.endTransaction();
                helper.close();
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        return in;
    }

    public int deleteDataTemporary(String tableName, String whereClause, String[] whereArgs) {

        try {
            int rowId = db.delete(tableName, whereClause, whereArgs);
            return rowId;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    public void deleteDataTable(String tableName, String whereClause, String[] whereArgs) {

        try {
            db.execSQL("DELETE FROM sqlite_sequence WHERE name= '" + tableName + "'");
            //  db.execSQL("VACUUM");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteDataFromTable(String tableName, String whereClause, String[] whereArgs) {
        try {
            db.execSQL("DELETE FROM " + tableName);
            //  db.execSQL("VACUUM");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public HashMap<String, String> GetUserField() {
        HashMap<String, String> hard = new HashMap<String, String>();
        db = helper.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + UserModel.TABLE_NAME;
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {

            hard.put(UserModel.XML_ID, cursor.getString(0));
            hard.put(UserModel.XML_USERID, cursor.getString(1));
            hard.put(UserModel.XML_USERNAME, cursor.getString(2));
            hard.put(UserModel.XML_SURNAME, cursor.getString(3));
            hard.put(UserModel.XML_EMAIL, cursor.getString(4));
            hard.put(UserModel.XML_LDAP, cursor.getString(5));
            hard.put(UserModel.XML_DOMAIN, cursor.getString(6));
            hard.put(UserModel.XML_USERDESCRIPTION, cursor.getString(7));
            hard.put(UserModel.XML_COMPANY, cursor.getString(8));
            hard.put(UserModel.XML_PROVINCE, cursor.getString(9));
            hard.put(UserModel.XML_DEPARTMENT, cursor.getString(10));
            hard.put(UserModel.XML_EMPLOYEEID, cursor.getString(11));
            hard.put(UserModel.XML_USERFULLNAME, cursor.getString(12));
            hard.put(UserModel.XML_USERPHONE, cursor.getString(13));
            hard.put(UserModel.XML_USERPASSWORD, cursor.getString(14));
            hard.put(UserModel.XML_USERKEY, cursor.getString(15));
        }
        cursor.close();
        return hard;
    }


    public Object getDataFirstRaw(String sqldb_query, String tableName, String[] a) {

        List<Object> listObject = getListDataRawQuery(sqldb_query, tableName, a);

        if (listObject.size() > 0) {
            return listObject.get(0);
        } else {
            return null;
        }
    }

    public List<Object> getListDataRawQuery(String sqldb_query, String tableName, String[] a) {

        Cursor cursor = null;
        List<Object> listObject = new ArrayList<Object>();

        try {
            cursor = db.rawQuery(sqldb_query, a);
            if (tableName.equals(SalesBilling.TABLE_NAME)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        SalesBilling sb = new SalesBilling();
                        for (int i = 0; i < cursor.getColumnCount(); i++) {
                            if (cursor.getColumnName(i).equalsIgnoreCase(SalesBilling.XML_COMPANY)) {
                                sb.setCompany(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(SalesBilling.XML_PRODUCT)) {
                                sb.setProduct(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(SalesBilling.XML_YEAR)) {
                                sb.setYear(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(SalesBilling.XML_PERIODE)) {
                                sb.setPeriode(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(SalesBilling.XML_TIPE)) {
                                sb.setTipe(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(SalesBilling.XML_QTY)) {
                                sb.setQty(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(SalesBilling.XML_VALUE)) {
                                sb.setValue(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(SalesBilling.XML_PRICE)) {
                                sb.setPrice(cursor.getString(i));
                            }


                        }

                        listObject.add(sb);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(GroupCompanyModel.TABLE_NAME)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        GroupCompanyModel sb = new GroupCompanyModel();
                        for (int i = 0; i < cursor.getColumnCount(); i++) {
                            if (cursor.getColumnName(i).equalsIgnoreCase(GroupCompanyModel.XML_groupcompany)) {
                                sb.setGroupcompany(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(GroupCompanyModel.XML_companycode)) {
                                sb.setCompanycode(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(GroupCompanyModel.XML_description)) {
                                sb.setDescription(cursor.getString(i));
                            }
                        }

                        listObject.add(sb);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(GroupProductModel.TABLE_NAME)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        GroupProductModel sb = new GroupProductModel();
                        for (int i = 0; i < cursor.getColumnCount(); i++) {
                            if (cursor.getColumnName(i).equalsIgnoreCase(GroupProductModel.XML_groupproduct)) {
                                sb.setGroupproduct(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(GroupProductModel.XML_subgroup)) {
                                sb.setSubgroup(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(GroupProductModel.XML_material)) {
                                sb.setMaterial(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(GroupProductModel.XML_materialDescription)) {
                                sb.setMaterialdescription(cursor.getString(i));
                            }
                        }

                        listObject.add(sb);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(AppDetailModel.TABLE_NAME)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        AppDetailModel sb = new AppDetailModel();
                        for (int i = 0; i < cursor.getColumnCount(); i++) {
                            if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_APPID)) {
                                sb.setID(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_APPNAME)) {
                                sb.setAPPNAME(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_APPDESC)) {
                                sb.setAPPDESCRIPTION(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_APPPACKAGE)) {
                                sb.setAPPPACKAGE(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_VERSIONCODE)) {
                                sb.setVERSIONCODE(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_VERSIONNAME)) {
                                sb.setVERSIONNAME(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_APKURL)) {
                                sb.setAPKURL(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_ASSETSICON)) {
                                sb.setASSETSICON(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_ASSETSICONURL)) {
                                sb.setASSETSICONURL(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_ACTIVE)) {
                                sb.setACTIVE(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_GROUPLEVEL)) {
                                sb.setGROUPLEVEL(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_USERAD)) {
                                sb.setUSERAD(cursor.getString(i));
                            }
                        }

                        listObject.add(sb);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(UserModel.TABLE_NAME)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        UserModel sb = new UserModel();
                        for (int i = 0; i < cursor.getColumnCount(); i++) {
                            if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERID)) {
                                sb.setUSERID(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERNAME)) {
                                sb.setUSERNAME(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_SURNAME)) {
                                sb.setSURNAME(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_EMAIL)) {
                                sb.setEMAIL(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_LDAP)) {
                                sb.setLDAP(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_DOMAIN)) {
                                sb.setDOMAIN(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERDESCRIPTION)) {
                                sb.setDESCRIPTION(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_COMPANY)) {
                                sb.setCOMPANY(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_PROVINCE)) {
                                sb.setPROVINCE(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_DEPARTMENT)) {
                                sb.setDEPARTMENT(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_EMPLOYEEID)) {
                                sb.setEMPLOYEEID(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERFULLNAME)) {
                                sb.setFULLNAME(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERPHONE)) {
                                sb.setPHONE(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERPASSWORD)) {
                                sb.setPASSWORD(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERKEY)) {
                                sb.setKEY(cursor.getString(i));
                            }
                        }

                        listObject.add(sb);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(PriceDataModel.TABLE_NAME)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        PriceDataModel sb = new PriceDataModel();
                        for (int i = 0; i < cursor.getColumnCount(); i++) {
                            if (cursor.getColumnName(i).equalsIgnoreCase(PriceDataModel.XML_ID)) {
                                sb.setId(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(PriceDataModel.XML_MATERIAL)) {
                                sb.setMaterial(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(PriceDataModel.XML_DESCRIPTION)) {
                                sb.setDescription(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(PriceDataModel.XML_PRICE)) {
                                sb.setPrice(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(PriceDataModel.XML_DATE)) {
                                sb.setDate(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(PriceDataModel.XML_GROUPPRODUCT)) {
                                sb.setGrouproduct(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(PriceDataModel.XML_SUBGROUP)) {
                                sb.setSubgroup(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(PriceDataModel.XML_PRICETYPE)) {
                                sb.setPricetype(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(PriceDataModel.XML_CURRENCY)) {
                                sb.setCurrency(cursor.getString(i));
                            }
                        }

                        listObject.add(sb);
                    } while (cursor.moveToNext());
                }
            } else if (tableName.equals(SalesStockModel.TABLE_NAME)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        SalesStockModel sb = new SalesStockModel();
                        for (int i = 0; i < cursor.getColumnCount(); i++) {
                            if (cursor.getColumnName(i).equalsIgnoreCase(SalesStockModel.XML_ID)) {
                                sb.setId(cursor.getInt(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(SalesStockModel.XML_COMPANY)) {
                                sb.setCompany(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(SalesStockModel.XML_AREA)) {
                                sb.setArea(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(SalesStockModel.XML_PLANT)) {
                                sb.setPlant(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(SalesStockModel.XML_MATERIAL)) {
                                sb.setMaterial(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(SalesStockModel.XML_COMPANYNAME)) {
                                sb.setCompanyname(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(SalesStockModel.XML_PLANTNAME)) {
                                sb.setPlantname(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(SalesStockModel.XML_BEGINSTOCK)) {
                                sb.setBegiNSTOCK(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(SalesStockModel.XML_RECEIVESTOCK)) {
                                sb.setReceivESTOCK(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(SalesStockModel.XML_DELVSTOCK)) {
                                sb.setDelVSTOCK(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(SalesStockModel.XML_INTRANSIT)) {
                                sb.setINTRANSIT(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(SalesStockModel.XML_OTHERSTOCK)) {
                                sb.setOtheRSTOCK(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(SalesStockModel.XML_ENDINGSTOCK)) {
                                sb.setEndinGSTOCK(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(SalesStockModel.XML_PABRIK)) {
                                sb.setPabrik(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(SalesStockModel.XML_OUTSTANDING)) {
                                sb.setOutstanding(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(SalesStockModel.XML_CREATEDDATE)) {
                                sb.setCreateddate(cursor.getString(i));
                            }
                        }
                        listObject.add(sb);
                    } while (cursor.moveToNext());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }

        return listObject;
    }


    public long setData(Object object) {
        String tableName = null;
        ContentValues values = new ContentValues();
        if (object.getClass().getName().equals(SalesBilling.class.getName())) {
            SalesBilling md = (SalesBilling) object;
            tableName = SalesBilling.TABLE_NAME;
            values.put(SalesBilling.XML_COMPANY, md.getCompany());
            values.put(SalesBilling.XML_PRODUCT, md.getProduct());
            values.put(SalesBilling.XML_YEAR, md.getYear());
            values.put(SalesBilling.XML_PERIODE, md.getPeriode());
            values.put(SalesBilling.XML_TIPE, md.getTipe());
            values.put(SalesBilling.XML_QTY, md.getQty());
            values.put(SalesBilling.XML_VALUE, md.getValue());
            values.put(SalesBilling.XML_PRICE, md.getPrice());
        }

        long rowId = 0;

        try {
            rowId = db.insertOrThrow(tableName, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rowId;
    }


}