package com.indoagri.comsatic.Retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indoagri.comsatic.Retrofit.Model.SalesStockModel;

import java.util.List;

public class StockResponse {

    @SerializedName("salesStock")
    @Expose
    private List<SalesStockModel> salesStock = null;

    public List<SalesStockModel> getSalesStock() {
        return salesStock;
    }

    public void setSalesStock(List<SalesStockModel> salesStock) {
        this.salesStock = salesStock;
    }

}
