package com.indoagri.comsatic.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.indoagri.comsatic.R;
import com.indoagri.comsatic.Retrofit.DB.DatabaseQuery;
import com.indoagri.comsatic.Retrofit.Model.GroupCompanyModel;
import com.indoagri.comsatic.Retrofit.Model.GroupProductModel;
import com.indoagri.comsatic.Retrofit.SharePreference;
import com.indoagri.comsatic.SalesBilling.SalesBilling;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FormSearching.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FormSearching#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FormSearching extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    View views;
    private OnFragmentInteractionListener mListener;
    private OnSubProductListener mProductListener;
    private OnCompanyListener mCompanyListener;
    Button btnSearch;
    DatabaseQuery query;
    Spinner spinnerGroupCompany;
    Spinner spinnerSubGroupCompany;
    Spinner spinnerProduct;
    Spinner spinnerSubProduct;
    EditText et_year,et_subproducts,et_company;
    int year = Calendar.getInstance().get(Calendar.YEAR);
    String dataYear;
    public FormSearching() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FormSearching.
     */
    // TODO: Rename and change types and number of parameters
    public static FormSearching newInstance(String param1, String param2) {
        FormSearching fragment = new FormSearching();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        query = new DatabaseQuery(getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        views = inflater.inflate(R.layout.fragment_form_searching, container, false);
        btnSearch = views.findViewById(R.id.btn_search);
        btnSearch.setOnClickListener(this);
        spinnerGroupCompany = views.findViewById(R.id.spinGroupCompany);
        spinnerSubGroupCompany = views.findViewById(R.id.spinCompany);
        spinnerProduct = views.findViewById(R.id.spinProduct);
        spinnerSubProduct = views.findViewById(R.id.spinSubProduct);
        et_year = views.findViewById(R.id.et_year);
        et_year.setText(String.valueOf(year));
        et_subproducts = views.findViewById(R.id.et_subProducts);
        et_company = views.findViewById(R.id.et_company);
        new SharePreference(getActivity()).setFormYear(String.valueOf(year));
        et_year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showYearDialog();
            }
        });

        // Inflate the layout for this fragment
        DataSourceCompany();
        DataSourceProduct();
//        ((SalesBilling)getActivity()).setFragmentRefreshListener(new SalesBilling.FragmentRefreshListener() {
//            @Override
//            public void onRefresh() {
//                if(!new SharePreference(getActivity()).isFormSubProduct().equals("")) {
//                    et_subproducts.setText(new SharePreference(getActivity()).isFormSubProduct());
//                }else{
//                    et_subproducts.setHint("Sub Products");
//                }
//                if(!new SharePreference(getActivity()).isFormCompanyDesc().equals("")) {
//                    et_company.setText(new SharePreference(getActivity()).isFormCompanyDesc());
//                }else{
//                    et_company.setHint("ALL");
//                }
//
//            }
//        });
        ((SalesBilling)getActivity()).setFragmentRefreshListener(new SalesBilling.FragmentRefreshListener() {
            @Override
            public void onRefresh() {
                if(!new SharePreference(getActivity()).isFormSubProduct().equals("")) {
                    et_subproducts.setText(new SharePreference(getActivity()).isFormSubProduct());
                }else{
                    et_subproducts.setHint("Sub Products");
                }
                if(!new SharePreference(getActivity()).isFormCompanyDesc().equals("")) {
                    et_company.setText(new SharePreference(getActivity()).isFormCompanyDesc());
                }else{
                    et_company.setHint("ALL");
                }
            }
        });
            et_subproducts.setHint("Sub Products");
            et_company.setHint("ALL");
        return views;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String year) {
        if (mListener != null) {
            mListener.onFragmentInteraction(year);
        }
    }
    public void onSubProduct(String product) {
        if (mProductListener != null) {
            mProductListener.onFragmentInteractionSubProducts(product);
        }
    }
    public void onCompany(String group) {
        if (mCompanyListener != null) {
            mCompanyListener.onFragmentInteractionCompany(group);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
            mProductListener = (OnSubProductListener) context;
            mCompanyListener = (OnCompanyListener)context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mProductListener = null;
        mCompanyListener = null;
    }

    @Override
    public void onClick(View v) {
        if(v==btnSearch){
            getActivity().onBackPressed();
            onButtonPressed( new SharePreference(getActivity()).isFormYear());
        }
        if(v==et_subproducts){
            onSubProduct( new SharePreference(getActivity()).isFormProduct());
        }
        if(v==et_company){
            onCompany( new SharePreference(getActivity()).isFormGroupCompany());
        }
    }
    private void setUpSpinnerCOmpany(List<Object>list){
        List<String> company = new ArrayList<String>();
        for(int i=0;i<list.size();i++){

            GroupCompanyModel groupCompanyModel =  (GroupCompanyModel) list.get(i);
            company.add(groupCompanyModel.getGroupcompany());
        }
        ArrayAdapter<String> dtCOmpany = new ArrayAdapter<String>(getActivity(), R.layout.item_spinner_sales, company);
        dtCOmpany.setDropDownViewResource(R.layout.spinner_dropdown);
        // attaching data adapter to spinner
        spinnerGroupCompany.setAdapter(dtCOmpany);
        spinnerGroupCompany.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView1, View selectedItemView, int position, long id) {
                // your code here
                String dataCompany = parentView1.getItemAtPosition(position).toString();
                new SharePreference(getActivity()).setFormGroupcompany(dataCompany);
                activeEtCompany();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    private void setUpSpinnerSubCompany(List<Object>list){
        List<String> company = new ArrayList<String>();
        company.add("ALL");
        for(int i=0;i<list.size();i++){

            GroupCompanyModel groupCompanyModel =  (GroupCompanyModel) list.get(i);
            company.add(groupCompanyModel.getDescription().trim());
        }
        ArrayAdapter<String> dtCOmpany = new ArrayAdapter<String>(getActivity(), R.layout.item_spinner_sales, company);
        dtCOmpany.setDropDownViewResource(R.layout.spinner_dropdown);
        // attaching data adapter to spinner
        spinnerSubGroupCompany.setAdapter(dtCOmpany);
        spinnerSubGroupCompany.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView1, View selectedItemView, int position, long id) {
                // your code here
                String dataCompany = parentView1.getItemAtPosition(position).toString();
                //new SharePreference(getActivity()).setFormCompany(dataCompany);
                if(dataCompany.equalsIgnoreCase("ALL")){
                    new SharePreference(getActivity()).setFormCompany("");
                    new SharePreference(getActivity()).setFormCompanyDesc("");
                }else{
                    new SharePreference(getActivity()).setFormCompanyDesc(dataCompany);
                    query.openTransaction();
                    String[] a = new String[1];
                    a[0] = dataCompany;
                    String queryString = "select * from T_GROUPCOMPANY where description=?";
                    Object object = query.getDataFirstRaw(queryString,GroupCompanyModel.TABLE_NAME,a);
                    GroupCompanyModel hasil = (GroupCompanyModel)object;
                    query.closeTransaction();
                    new SharePreference(getActivity()).setFormCompany(hasil.getCompanycode());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

        private void DataSourceCompany(){
            query.openTransaction();
            List<GroupCompanyModel> listTemp = new ArrayList<GroupCompanyModel>();
            List<Object> listObject;
            String sqldb_query = "SELECT groupCompany,companyCode,description " +
                    "FROM T_GROUPCOMPANY group by groupCompany order by groupCompany desc ";
                listObject = query.getListDataRawQuery(sqldb_query,GroupCompanyModel.TABLE_NAME,null);
            query.closeTransaction();
            if(listObject.size() > 0){
                for(int i = 0; i < listObject.size(); i++){
                    GroupCompanyModel blk = (GroupCompanyModel) listObject.get(i);
                    listTemp.add(blk);

                }
                setUpSpinnerCOmpany(listObject);
            }
            else{
                //Toast.makeText(getActivity(),"Data TIdak ada",Toast.LENGTH_SHORT).show();
            }
        }

        private void DataSourceSubCompany(String Group){
            query.openTransaction();
            List<GroupCompanyModel> listTemp2 = new ArrayList<GroupCompanyModel>();
            List<Object> listObject2;
            String[] a = new String[1];
            a[0] = Group;
            String sqldb_query = "select groupCompany,companyCode,description from T_GROUPCOMPANY where groupCompany=?";
            listObject2 = query.getListDataRawQuery(sqldb_query,GroupCompanyModel.TABLE_NAME,a);
            query.closeTransaction();
            if(listObject2.size() > 0){
                for(int i = 0; i < listObject2.size(); i++){
                    GroupCompanyModel blk = (GroupCompanyModel) listObject2.get(i);
                    listTemp2.add(blk);
                }
                setUpSpinnerSubCompany(listObject2);
            }
            else{
                //Toast.makeText(getActivity(),"Data TIdak ada",Toast.LENGTH_SHORT).show();
            }
        }


        private void setUpSpinnerProduct(List<Object>list) {
            List<String> product = new ArrayList<String>();
            product.add("ALL");
            for (int i = 0; i < list.size(); i++) {

                GroupProductModel groupProductModel = (GroupProductModel) list.get(i);
                product.add(groupProductModel.getGroupproduct());
            }
            ArrayAdapter<String> dtCOmpany = new ArrayAdapter<String>(getActivity(), R.layout.item_spinner_sales, product);
            dtCOmpany.setDropDownViewResource(R.layout.spinner_dropdown);
            // attaching data adapter to spinner
            spinnerProduct.setAdapter(dtCOmpany);
            spinnerProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView1, View selectedItemView, int position, long id) {
                    // your code here
                    String dataProduct = parentView1.getItemAtPosition(position).toString();
                    if(dataProduct.equalsIgnoreCase("ALL")){
                        new SharePreference(getActivity()).setFormProduct("");
                        disableEt();
                    }else{
                        new SharePreference(getActivity()).setFormProduct(dataProduct);
                        activeEt();
                    }
                      //DataSourceSubProduct(dataProduct);

                }
                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    // your code here
                }

            });
        }
    private void setUpSpinnerSubProduct(List<Object>list) {
        List<String> product = new ArrayList<String>();
        product.add("ALL");
        for (int i = 0; i < list.size(); i++) {
            GroupProductModel groupProductModel = (GroupProductModel) list.get(i);
            product.add(groupProductModel.getSubgroup());
        }
        ArrayAdapter<String> dtCOmpany = new ArrayAdapter<String>(getActivity(), R.layout.item_spinner_sales, product);
        dtCOmpany.setDropDownViewResource(R.layout.spinner_dropdown);
        // attaching data adapter to spinner
        spinnerSubProduct.setAdapter(dtCOmpany);
        spinnerSubProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView1, View selectedItemView, int position, long id) {
                // your code here
                String dataProduct = parentView1.getItemAtPosition(position).toString();
                new SharePreference(getActivity()).setFormSubproduct(dataProduct);
              //  Toast.makeText(getActivity(), dataProduct, Toast.LENGTH_SHORT).show();
                //  DataSourceSubCompany(dataCompany);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

        private void DataSourceProduct(){
            query.openTransaction();
            List<GroupProductModel> listTemp3 = new ArrayList<GroupProductModel>();
            List<Object> listObject3;

            String sqldb_query = "select groupproduct,subgroup,material,materialdescription from T_GROUPPRODUCT group by groupproduct";
            listObject3 = query.getListDataRawQuery(sqldb_query,GroupProductModel.TABLE_NAME,null);
            query.closeTransaction();
            if(listObject3.size() > 0){
                for(int i = 0; i < listObject3.size(); i++){
                    GroupProductModel blk = (GroupProductModel) listObject3.get(i);
                    listTemp3.add(blk);
                }

                setUpSpinnerProduct(listObject3);
            }
            else{
            //    Toast.makeText(getActivity(),"Data TIdak ada",Toast.LENGTH_SHORT).show();
            }
        }

        private void DataSourceSubProduct(String Group){
            query.openTransaction();
            List<GroupProductModel> listTemp4 = new ArrayList<GroupProductModel>();
            List<Object> listObject4;
            String[] a = new String[1];
            a[0] = Group;
            String sqldb_query = "select groupproduct,subgroup,material,materialdescription from T_GROUPPRODUCT where groupProduct=?";
            listObject4 = query.getListDataRawQuery(sqldb_query,GroupProductModel.TABLE_NAME,a);
            query.closeTransaction();
            if(listObject4.size() > 0){
                for(int i = 0; i < listObject4.size(); i++){
                    GroupProductModel blk = (GroupProductModel) listObject4.get(i);
                    listTemp4.add(blk);
                }
                setUpSpinnerSubProduct(listObject4);
            }
            else{
         //       Toast.makeText(getActivity(),"Data TIdak ada",Toast.LENGTH_SHORT).show();
             //   new SharePreference(getActivity()).setFormSubproduct(dataProduct);
            }
        }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String year);
    }
    public interface OnSubProductListener {
        // TODO: Update argument type and name
        void onFragmentInteractionSubProducts(String Group);
    }
    public interface OnCompanyListener {
        // TODO: Update argument type and name
        void onFragmentInteractionCompany(String Group);
    }

    public void showYearDialog()
    {

        final Dialog d = new Dialog(getActivity());
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.dialog_year);
        Button set = (Button) d.findViewById(R.id.btnOk);
        Button cancel = (Button) d.findViewById(R.id.btnCancel);
        TextView year_text=(TextView)d.findViewById(R.id.year_text);
        year_text.setText("Year");
        final NumberPicker yearpicker = (NumberPicker) d.findViewById(R.id.picker_year);

        yearpicker.setMinValue(2016);
        yearpicker.setMaxValue(2025);
        yearpicker.setWrapSelectorWheel(false);
        yearpicker.setValue(year);

        set.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                et_year.setText(String.valueOf(yearpicker.getValue()));
                new SharePreference(getActivity()).setFormYear(String.valueOf(yearpicker.getValue()));
                dataYear=String.valueOf(yearpicker.getValue());
                d.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }

    void activeEtCompany(){
        et_company.setOnClickListener(this);
    }
    void disableEtCompany(){
        et_company.setOnClickListener(null);
        et_company.setHint("ALL");
    }
    void activeEt(){
        et_subproducts.setOnClickListener(this);
    }
    void disableEt(){
        et_subproducts.setOnClickListener(null);
        et_subproducts.setHint("Sub Products");
    }
}
