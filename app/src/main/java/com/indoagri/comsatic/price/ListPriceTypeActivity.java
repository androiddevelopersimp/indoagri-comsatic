package com.indoagri.comsatic.price;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.indoagri.comsatic.R;
import com.indoagri.comsatic.Retrofit.DB.DatabaseQuery;
import com.indoagri.comsatic.Retrofit.Model.GroupProductModel;
import com.indoagri.comsatic.Retrofit.Model.PriceDataModel;
import com.indoagri.comsatic.Retrofit.SharePreference;

import java.util.ArrayList;
import java.util.List;

public class ListPriceTypeActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, TextWatcher {

    Bundle extras;
    String extrasString;
    Toolbar toolbar;
    TextView mTextToolbar, txtSetAll;
    ListView listView;
    EditText etSearch;
    DatabaseQuery query;
    SharePreference sharedPreferences;
    List<PriceDataModel> priceDataModelList;
    ListPriceTypeAdapter listPriceTypeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_price_type);
        setUpToolbar();
        sharedPreferences = new SharePreference(ListPriceTypeActivity.this);
        query = new DatabaseQuery(ListPriceTypeActivity.this);
        listView = (ListView) findViewById(R.id.listView);
        etSearch = findViewById(R.id.et_search);
        etSearch.setVisibility(View.VISIBLE);
        txtSetAll = (findViewById(R.id.txt_setAll));
        txtSetAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String subproduct = "";
                Intent intent = new Intent();
//                intent.putExtra("PRICETYPE",subproduct);
                intent.putExtra("CODE", "All");
                intent.putExtra("DESC", "All");
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        extras = getIntent().getExtras();
        if (extras != null) {
            extrasString = extras.getString("Pricetype");
            init();
            // and get whatever type user account id is
            //Toast.makeText(this, extrasString+" || "+new SharePreference(ListSubProduct.this).isFormProduct(), Toast.LENGTH_SHORT).show();
        } else {
            init();
        }

    }

    private void setUpToolbar() {
        toolbar = (findViewById(R.id.toolbar));
        mTextToolbar = (findViewById(R.id.txt_Toolbar));
        mTextToolbar.setText("List Price Type");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    void init() {
        priceDataModelList = new ArrayList<PriceDataModel>();
        etSearch.addTextChangedListener(this);
        listView.setOnItemClickListener(this);
        listPriceTypeAdapter = new ListPriceTypeAdapter(this, R.layout.item_listcontent, priceDataModelList);
        listView.setAdapter(listPriceTypeAdapter);
        listView.setScrollingCacheEnabled(false);
        String material = getIntent().getStringExtra("material");
        if (material != null)
            setHalaman(material);

    }

    private void setHalaman(String material) {
        etSearch.addTextChangedListener(this);
        List<Object> listObject;
        query.openTransaction();
        String[] a = new String[1];
        String sqldb_query = "select PRICETYPE from Table_price WHERE MATERIAL = ? group by PRICETYPE";
        a[0] = material;
        listObject = query.getListDataRawQuery(sqldb_query, PriceDataModel.TABLE_NAME, a);
        query.closeTransaction();
        if (listObject.size() > 0) {
            for (int i = 0; i < listObject.size(); i++) {
                PriceDataModel hasil = (PriceDataModel) listObject.get(i);
                priceDataModelList.add(hasil);
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        listPriceTypeAdapter.getFilter().filter(s);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
        PriceDataModel subProductAdapterItem = (PriceDataModel) listPriceTypeAdapter.getItem(i);
        String CODE = subProductAdapterItem.getPricetype();
        String DESC = subProductAdapterItem.getPricetype();
        Intent intent = new Intent();
        intent.putExtra("CODE", CODE);
        intent.putExtra("DESC", DESC);
        setResult(RESULT_OK, intent);
        finish();
    }
}
