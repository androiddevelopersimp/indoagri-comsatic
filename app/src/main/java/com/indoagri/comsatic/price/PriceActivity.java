package com.indoagri.comsatic.price;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.indoagri.comsatic.MainActivity;
import com.indoagri.comsatic.R;

import org.w3c.dom.Text;

public class PriceActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView title = (TextView) findViewById(R.id.title_toolbar);
        title.setText("Price");

        Button btnReportPrice = (Button) findViewById(R.id.btn_reportprice);
        Button btnGrafikPrice = (Button) findViewById(R.id.btn_grafikprice);

        btnReportPrice.setOnClickListener(this);
        btnGrafikPrice.setOnClickListener(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_reportprice:
                Intent b = new Intent(PriceActivity.this, ReportActivity.class);
                startActivity(b);
                break;
            case R.id.btn_grafikprice:
                Intent c = new Intent(PriceActivity.this, GrafikActivity.class);
                startActivity(c);
                break;
        }
    }
}
