package com.indoagri.comsatic.price;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.indoagri.comsatic.R;
import com.indoagri.comsatic.Retrofit.Model.GroupProductModel;

import java.util.ArrayList;
import java.util.List;

public class ListProductAdapter extends ArrayAdapter<GroupProductModel> {

    List<GroupProductModel> productModelList, lst_temp;
    Context context;
    int layout;

    public ListProductAdapter(Context context, int layout, List<GroupProductModel> uadEstateModelList) {
        super(context, layout, uadEstateModelList);
        this.context = context;
        this.layout = layout;
        this.productModelList = uadEstateModelList;
    }


    @Override
    public View getView(int position, View v, ViewGroup parent) {
        GroupProductModel student = productModelList.get(position);
        StudentHolder holder;
        if (v == null) {
            LayoutInflater vi = ((Activity) context).getLayoutInflater();
            //  v=vi.inflate(layout, parent,false);
            v = vi.inflate(layout, null);
            holder = new StudentHolder();
            holder.imageView = (ImageView) v.findViewById(R.id.imgClick);
            holder.code = (TextView) v.findViewById(R.id.txtestateCode);
            holder.name = (TextView) v.findViewById(R.id.txtestateName);
            v.setTag(holder);
        } else {
            holder = (StudentHolder) v.getTag();
            holder.imageView = (ImageView) v.findViewById(R.id.imgClick);
            holder.code = (TextView) v.findViewById(R.id.txtestateCode);
            holder.name = (TextView) v.findViewById(R.id.txtestateName);
        }
        holder.code.setText(student.getMaterial() + " | ");
        holder.name.setText(student.getMaterialdescription());
        return v;
    }

    static class StudentHolder {
        ImageView imageView;
        TextView code;
        TextView name;
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                List<GroupProductModel> filteredList = new ArrayList<>();
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredList.addAll(lst_temp);
                } else {
                    if (lst_temp == null) {
                        lst_temp = new ArrayList<GroupProductModel>(productModelList);
                    }
                    for (GroupProductModel row : lst_temp) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getMaterial().toLowerCase().contains(charString.toLowerCase()) || row.getMaterialdescription().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

//                    productModelList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
//                filterResults.values = productModelList;
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
//                productModelList = (ArrayList<GroupProductModel>) filterResults.values;

                // refresh the list with filtered data
//                notifyDataSetChanged();

//                clear();
//                int count = productModelList.size();
//                for (int i = 0; i < count; i++) {
//                    add(productModelList.get(i));
//                    notifyDataSetInvalidated();
//                }

                productModelList.clear();
                productModelList.addAll((List) filterResults.values);
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getCount() {
        return productModelList.size();
    }


    @Override
    public long getItemId(int pos) {
        return pos;
    }

}