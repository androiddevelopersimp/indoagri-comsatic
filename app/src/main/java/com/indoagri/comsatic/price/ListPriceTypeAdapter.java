package com.indoagri.comsatic.price;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.indoagri.comsatic.R;
import com.indoagri.comsatic.Retrofit.Model.GroupProductModel;
import com.indoagri.comsatic.Retrofit.Model.PriceDataModel;

import java.util.ArrayList;
import java.util.List;

public class ListPriceTypeAdapter extends ArrayAdapter<PriceDataModel> {

    List<PriceDataModel> priceDataModelList,lst_temp;
    Context context;
    int layout;

    public ListPriceTypeAdapter(Context context, int layout, List<PriceDataModel> priceDataModels) {
        super(context, layout, priceDataModels);
        this.context=context;
        this.layout=layout;
        this.priceDataModelList=priceDataModels;
    }


    @Override
    public View getView(int position, View v, ViewGroup parent) {
        PriceDataModel student=priceDataModelList.get(position);
        StudentHolder holder;
        if(v==null){
            LayoutInflater vi=((Activity)context).getLayoutInflater();
            //  v=vi.inflate(layout, parent,false);
            v = vi.inflate(layout, null);
            holder=new StudentHolder();
            holder.imageView=(ImageView) v.findViewById(R.id.imgClick);
            holder.code=(TextView) v.findViewById(R.id.txtestateCode);
            holder.name=(TextView) v.findViewById(R.id.txtestateName);
            v.setTag(holder);
        }
        else{
            holder=(StudentHolder) v.getTag();
            holder.imageView=(ImageView) v.findViewById(R.id.imgClick);
            holder.code=(TextView) v.findViewById(R.id.txtestateCode);
            holder.name=(TextView) v.findViewById(R.id.txtestateName);
        }
        holder.code.setText(student.getPricetype()+" | ");
        holder.name.setText(student.getPricetype());
        return v;
    }

    static class StudentHolder{
        ImageView imageView;
        TextView code;
        TextView name;
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                List<PriceDataModel> filteredList = new ArrayList<>();
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredList.addAll(lst_temp);
                } else {
                    if(lst_temp == null){
                        lst_temp = new ArrayList<PriceDataModel>(priceDataModelList);
                    }
                    for (PriceDataModel row : lst_temp) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getPricetype().toLowerCase().contains(charString.toLowerCase()) || row.getPricetype().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

//                    priceDataModelList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
//                priceDataModelList = (ArrayList<PriceDataModel>) filterResults.values;
//
//                // refresh the list with filtered data
////                notifyDataSetChanged();
//
//                notifyDataSetChanged();
//                clear();
//                int count = priceDataModelList.size();
//                for(int i = 0; i<count; i++){
//                    add(priceDataModelList.get(i));
//                    notifyDataSetInvalidated();
//                }
                priceDataModelList.clear();
                priceDataModelList.addAll((List) filterResults.values);
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getCount() {
        return priceDataModelList.size();
    }


    @Override
    public long getItemId(int pos) {
        return pos;
    }

}