package com.indoagri.comsatic.price;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.indoagri.comsatic.ChooseDate;
import com.indoagri.comsatic.Common.DateLocal;
import com.indoagri.comsatic.R;
import com.indoagri.comsatic.Retrofit.DB.DatabaseQuery;
import com.indoagri.comsatic.Retrofit.Model.PriceDataModel;
import com.indoagri.comsatic.Retrofit.SharePreference;
import com.indoagri.comsatic.SalesBilling.ListSubProduct;
import com.indoagri.comsatic.SalesBilling.SalesBilling;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReportActivity extends AppCompatActivity implements View.OnClickListener {

    DatabaseQuery query;
    SharePreference sharePreference;
    ProgressDialog pDialog;
    boolean iShowing;
    int selectedId = 0;
    EditText et_material;
    EditText et_pricetype;
    EditText et_from_date, et_to_date;
    public static int PRODUCTLIST = 11;
    public static int PRICETYPELIST = 14;
    public static int FROMDATE = 12;
    public static int TODATE = 13;
    PriceDataModel priceDataModel;
    String XCODEMATERIAL = null;
    String XPRICETYPE = null;
    String XFROMDATE = null;
    String XTODATE = null;
    Button btnSearch;
    TableLayout tableLayout, tableLayoutfooter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        query = new DatabaseQuery(ReportActivity.this);
        sharePreference = new SharePreference(ReportActivity.this);
        pDialog = new ProgressDialog(ReportActivity.this);
        iShowing = pDialog.isShowing();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        et_material = findViewById(R.id.et_product);
        et_pricetype = findViewById(R.id.et_pricetype);
        et_from_date = findViewById((R.id.et_date_from));
        et_to_date = findViewById((R.id.et_date_to));
        btnSearch = findViewById(R.id.btnSearch);
        btnSearch.setText("Show Report");
        btnSearch.setOnClickListener(this);
        tableLayout = (TableLayout) findViewById(R.id.main_table);
        tableLayout.bringToFront();
        tableLayout.setStretchAllColumns(true);
        TextView title = (TextView) findViewById(R.id.title_toolbar);
        title.setText("Report Price");
        init();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void init() {
        et_material.setText("Choose Material");
        et_material.setOnClickListener(this);
        et_pricetype.setText("Choose Price Type");
        et_pricetype.setOnClickListener(this);
        et_from_date.setOnClickListener(this);
        et_to_date.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == et_material) {
            Intent newIntent = new Intent(ReportActivity.this, ListProductActivity.class);
            startActivityForResult(newIntent, PRODUCTLIST);
        }
        if (v == et_pricetype && XCODEMATERIAL != null) {
            Intent newIntent = new Intent(ReportActivity.this, ListPriceTypeActivity.class);
            newIntent.putExtra("material", XCODEMATERIAL);
            startActivityForResult(newIntent, PRICETYPELIST);
        }
        if (v == et_from_date) {
            Intent intent = new Intent(ReportActivity.this, ChooseDate.class);
            startActivityForResult(intent, FROMDATE);
        }
        if (v == et_to_date) {
            Intent intent = new Intent(ReportActivity.this, ChooseDate.class);
            startActivityForResult(intent, TODATE);
        }
        if (v == btnSearch) {
            if (XCODEMATERIAL != null && XPRICETYPE != null && XFROMDATE != null && XTODATE != null) {
                DATASEARCHPRICE(XCODEMATERIAL, XPRICETYPE, XFROMDATE, XTODATE);
            } else {
                Toast.makeText(this, "Tidak dapat dilanjutkan", Toast.LENGTH_SHORT).show();
            }
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == PRODUCTLIST) {
            if (resultCode == RESULT_OK) {
                String CODE = data.getStringExtra("CODE");
                XCODEMATERIAL = data.getStringExtra("CODE");
                String DESC = data.getStringExtra("DESC");

                if (CODE == null) {
                    et_material.setText("Choose Material");
                } else {
                    String description = null;
                    if (DESC.length() >= 20) {
                        description = DESC.substring(0, 20) + "...";
                    } else {
                        description = DESC;
                    }
                    et_material.setText(description);
                }
                et_pricetype.setText("Choose Price Type");
            }
        }
        if (requestCode == PRICETYPELIST) {
            if (resultCode == RESULT_OK) {
                String CODE = data.getStringExtra("CODE");
                XPRICETYPE = data.getStringExtra("CODE");
                String DESC = data.getStringExtra("DESC");

                if (CODE == null) {
                    et_pricetype.setText("Choose Price Type");
                } else {
                    String description = null;
                    if (DESC.length() >= 20) {
                        description = DESC.substring(0, 20) + "...";
                    } else {
                        description = DESC;
                    }
                    et_pricetype.setText(description);
                }
            }
        }
        if (requestCode == FROMDATE) {
            if (resultCode == RESULT_OK) {
                String TANGGAL = data.getStringExtra("TANGGAL");
                if (TANGGAL == null) {
                    et_from_date.setText("From Date");
                } else {
                    XFROMDATE = data.getStringExtra("TANGGAL").substring(0, 10);
                    et_from_date.setText(XFROMDATE);

                }
            }
        }
        if (requestCode == TODATE) {
            if (resultCode == RESULT_OK) {
                String TANGGAL = data.getStringExtra("TANGGAL");
                if (TANGGAL == null) {
                    et_to_date.setText("To Date");
                } else {
                    XTODATE = data.getStringExtra("TANGGAL").substring(0, 10);
                    et_to_date.setText(XTODATE);

                }
            }
        }
    }

    private void DATASEARCHPRICE(String XCODE, String XPRICETYPE, String XFROMDATE, String XTODATE) {
        query.openTransaction();
        List<Object> listObject;
        List<PriceDataModel> listTemp = new ArrayList<PriceDataModel>();
        listTemp.clear();
//        String[] a = new String[4];
//        a[0] = XCODE;
//        a[1] = XPRICETYPE;
//        a[2] = XFROMDATE.substring(0,10);
//        a[3] = XTODATE.substring(0,10);
//        String queryString = "select ID,MATERIAL,DESCRIPTION,PRICE,DATE,groupProduct,subGroup ,PRICETYPE from Table_Price where id in" +
//                " (select max(id) from Table_Price WHERE material=? AND PRICETYPE=? AND DATE BETWEEN ? AND ? group by DATE ) ORDER BY DATE ";

        String whereValue = "";
        String queryString = "select ID,MATERIAL,DESCRIPTION,PRICE,DATE,groupProduct,subGroup ,PRICETYPE, CURRENCY from Table_Price where id in" +
                " (select max(id) from Table_Price WHERE ";
        if (!XCODE.equals("All")) {
            queryString += " material=? AND ";
            whereValue += XCODE + ":";
        }
        if (!XPRICETYPE.equals("All")) {
            queryString += " PRICETYPE=? AND ";
            whereValue += XPRICETYPE + ":";
        }
        queryString += " DATE BETWEEN ? AND ? group by DATE, material,pricetype ) ORDER BY DATE ";
        whereValue += XFROMDATE.substring(0, 10) + ":" + XTODATE.substring(0, 10);

//        listObject = query.getListDataRawQuery(queryString, PriceDataModel.TABLE_NAME, a);
        listObject = query.getListDataRawQuery(queryString, PriceDataModel.TABLE_NAME, whereValue.split(":"));
        query.closeTransaction();
        if (listObject == null) {
            // tableLayout.removeAllViews();
        } else {
            if (listObject.size() > 0) {
                for (int i = 0; i < listObject.size(); i++) {
                    PriceDataModel priceDataModel = (PriceDataModel) listObject.get(i);
                    listTemp.add(priceDataModel);
                }
                //startLoadData();
                AsyncDetail asyncBersih = new AsyncDetail(listObject, ReportActivity.this);
                asyncBersih.execute();
            } else {
                tableLayout.removeAllViews();

            }
        }
    }

    private class AsyncDetail extends AsyncTask<Void, Integer, Void> {
        List<Object> objectList;
        PriceDataModel priceDataModel;
        ProgressDialog progressDialog;

        public AsyncDetail(List<Object> objects, Context context) {
            objectList = objects;
            progressDialog = new ProgressDialog(context);
        }

        @Override
        protected void onPreExecute() {
            //mProgressbar.setVisibility(View.VISIBLE);
            iShowing = progressDialog.isShowing();
            if (!iShowing) {
                progressDialog.setMessage("Get Data");
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progressDialog.setProgress(values[0]);

        }

        @Override
        protected void onPostExecute(Void result) {

            loadData(objectList);
            progressDialog.dismiss();
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (int i = 0; i < objectList.size(); i++) {
                publishProgress(i);
                priceDataModel = (PriceDataModel) objectList.get(i);
            }

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }
    }

    public void loadData(List<Object> objectList) {

        int leftRowMargin = 0;
        int topRowMargin = 0;
        int rightRowMargin = 0;
        int bottomRowMargin = 0;
        int textSize = 0, smallTextSize = 0, mediumTextSize = 0;

        textSize = (int) getResources().getDimension(R.dimen.font_size_medium);
        smallTextSize = (int) getResources().getDimension(R.dimen.font_size_normal);
        mediumTextSize = (int) getResources().getDimension(R.dimen.font_size_medium);

        int totalrows = objectList.size();
        int rows = objectList.size();
        int rowsIden = 0;
        getSupportActionBar().setTitle("Price (" + String.valueOf(rows) + ")");
        TextView textSpacer = null;

        tableLayout.removeAllViews();

        NumberFormat PriceFormat = new DecimalFormat("#,###,###,###,###,###");
        double priceFormat = 0;
        String DatapriceFormattedNumber = null;
        String Datamaterial = null;
        String DataTypeGroup = null;
        String DataDate = null;
        String DataCurrency = null;
        // -1 means heading row
        for (rowsIden = -1; rowsIden < objectList.size(); rowsIden++) {
            if (rowsIden > -1) {
                priceDataModel = (PriceDataModel) objectList.get(rowsIden);
//                priceFormat = Double.parseDouble(priceDataModel.getPrice()) / 1000; data ditampilkan tanpa di bagi
                priceFormat = Double.parseDouble(priceDataModel.getPrice());
                DatapriceFormattedNumber = PriceFormat.format(priceFormat);
                if (priceDataModel.getPricetype().length() >= 13) {
                    DataTypeGroup = priceDataModel.getPricetype().substring(0, 13) + "...";
                } else {
                    DataTypeGroup = priceDataModel.getPricetype();
                }
                if (priceDataModel.getDescription().length() >= 13) {
                    Datamaterial = priceDataModel.getDescription().substring(0, 13) + "...";
                } else {
                    Datamaterial = priceDataModel.getDescription();
                }
                DataCurrency = priceDataModel.getCurrency();

                SimpleDateFormat orisinilDate = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat formatUpdateDated = new SimpleDateFormat("dd-MMM-yyyy");
                try {
                    Date date = orisinilDate.parse(priceDataModel.getDate());

                    DataDate = formatUpdateDated.format(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


            } else {
                textSpacer = new TextView(this);
                textSpacer.setText("");
            }

            final TextView tvPeriode = new TextView(this);
            if (rowsIden == -1) {
                tvPeriode.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.WRAP_CONTENT));
                tvPeriode.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            } else {
                tvPeriode.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                tvPeriode.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
            }

            tvPeriode.setGravity(Gravity.LEFT);

            tvPeriode.setPadding(15, 15, 0, 15);
            if (rowsIden == -1) {
                tvPeriode.setText("Material");
                tvPeriode.setTextColor(getResources().getColor(R.color.colorWhiteFont));
                tvPeriode.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
            } else {
                tvPeriode.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                tvPeriode.setTextColor(getResources().getColor(R.color.colorBlackGray));
                tvPeriode.setText(Datamaterial);
            }

            final LinearLayout layQty = new LinearLayout(this);
            layQty.setOrientation(LinearLayout.VERTICAL);
            layQty.setGravity(Gravity.RIGHT);
            layQty.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            final TextView tvQty = new TextView(this);
            tvQty.setPadding(5, 15, 15, 15);
            if (rowsIden == -1) {
                tvQty.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                tvQty.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            } else {
                tvQty.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                tvQty.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
            }

            if (rowsIden == -1) {
                tvQty.setGravity(Gravity.CENTER);
                tvQty.setText("Date");
                tvQty.setTextColor(getResources().getColor(R.color.colorWhiteFont));
                tvQty.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
            } else {
                tvQty.setGravity(Gravity.RIGHT);
                tvQty.setBackgroundColor(getResources().getColor(R.color.colorWhiteinit));
                tvQty.setTextColor(getResources().getColor(R.color.colorBlackGray));
                tvQty.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
                tvQty.setText(DataDate);
            }
            layQty.addView(tvQty);


            //type
            final LinearLayout layValue = new LinearLayout(this);
            layValue.setOrientation(LinearLayout.VERTICAL);
            layValue.setGravity(Gravity.RIGHT);
            layValue.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));

            final TextView tvValue = new TextView(this);
            tvValue.setPadding(5, 15, 15, 15);
            if (rowsIden == -1) {
                tvValue.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                layValue.setBackgroundColor(Color.parseColor("#f7f7f7"));
            } else {
                tvValue.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.WRAP_CONTENT));
                layValue.setBackgroundColor(Color.parseColor("#ffffff"));
            }

            if (rowsIden == -1) {
                tvValue.setGravity(Gravity.CENTER);
                tvValue.setText("Type");
                tvValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
                tvValue.setTextColor(getResources().getColor(R.color.colorWhiteFont));
                tvValue.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
            } else {
                tvValue.setGravity(Gravity.LEFT);
                tvValue.setBackgroundColor(getResources().getColor(R.color.colorWhiteinit));
                tvValue.setTextColor(getResources().getColor(R.color.colorBlackGray));
                tvValue.setText(DataTypeGroup);
                tvValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
            }

            layValue.addView(tvValue);

            //currency
            final LinearLayout layCurrency = new LinearLayout(this);
            layCurrency.setOrientation(LinearLayout.VERTICAL);
            layCurrency.setGravity(Gravity.RIGHT);
            layCurrency.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));

            final TextView tvCurrency = new TextView(this);
            tvCurrency.setPadding(5, 15, 15, 15);
            if (rowsIden == -1) {
                tvCurrency.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                layCurrency.setBackgroundColor(Color.parseColor("#f7f7f7"));
            } else {
                tvCurrency.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.WRAP_CONTENT));
                layCurrency.setBackgroundColor(Color.parseColor("#ffffff"));
            }

            if (rowsIden == -1) {
                tvCurrency.setGravity(Gravity.CENTER);
                tvCurrency.setText("Curr.");
                tvCurrency.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
                tvCurrency.setTextColor(getResources().getColor(R.color.colorWhiteFont));
                tvCurrency.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
            } else {
                tvCurrency.setGravity(Gravity.CENTER);
                tvCurrency.setBackgroundColor(getResources().getColor(R.color.colorWhiteinit));
                tvCurrency.setTextColor(getResources().getColor(R.color.colorBlackGray));
                tvCurrency.setText(DataCurrency);
                tvCurrency.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
            }

            layCurrency.addView(tvCurrency);

            //price
            final LinearLayout layPrices = new LinearLayout(this);
            layPrices.setOrientation(LinearLayout.VERTICAL);
            layPrices.setGravity(Gravity.RIGHT);
            layPrices.setBackgroundColor(Color.parseColor("#f8f8f8"));

            final TextView tvPrice = new TextView(this);

            tvPrice.setPadding(5, 15, 15, 15);
            if (rowsIden == -1) {
                tvPrice.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                tvPrice.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);

            } else {
                tvPrice.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                tvPrice.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
            }


            if (rowsIden == -1) {
                tvPrice.setGravity(Gravity.CENTER);
                tvPrice.setText("Price");
                tvPrice.setTextColor(getResources().getColor(R.color.colorWhiteFont));
                tvPrice.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
            } else {
                tvPrice.setGravity(Gravity.RIGHT);
                tvPrice.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                tvPrice.setTextColor(getResources().getColor(R.color.colorBlackGray));
                tvPrice.setText(DatapriceFormattedNumber);
                tvPrice.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
            }
            layPrices.addView(tvPrice);

            // add table row
            final TableRow tr = new TableRow(this);
            tr.setId(rowsIden + 1);
            TableLayout.LayoutParams trParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT);
            trParams.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
            tr.setPadding(0, 0, 0, 0);
            tr.setLayoutParams(trParams);

            //tr.addView(tv);
            tr.addView(tvPeriode);
            tr.addView(layQty);
            tr.addView(layValue);
            tr.addView(layCurrency);
            tr.addView(layPrices);
            if (rowsIden > -1) {
                tr.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        TableRow tr = (TableRow) v;
                        //do whatever action is needed
                        //   Toast.makeText(SalesBilling.this,"Test Toast",Toast.LENGTH_SHORT).show();
                    }
                });
            }
            tableLayout.addView(tr, trParams);
            if (rowsIden > -1) {
                // add separator row
                final TableRow trSep = new TableRow(this);
                TableLayout.LayoutParams trParamsSep = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                        TableLayout.LayoutParams.WRAP_CONTENT);
                trParamsSep.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);

                trSep.setLayoutParams(trParamsSep);
                TextView tvSep = new TextView(this);
                TableRow.LayoutParams tvSepLay = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.WRAP_CONTENT);
                tvSepLay.span = 5;
                tvSep.setLayoutParams(tvSepLay);
                tvSep.setBackgroundColor(Color.parseColor("#d9d9d9"));
                tvSep.setHeight(1);
                trSep.addView(tvSep);
                tableLayout.addView(trSep, trParamsSep);
            }
        }

    }

}
