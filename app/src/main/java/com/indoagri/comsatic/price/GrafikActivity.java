package com.indoagri.comsatic.price;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;

import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.indoagri.comsatic.ChooseDate;
import com.indoagri.comsatic.R;
import com.indoagri.comsatic.Retrofit.DB.DatabaseQuery;
import com.indoagri.comsatic.Retrofit.Model.PriceDataModel;
import com.indoagri.comsatic.Retrofit.SharePreference;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class GrafikActivity extends AppCompatActivity implements View.OnClickListener {

    DatabaseQuery query;
    SharePreference sharePreference;
    ProgressDialog pDialog;
    boolean iShowing;
    int selectedId = 0;
    EditText et_material;
    EditText et_pricetype;
    EditText et_from_date, et_to_date;
    public static int PRODUCTLIST = 11;
    public static int PRICETYPELIST = 14;
    public static int FROMDATE = 12;
    public static int TODATE = 13;
    PriceDataModel priceDataModel;
    String XCODEMATERIAL = null;
    String XPRICETYPE = null;
    String XFROMDATE = null;
    String XTODATE = null;
    Button btnSearch;
    List<PriceDataModel> priceDataModelList;
    ArrayList<Entry> entries;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    private RadioGroup RadioGroupSearch;
    RadioButton rbDays, rbWeeks, rbMonths;
    ArrayList<String> arrlist = new ArrayList<String>();
    ArrayList<String> arrlistData = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grafik);
        query = new DatabaseQuery(GrafikActivity.this);
        sharePreference = new SharePreference(GrafikActivity.this);
        pDialog = new ProgressDialog(GrafikActivity.this);
        iShowing = pDialog.isShowing();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        et_material = findViewById(R.id.et_product);
        et_pricetype = findViewById(R.id.et_pricetype);
        et_from_date = findViewById((R.id.et_date_from));
        et_to_date = findViewById((R.id.et_date_to));

        btnSearch = findViewById(R.id.btnSearch);
        btnSearch.setText("Show Grafik");
        btnSearch.setOnClickListener(this);
        TextView title = (TextView) findViewById(R.id.title_toolbar);
        title.setText("Grafik Price");
        RadioGroupSearch = (findViewById(R.id.rg));
        rbDays = (findViewById(R.id.radioDays));
        rbMonths = (findViewById(R.id.radioMonths));
        rbWeeks = (findViewById(R.id.radioWeeks));
        RadioGroupSearch.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioDays:
                        selectedId = 2;
                        if (XCODEMATERIAL != null && XPRICETYPE != null && XFROMDATE != null && XTODATE != null) {
                            DataByDays(XCODEMATERIAL, XPRICETYPE, XFROMDATE, XTODATE);
                        } else {
                            Toast.makeText(GrafikActivity.this, "Tidak dapat dilanjutkan", Toast.LENGTH_SHORT).show();
                        }
                        //   Toast.makeText(GrafikActivity.this, "Pilih Days", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioWeeks:
                        selectedId = 1;
                        if (XCODEMATERIAL != null && XPRICETYPE != null && XFROMDATE != null && XTODATE != null) {
                            DataByWeeks(XCODEMATERIAL, XPRICETYPE, XFROMDATE, XTODATE);
                        } else {
                            Toast.makeText(GrafikActivity.this, "Tidak dapat dilanjutkan", Toast.LENGTH_SHORT).show();
                        }
                        //   Toast.makeText(GrafikActivity.this, "Pilih Mingguan", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioMonths:
                        selectedId = 0;
                        if (XCODEMATERIAL != null && XFROMDATE != null && XTODATE != null) {
                            DATASEARCHPRICE(XCODEMATERIAL, XPRICETYPE, XFROMDATE, XTODATE);
                        } else {
                            Toast.makeText(GrafikActivity.this, "Tidak dapat dilanjutkan", Toast.LENGTH_SHORT).show();
                        }
                        //   Toast.makeText(GrafikActivity.this, "Pilih Bulan", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
        init();
    }

    private void init() {
        et_material.setText("Choose Material");
        et_material.setOnClickListener(this);
        et_pricetype.setText("Choose Price Type");
        et_pricetype.setOnClickListener(this);
        et_from_date.setOnClickListener(this);
        et_to_date.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        RadioGroupSearch.setVisibility(View.GONE);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View v) {
        if (v == et_material) {
            Intent newIntent = new Intent(GrafikActivity.this, ListProductActivity.class);
            startActivityForResult(newIntent, PRODUCTLIST);
        }
        if (v == et_pricetype && XCODEMATERIAL != null) {
            Intent newIntent = new Intent(GrafikActivity.this, ListPriceTypeActivity.class);
            newIntent.putExtra("material", XCODEMATERIAL);
            startActivityForResult(newIntent, PRICETYPELIST);
        }
        if (v == et_from_date) {
            Intent intent = new Intent(GrafikActivity.this, ChooseDate.class);
            startActivityForResult(intent, FROMDATE);
        }
        if (v == et_to_date) {
            Intent intent = new Intent(GrafikActivity.this, ChooseDate.class);
            startActivityForResult(intent, TODATE);
        }
        if (v == btnSearch) {
            if (XCODEMATERIAL != null && XPRICETYPE != null && XFROMDATE != null && XTODATE != null) {
                if (rbMonths.isChecked()) {
                    DATASEARCHPRICE(XCODEMATERIAL, XPRICETYPE, XFROMDATE, XTODATE);
                } else {
                    rbMonths.setChecked(true);
                }
            } else {
                Toast.makeText(this, "Tidak dapat dilanjutkan", Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void DataByDays(String XCODE, String XPRICETYPE, String XFROMDATE, String XTODATE) {
        ArrayList<String> listDate = getWeekday(XFROMDATE, XTODATE);
        priceDataModelList = new ArrayList<PriceDataModel>();
        priceDataModelList.clear();
        query.openTransaction();
        List<Object> listObject;
        List<PriceDataModel> listTemp = new ArrayList<PriceDataModel>();
        listTemp.clear();
        String[] a = new String[4];
        a[0] = XCODE;
        a[1] = XPRICETYPE;
        a[2] = XFROMDATE.substring(0, 10);
        a[3] = XTODATE.substring(0, 10);
        String queryString = "SELECT * FROM Table_Price \n" +
                "WHERE ID in (select max(id) from Table_Price WHERE material= ? AND PRICETYPE= ? AND DATE BETWEEN ? AND ? group by DATE)\n" +
                "order by DATE ASC LIMIT 10";
        listObject = query.getListDataRawQuery(queryString, PriceDataModel.TABLE_NAME, a);
        query.closeTransaction();
        if (listObject == null) {
            // tableLayout.removeAllViews();
            RadioGroupSearch.setVisibility(View.GONE);
        } else {
            RadioGroupSearch.setVisibility(View.VISIBLE);
            if (listObject.size() > 0) {
                for (int i = 0; i < listObject.size(); i++) {
                    PriceDataModel priceDataModel = (PriceDataModel) listObject.get(i);
                    listTemp.add(priceDataModel);
                    priceDataModelList.add(priceDataModel);
                    Log.e("DATA", priceDataModel.getDate());
//                    arrlist.add(priceDataModel.getDate());
                    arrlistData.add(priceDataModel.getPrice());
                }

                LineChartDays(listObject, listTemp, listObject.size(), listDate, arrlistData);
            } else {
            }
        }
    }

    private void DataByWeeks(String XCODE, String XPRICETYPE, String XFROMDATE, String XTODATE) {
        ArrayList<String> listDate = getWeeknumber(XFROMDATE, XTODATE);
        priceDataModelList = new ArrayList<PriceDataModel>();
        priceDataModelList.clear();
        query.openTransaction();
        List<Object> listObject;
        List<PriceDataModel> listTemp = new ArrayList<PriceDataModel>();
        listTemp.clear();
        String[] a = new String[4];
        a[0] = XCODE;
        a[1] = XPRICETYPE;
        a[2] = XFROMDATE.substring(0, 10);
        a[3] = XTODATE.substring(0, 10);
//        String queryString = "SELECT * FROM Table_Price \n" +
//                "WHERE ID in (select max(id) from Table_Price WHERE material= ? AND PRICETYPE= ?  AND strftime('%W',DATE) BETWEEN strftime('%W',?) AND strftime('%W',?) group by strftime('%W',DATE))\n" +
//                "order by DATE ASC LIMIT 5";
        String queryString = "SELECT MATERIAL,DESCRIPTION,avg(PRICE) as PRICE,groupProduct,subGroup,PRICETYPE,strftime('%W',DATE), min(date) as DATE  FROM Table_Price \n" +
                "WHERE material= ? AND PRICETYPE= ?  AND strftime('%W',DATE) BETWEEN strftime('%W',?) AND strftime('%W',?) group by strftime('%W',DATE)\n" +
                "order by strftime('%W',DATE) ASC";
        listObject = query.getListDataRawQuery(queryString, PriceDataModel.TABLE_NAME, a);
        query.closeTransaction();
        if (listObject == null) {
            // tableLayout.removeAllViews();
            RadioGroupSearch.setVisibility(View.GONE);
        } else {
            RadioGroupSearch.setVisibility(View.VISIBLE);
            if (listObject.size() > 0) {
                for (int i = 0; i < listObject.size(); i++) {
                    PriceDataModel priceDataModel = (PriceDataModel) listObject.get(i);
                    listTemp.add(priceDataModel);
                    priceDataModelList.add(priceDataModel);
//                    arrlist.add(priceDataModel.getDate());
                    arrlistData.add(priceDataModel.getPrice());
                }

                LineChartWeeks(listObject, listTemp, listObject.size(), listDate, arrlistData);
            } else {
            }
        }
    }

    private void DATASEARCHPRICE(String XCODE, String XPRICETYPE, String XFROMDATE, String XTODATE) {
        priceDataModelList = new ArrayList<PriceDataModel>();
        priceDataModelList.clear();
        query.openTransaction();
        List<Object> listObject;
        List<PriceDataModel> listTemp = new ArrayList<PriceDataModel>();
        listTemp.clear();
        String[] a = new String[4];
        a[0] = XCODE;
        a[1] = XPRICETYPE;
        a[2] = XFROMDATE.substring(0, 10);
        a[3] = XTODATE.substring(0, 10);
//        String queryString = "select ID,MATERIAL,DESCRIPTION,PRICE,substr(DATE,0,8) AS DATE,groupProduct,subGroup,PRICETYPE from Table_Price where id in " +
//                "(select max(id) from Table_Price WHERE material=? AND PRICETYPE=? AND DATE BETWEEN ? AND ? group by substr(DATE,0,8)) ORDER BY DATE ASC  ";
        String queryString = "select MATERIAL,DESCRIPTION,avg(PRICE) as PRICE,substr(DATE,0,8) AS DATE,groupProduct,subGroup,PRICETYPE from Table_Price " +
                "WHERE material= ? AND PRICETYPE= ? AND DATE BETWEEN ? AND ? group by substr(DATE,0,8),MATERIAL,DESCRIPTION,groupProduct,subGroup,PRICETYPE  ORDER BY DATE ASC";

        listObject = query.getListDataRawQuery(queryString, PriceDataModel.TABLE_NAME, a);
        query.closeTransaction();
        if (listObject == null) {
            // tableLayout.removeAllViews();
            RadioGroupSearch.setVisibility(View.GONE);
        } else {
            RadioGroupSearch.setVisibility(View.VISIBLE);
            if (listObject.size() > 0) {
                for (int i = 0; i < listObject.size(); i++) {
                    PriceDataModel priceDataModel = (PriceDataModel) listObject.get(i);
                    listTemp.add(priceDataModel);
                    priceDataModelList.add(priceDataModel);
//                    Log.e("DATA", priceDataModel.getDate());
                }

                LineChartBulan(listObject, listTemp);
            } else {
            }
        }
    }

    public ArrayList<String> getWeekday(String XFROMDATE, String XTODATE) {
        ArrayList<String> listDate = new ArrayList<>();
        try {
            Date startDate = sdf.parse(XFROMDATE);
            Date endDate = sdf.parse(XTODATE);

            Calendar startCal = Calendar.getInstance();
            startCal.setTime(startDate);

            Calendar endCal = Calendar.getInstance();
            endCal.setTime(endDate);

            do {
                //excluding start date
                if (startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
                    listDate.add(sdf.format(startCal.getTime()));
                }
                startCal.add(Calendar.DAY_OF_MONTH, 1);
            } while (startCal.getTimeInMillis() <= endCal.getTimeInMillis() && listDate.size() <= 5); //excluding end date

        } catch (Exception e) {

        }
        return listDate;
    }

    public ArrayList<String> getWeeknumber(String XFROMDATE, String XTODATE) {
        ArrayList<String> listDate = new ArrayList<>();
        try {
            Date startDate = sdf.parse(XFROMDATE);
            Date endDate = sdf.parse(XTODATE);

            Calendar startCal = Calendar.getInstance();
            startCal.setTime(startDate);

            Calendar endCal = Calendar.getInstance();
            endCal.setTime(endDate);

            do {
                listDate.add(String.valueOf(startCal.get(Calendar.WEEK_OF_YEAR)));
                startCal.add(Calendar.WEEK_OF_MONTH, 1);
            } while (listDate.size() < 5);
        } catch (Exception e) {

        }
        return listDate;
    }

    public String getWeekNumber(String Date) {
        try {
            Date startDate = sdf.parse(Date);
            Calendar startCal = Calendar.getInstance();
            startCal.setTime(startDate);
            return String.valueOf(startCal.get(Calendar.WEEK_OF_YEAR));
        } catch (Exception e) {
            return null;
        }

    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == PRODUCTLIST) {
            if (resultCode == RESULT_OK) {
                String CODE = data.getStringExtra("CODE");
                XCODEMATERIAL = data.getStringExtra("CODE");
                String DESC = data.getStringExtra("DESC");

                if (CODE == null) {
                    et_material.setText("Choose Material");
                } else {
                    String description = null;
                    if (DESC.length() >= 20) {
                        description = DESC.substring(0, 20) + "...";
                    } else {
                        description = DESC;
                    }
                    et_material.setText(description);
                }
                et_pricetype.setText("Choose Price Type");
            }
        }
        if (requestCode == PRICETYPELIST) {
            if (resultCode == RESULT_OK) {
                String CODE = data.getStringExtra("CODE");
                XPRICETYPE = data.getStringExtra("CODE");
                String DESC = data.getStringExtra("DESC");

                if (CODE == null) {
                    et_pricetype.setText("Choose PRICE TYPE");
                } else {
                    String description = null;
                    if (DESC.length() >= 20) {
                        description = DESC.substring(0, 20) + "...";
                    } else {
                        description = DESC;
                    }
                    et_pricetype.setText(description);
                }
            }
        }
        if (requestCode == FROMDATE) {
            if (resultCode == RESULT_OK) {
                String TANGGAL = data.getStringExtra("TANGGAL");
                if (TANGGAL == null) {
                    et_from_date.setText("From Date");
                } else {
                    XFROMDATE = data.getStringExtra("TANGGAL").substring(0, 10);
                    et_from_date.setText(XFROMDATE);

                }
            }
        }
        if (requestCode == TODATE) {
            if (resultCode == RESULT_OK) {
                String TANGGAL = data.getStringExtra("TANGGAL");
                if (TANGGAL == null) {
                    et_to_date.setText("To Date");
                } else {
                    XTODATE = data.getStringExtra("TANGGAL").substring(0, 10);
                    et_to_date.setText(XTODATE);

                }
            }
        }
    }

    void LineChartBulan(List<Object> listObject, List<PriceDataModel> priceDataModels) {
        entries = new ArrayList<>();
        final LineChart lineChartView2;
        LineDataSet lineDataSet;
        lineChartView2 = (LineChart) findViewById(R.id.chart2);
        lineDataSet = new LineDataSet(getDataBulan(listObject, priceDataModels), "Price");
        XAxis xAxis = lineChartView2.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        final String[] months = new String[]{"Jan", "Feb", "Mar", "Apr", "Mei", "Juni", "July", "August", "Sept", "Oct", "Nov", "Dec"};
        ValueFormatter formatter = new ValueFormatter() {
            @Override
            public String getAxisLabel(float value, AxisBase axis) {
                return months[(int) value];
            }
        };
        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(formatter);
        lineDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        lineDataSet.setHighlightEnabled(true);
        lineDataSet.setLineWidth(2);
        lineDataSet.setColor(Color.RED);
        lineDataSet.setCircleColor(Color.YELLOW);
        lineDataSet.setCircleRadius(6);
        lineDataSet.setCircleHoleRadius(3);
        lineDataSet.setDrawHighlightIndicators(true);
        lineDataSet.setHighLightColor(Color.RED);
        lineDataSet.setValueTextSize(12);
        lineDataSet.setValueTextColor(Color.DKGRAY);


        LineData data = new LineData(lineDataSet);
        lineChartView2.getDescription().setText("Data Price");
        lineChartView2.getDescription().setTextSize(12);
        lineChartView2.setDrawMarkers(true);
        lineChartView2.getXAxis().setPosition(XAxis.XAxisPosition.BOTH_SIDED);
        lineChartView2.animateY(1000);
        lineChartView2.getXAxis().setGranularityEnabled(true);
        lineChartView2.getXAxis().setGranularity(1.0f);
        lineChartView2.getXAxis().setLabelCount(lineDataSet.getEntryCount());
        lineChartView2.setData(data);
//        lineChartView2.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
//            @Override
//            public void onValueSelected(Entry e, Highlight h) {
//                lineChartView2.centerViewToAnimated(e.getX(), e.getY(), lineChartView2.getData().getDataSetByIndex(h.getDataSetIndex())
//                        .getAxisDependency(), 500);
//            }
//
//            @Override
//            public void onNothingSelected() {
//222
//            }
//        });

    }

    void LineChartDays(List<Object> listObject, List<PriceDataModel> priceDataModels, int TotalData, ArrayList<String> arrlist, ArrayList<String> arrlistData) {
        entries.clear();
        entries = new ArrayList<>();
        LineChart lineChartView2;
        LineDataSet lineDataSet;
        lineChartView2 = (LineChart) findViewById(R.id.chart2);
        lineDataSet = new LineDataSet(getDataDays(listObject, priceDataModels, TotalData, arrlist, arrlistData), "Price");
        XAxis xAxis = lineChartView2.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        //  String[] str = GetStringArray(arrlist);
//        final ArrayList<String> day = new ArrayList<String>();
        final String[] day = new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};
//        final String[] day = new String[TotalData];
        for (int i = 0; i < arrlist.size(); i++) {
            day[i] = arrlist.get(i).substring(5, 10);
        }
        // final String[] months = str;
        ValueFormatter formatter = new ValueFormatter() {
            @Override
            public String getAxisLabel(float value, AxisBase axis) {
                Log.e(ContentValues.TAG, "getAxisLabel: " + value);
                if (value == -1) {
                    value = 0;
                }
                return day[(int) value];
            }
        };
        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(formatter);
        lineDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        lineDataSet.setHighlightEnabled(true);
        lineDataSet.setLineWidth(2);
        lineDataSet.setColor(Color.RED);
        lineDataSet.setCircleColor(Color.YELLOW);
        lineDataSet.setCircleRadius(6);
        lineDataSet.setCircleHoleRadius(3);
        lineDataSet.setDrawHighlightIndicators(true);
        lineDataSet.setHighLightColor(Color.RED);
        lineDataSet.setValueTextSize(12);
        lineDataSet.setValueTextColor(Color.DKGRAY);


        LineData data = new LineData(lineDataSet);
        lineChartView2.getDescription().setText("Data Price");
        lineChartView2.getDescription().setTextSize(12);
        lineChartView2.setDrawMarkers(true);
        lineChartView2.getXAxis().setPosition(XAxis.XAxisPosition.BOTH_SIDED);
        lineChartView2.animateY(1000);
        lineChartView2.getXAxis().setGranularityEnabled(true);
        lineChartView2.getXAxis().setGranularity(1.0f);
        lineChartView2.getXAxis().setLabelCount(lineDataSet.getEntryCount());
        lineChartView2.setData(data);

    }


    void LineChartWeeks(List<Object> listObject, List<PriceDataModel> priceDataModels, int TotalData, ArrayList<String> arrlist, ArrayList<String> arrlistData) {
        entries.clear();
        entries = new ArrayList<>();
        LineChart lineChartView2;
        LineDataSet lineDataSet;
        lineChartView2 = (LineChart) findViewById(R.id.chart2);
        lineDataSet = new LineDataSet(getDataWeeks(listObject, priceDataModels, TotalData, arrlist, arrlistData), "Price");
        XAxis xAxis = lineChartView2.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        //  String[] str = GetStringArray(arrlist);
//        final ArrayList<String> day = new ArrayList<String>();
        final String[] day = new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};
//        final String[] day = new String[TotalData];
//        for (int i = 0; i < arrlist.size(); i++) {
//            day[i] = arrlist.get(i);
//        }
        // final String[] months = str;
        ValueFormatter formatter = new ValueFormatter() {
            @Override
            public String getAxisLabel(float value, AxisBase axis) {
                Log.e(ContentValues.TAG, "getAxisLabel: " + value);
                return day[(int) value];
            }
        };
        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(formatter);
        lineDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        lineDataSet.setHighlightEnabled(true);
        lineDataSet.setLineWidth(2);
        lineDataSet.setColor(Color.RED);
        lineDataSet.setCircleColor(Color.YELLOW);
        lineDataSet.setCircleRadius(6);
        lineDataSet.setCircleHoleRadius(3);
        lineDataSet.setDrawHighlightIndicators(true);
        lineDataSet.setHighLightColor(Color.RED);
        lineDataSet.setValueTextSize(12);
        lineDataSet.setValueTextColor(Color.DKGRAY);


        LineData data = new LineData(lineDataSet);
        lineChartView2.getDescription().setText("Data Price");
        lineChartView2.getDescription().setTextSize(12);
        lineChartView2.setDrawMarkers(true);
        lineChartView2.getXAxis().setPosition(XAxis.XAxisPosition.BOTH_SIDED);
        lineChartView2.animateY(1000);
        lineChartView2.getXAxis().setGranularityEnabled(true);
        lineChartView2.getXAxis().setGranularity(1.0f);
        lineChartView2.getXAxis().setLabelCount(lineDataSet.getEntryCount());
        lineChartView2.setData(data);

    }


    private ArrayList getDataBulan(List<Object> listObject, List<PriceDataModel> priceDataModels) {
        int countData = 12;
        int rows = listObject.size();
        String periode = null;
        NumberFormat PriceFormat = new DecimalFormat("####");
        double priceFormat = 0;
        String DatapriceFormattedNumber = null;

        String[] DataPrice = new String[countData];
        for (int i = 0; i < countData; i++) {
            if (i == 0) {
                for (rows = 0; rows < listObject.size(); rows++) {
                    priceDataModel = (PriceDataModel) listObject.get(rows);
                    String bulan = priceDataModel.getDate();
                    if (bulan.equalsIgnoreCase("2020-01")) {
//                        priceFormat = Double.parseDouble(priceDataModel.getPrice()) / 1000;
                        priceFormat = Double.parseDouble(priceDataModel.getPrice());
                        DatapriceFormattedNumber = PriceFormat.format(priceFormat);
                        DataPrice[0] = DatapriceFormattedNumber;
                    } else {
                        if (DataPrice[0] == null) {
                            DataPrice[0] = "0";
                        }
                    }
                }
            }
            if (i == 1) {
                for (rows = 0; rows < listObject.size(); rows++) {
                    priceDataModel = (PriceDataModel) listObject.get(rows);
                    String bulan = priceDataModel.getDate();
                    if (bulan.equalsIgnoreCase("2020-02")) {
//                        priceFormat = Double.parseDouble(priceDataModel.getPrice()) / 1000;
                        priceFormat = Double.parseDouble(priceDataModel.getPrice());
                        DatapriceFormattedNumber = PriceFormat.format(priceFormat);
                        DataPrice[1] = DatapriceFormattedNumber;
                    } else {
                        if (DataPrice[1] == null) {
                            DataPrice[1] = "0";
                        }
                    }
                }
            }
            if (i == 2) {
                for (rows = 0; rows < listObject.size(); rows++) {
                    priceDataModel = (PriceDataModel) listObject.get(rows);
                    String bulan = priceDataModel.getDate();
                    if (bulan.equalsIgnoreCase("2020-03")) {
//                        priceFormat = Double.parseDouble(priceDataModel.getPrice()) / 1000;
                        priceFormat = Double.parseDouble(priceDataModel.getPrice());
                        DatapriceFormattedNumber = PriceFormat.format(priceFormat);
                        DataPrice[2] = DatapriceFormattedNumber;
                    } else {
                        if (DataPrice[2] == null) {
                            DataPrice[2] = "0";
                        }
                    }
                }
            }
            if (i == 3) {
                for (rows = 0; rows < listObject.size(); rows++) {
                    priceDataModel = (PriceDataModel) listObject.get(rows);
                    String bulan = priceDataModel.getDate();
                    if (bulan.equalsIgnoreCase("2020-04")) {
//                        priceFormat = Double.parseDouble(priceDataModel.getPrice()) / 1000;
                        priceFormat = Double.parseDouble(priceDataModel.getPrice());
                        DatapriceFormattedNumber = PriceFormat.format(priceFormat);
                        DataPrice[3] = DatapriceFormattedNumber;
                    } else {
                        if (DataPrice[3] == null) {
                            DataPrice[3] = "0";
                        }
                    }
                }
            }
            if (i == 4) {
                for (rows = 0; rows < listObject.size(); rows++) {
                    priceDataModel = (PriceDataModel) listObject.get(rows);
                    String bulan = priceDataModel.getDate();
                    if (bulan.equalsIgnoreCase("2020-05")) {
//                        priceFormat = Double.parseDouble(priceDataModel.getPrice()) / 1000;
                        priceFormat = Double.parseDouble(priceDataModel.getPrice());
                        DatapriceFormattedNumber = PriceFormat.format(priceFormat);
                        DataPrice[4] = DatapriceFormattedNumber;
                    } else {
                        if (DataPrice[4] == null) {
                            DataPrice[4] = "0";
                        }
                    }
                }
            }
            if (i == 5) {
                for (rows = 0; rows < listObject.size(); rows++) {
                    priceDataModel = (PriceDataModel) listObject.get(rows);
                    String bulan = priceDataModel.getDate();
                    if (bulan.equalsIgnoreCase("2020-06")) {
//                        priceFormat = Double.parseDouble(priceDataModel.getPrice()) / 1000;
                        priceFormat = Double.parseDouble(priceDataModel.getPrice());
                        DatapriceFormattedNumber = PriceFormat.format(priceFormat);
                        DataPrice[5] = DatapriceFormattedNumber;
                    } else {
                        if (DataPrice[5] == null) {
                            DataPrice[5] = "0";
                        }
                    }
                }
            }
            if (i == 6) {
                for (rows = 0; rows < listObject.size(); rows++) {
                    priceDataModel = (PriceDataModel) listObject.get(rows);
                    String bulan = priceDataModel.getDate();
                    if (bulan.equalsIgnoreCase("2020-07")) {
//                        priceFormat = Double.parseDouble(priceDataModel.getPrice()) / 1000;
                        priceFormat = Double.parseDouble(priceDataModel.getPrice());
                        DatapriceFormattedNumber = PriceFormat.format(priceFormat);
                        DataPrice[6] = DatapriceFormattedNumber;
                    } else {
                        if (DataPrice[6] == null) {
                            DataPrice[6] = "0";
                        }
                    }
                }
            }
            if (i == 7) {
                for (rows = 0; rows < listObject.size(); rows++) {
                    priceDataModel = (PriceDataModel) listObject.get(rows);
                    String bulan = priceDataModel.getDate();
                    if (bulan.equalsIgnoreCase("2020-08")) {
//                        priceFormat = Double.parseDouble(priceDataModel.getPrice()) / 1000;
                        priceFormat = Double.parseDouble(priceDataModel.getPrice());
                        DatapriceFormattedNumber = PriceFormat.format(priceFormat);
                        DataPrice[7] = DatapriceFormattedNumber;
                    } else {
                        if (DataPrice[7] == null) {
                            DataPrice[7] = "0";
                        }
                    }
                }
            }
            if (i == 8) {
                for (rows = 0; rows < listObject.size(); rows++) {
                    priceDataModel = (PriceDataModel) listObject.get(rows);
                    String bulan = priceDataModel.getDate();
                    if (bulan.equalsIgnoreCase("2020-09")) {
//                        priceFormat = Double.parseDouble(priceDataModel.getPrice()) / 1000;
                        priceFormat = Double.parseDouble(priceDataModel.getPrice());
                        DatapriceFormattedNumber = PriceFormat.format(priceFormat);
                        DataPrice[8] = DatapriceFormattedNumber;
                    } else {
                        if (DataPrice[8] == null) {
                            DataPrice[8] = "0";
                        }

                    }
                }
            }
            if (i == 9) {
                for (rows = 0; rows < listObject.size(); rows++) {
                    priceDataModel = (PriceDataModel) listObject.get(rows);
                    String bulan = priceDataModel.getDate();
                    if (bulan.equalsIgnoreCase("2020-10")) {
//                        priceFormat = Double.parseDouble(priceDataModel.getPrice()) / 1000;
                        priceFormat = Double.parseDouble(priceDataModel.getPrice());
                        DatapriceFormattedNumber = PriceFormat.format(priceFormat);
                        DataPrice[9] = DatapriceFormattedNumber;
                    } else {
                        if (DataPrice[9] == null) {
                            DataPrice[9] = "0";
                        }

                    }
                }
            }
            if (i == 10) {
                for (rows = 0; rows < listObject.size(); rows++) {
                    priceDataModel = (PriceDataModel) listObject.get(rows);
                    String bulan = priceDataModel.getDate();
                    if (bulan.equalsIgnoreCase("2020-11")) {
//                        priceFormat = Double.parseDouble(priceDataModel.getPrice()) / 1000;
                        priceFormat = Double.parseDouble(priceDataModel.getPrice());
                        DatapriceFormattedNumber = PriceFormat.format(priceFormat);
                        DataPrice[10] = DatapriceFormattedNumber;
                    } else {
                        if (DataPrice[10] == null) {
                            DataPrice[10] = "0";
                        }

                    }
                }
            }

            if (i == 11) {
                for (rows = 0; rows < listObject.size(); rows++) {
                    priceDataModel = (PriceDataModel) listObject.get(rows);
                    String bulan = priceDataModel.getDate();
                    if (bulan.equalsIgnoreCase("2020-12")) {
//                        priceFormat = Double.parseDouble(priceDataModel.getPrice()) / 1000;
                        priceFormat = Double.parseDouble(priceDataModel.getPrice());
                        DatapriceFormattedNumber = PriceFormat.format(priceFormat);
                        DataPrice[11] = DatapriceFormattedNumber;
                    } else {
                        if (DataPrice[11] == null) {
                            DataPrice[11] = "0";
                        }

                    }
                }
            }
        }
        for (int i = 0; i < DataPrice.length; i++) {
            entries.add(new Entry(i, Float.parseFloat(DataPrice[i])));
        }
        Log.e("Data", entries.toString());
        return entries;
    }

    private ArrayList getDataDays(List<Object> listObject, List<PriceDataModel> priceDataModels, int TotalData, ArrayList<String> arrlist, ArrayList<String> arrlistData) {
        String[] DataPrice = new String[arrlist.size()];
        NumberFormat PriceFormat = new DecimalFormat("####");
        double priceFormat = 0;
        String DatapriceFormattedNumber = null;
        for (int rows = 0; rows < arrlist.size(); rows++) {
            boolean isExist = false;
            for (PriceDataModel priceDataModel : priceDataModels) {
                if (priceDataModel.getDate().equals(arrlist.get(rows))) {
//                    priceFormat = Double.parseDouble(priceDataModel.getPrice()) / 1000;
                    priceFormat = Double.parseDouble(priceDataModel.getPrice());
                    DatapriceFormattedNumber = PriceFormat.format(priceFormat);
                    DataPrice[rows] = DatapriceFormattedNumber;
                    isExist = true;
                    break;
                }
            }

            if (!isExist) {
                DataPrice[rows] = "0";
            }
        }

        for (int i = 0; i < DataPrice.length; i++) {
            entries.add(new Entry(i, Float.parseFloat(DataPrice[i])));
        }
        Log.e("Data", entries.toString());
        return entries;
    }

    private ArrayList getDataWeeks(List<Object> listObject, List<PriceDataModel> priceDataModels, int TotalData, ArrayList<String> arrlist, ArrayList<String> arrlistData) {
        String[] DataPrice = new String[arrlist.size()];
        NumberFormat PriceFormat = new DecimalFormat("####");
        double priceFormat = 0;
        String DatapriceFormattedNumber = null;
        for (int rows = 0; rows < arrlist.size(); rows++) {
            boolean isExist = false;
            for (PriceDataModel priceDataModel : priceDataModels) {
                if (getWeekNumber(priceDataModel.getDate()).equals(arrlist.get(rows))) {
//                    priceFormat = Double.parseDouble(priceDataModel.getPrice()) / 1000;
                    priceFormat = Double.parseDouble(priceDataModel.getPrice());
                    DatapriceFormattedNumber = PriceFormat.format(priceFormat);
                    DataPrice[rows] = DatapriceFormattedNumber;
                    isExist = true;
                    break;
                }
            }

            if (!isExist) {
                DataPrice[rows] = "0";
            }
        }

        for (int i = 0; i < DataPrice.length; i++) {
            entries.add(new Entry(i, Float.parseFloat(DataPrice[i])));
        }
        Log.e("Data", entries.toString());
        return entries;
    }

    public static String[] GetStringArray(ArrayList<String> arr) {

        // declaration and initialise String Array
        String str[] = new String[arr.size()];

        // ArrayList to Array Conversion
        for (int j = 0; j < arr.size(); j++) {

            // Assign each value to String array
            str[j] = arr.get(j);
        }

        return str;
    }
}