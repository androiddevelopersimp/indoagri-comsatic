package com.indoagri.comsatic;

import android.app.Application;
import android.content.Context;

public class Apps extends Application {

    private static Apps sInstance;
    private static Context context;
    public static Apps getInstance() {
        return sInstance;
    }

    public static Context getAppContext() {
        return Apps.context;
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
            super.onCreate();
            Apps.context = getApplicationContext();


        sInstance = this;
    }
}