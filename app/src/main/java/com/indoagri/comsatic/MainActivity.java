package com.indoagri.comsatic;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.indoagri.comsatic.Common.Constants;
import com.indoagri.comsatic.Common.DeviceUtils;
import com.indoagri.comsatic.Retrofit.ApiServices;
import com.indoagri.comsatic.Retrofit.DB.DatabaseQuery;
import com.indoagri.comsatic.Retrofit.GroupCompanyResponse;
import com.indoagri.comsatic.Retrofit.GroupProductResponse;
import com.indoagri.comsatic.Retrofit.Model.GroupCompanyModel;
import com.indoagri.comsatic.Retrofit.Model.GroupProductModel;
import com.indoagri.comsatic.Retrofit.Model.PriceDataModel;
import com.indoagri.comsatic.Retrofit.Model.SalesBilling;
import com.indoagri.comsatic.Retrofit.Model.SalesDataModel;
import com.indoagri.comsatic.Retrofit.Model.UserModel;
import com.indoagri.comsatic.Retrofit.NetClient;
import com.indoagri.comsatic.Retrofit.PriceResponse;
import com.indoagri.comsatic.Retrofit.SalesDataResponse;
import com.indoagri.comsatic.Retrofit.SharePreference;
import com.indoagri.comsatic.Retrofit.UsersLoginResponse;
import com.indoagri.comsatic.price.PriceActivity;
import com.indoagri.comsatic.stock.StockActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    int months = Calendar.getInstance().get(Calendar.MONTH);
    int years = Calendar.getInstance().get(Calendar.YEAR);
    private static final String TAGCOMPANY = "GET COMPANY GROUP";
    ImageView imgSettings, imgReload;
    @SuppressLint("RestrictedApi")
    TextView textViewUser;
    SharePreference sharePreference;
    DatabaseQuery database;
    SalesBilling salesBilling;
    List<SalesBilling> salesBillingList;
    LinearLayout cardSales, cardStock, cardPrice;
    ProgressBar progressBar;
    ProgressDialog pDialog;
    ApiServices apiServices;
    GroupCompanyResponse groupCompanyResponse;
    List<GroupCompanyModel> groupCompanyModelList;
    GroupProductResponse groupProductResponse;
    List<GroupProductModel> groupProductModelList;
    SalesDataResponse salesDataResponse;
    List<SalesDataModel> salesDataModelList;
    PriceResponse priceResponse;
    List<PriceDataModel> priceDataModelList;
    Date today, year;
    String dateToStr;
    String Tahun;
    boolean isShowing;
    ImageView imgInfoVersion;
    UserModel userModel;
    UsersLoginResponse usersLoginResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharePreference = new SharePreference(MainActivity.this);
        database = new DatabaseQuery(MainActivity.this);
        apiServices = NetClient.DataProduction().create(ApiServices.class);
        progressBar = (findViewById(R.id.progress_bar));
        progressBar.setVisibility(View.GONE);
        pDialog = new ProgressDialog(MainActivity.this);
        isShowing = pDialog.isShowing();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        imgSettings = (findViewById(R.id.settings));
        imgReload = (findViewById(R.id.imgReload));
        imgReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //showYearDialog();
                showYearDialog();
            }
        });
        imgSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = sharePreference.isUserName();
                String email = sharePreference.isEmail();
                String Domain = sharePreference.isDomain();
                String password = sharePreference.isPassword();
                String DeviceId = DeviceUtils.getDeviceID(MainActivity.this);
                AsyncLogout(MainActivity.this, email, "Logout Process Initialitation", password, username, Domain);
            }
        });
        today = new Date();
        year = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        SimpleDateFormat years = new SimpleDateFormat("yyyy");
        dateToStr = format.format(today);
        Tahun = years.format(year);
        textViewUser = (findViewById(R.id.namaUser));
        textViewUser.setText(getResources().getString(R.string.app_name));
        cardSales = (findViewById(R.id.salesbill));
        cardSales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(MainActivity.this, com.indoagri.comsatic.SalesBilling.SalesBilling.class);
                startActivity(a);
            }
        });
        cardPrice = (findViewById(R.id.price));
        cardPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(MainActivity.this, PriceActivity.class);
                startActivity(a);
            }
        });
        cardStock = (findViewById(R.id.stock));
        cardStock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(MainActivity.this, StockActivity.class);
                startActivity(a);

            }
        });
        TextView txtUsername = (findViewById(R.id.txt_profile));
        String email = sharePreference.isEmail();
        String nama = sharePreference.isFullname();
        txtUsername.setText("Welcome,   " + nama);
        if (deletetableWithSales()) {
            GetDataCompany();
        }
        TextView txtVersion = (findViewById(R.id.txtVersion));
        txtVersion.setText("Vers. " + Constants.versionApps);
        imgInfoVersion = (findViewById(R.id.imgInfoVersion));
        if (sharePreference.isConfirmationUpdate()) {
            imgInfoVersion.setVisibility(View.GONE);
        } else {
            imgInfoVersion.setVisibility(View.VISIBLE);
        }
        imgInfoVersion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogInfoVersion();
            }
        });
        checkRow();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean deletetableWithSales() {
        if (!isShowing) {
            pDialog.setMessage("Loading");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        boolean result = false;
        database.openTransaction();
        int rowID = 0;
        int rowID2 = 0;
        try {
            rowID = database.deleteDataTemporary(GroupCompanyModel.TABLE_NAME, null, null);
            if (rowID > 0 || rowID == 0) {
                rowID2 = database.deleteDataTemporary(GroupProductModel.TABLE_NAME, null, null);
                if (rowID2 > 0 || rowID2 == 0) {
                    database.deleteDataTemporary(SalesBilling.TABLE_NAME, null, null);
                    database.deleteDataTemporary(PriceDataModel.TABLE_NAME, null, null);
                    database.deleteDataTable(GroupCompanyModel.TABLE_NAME, null, null);
                    database.deleteDataTable(GroupProductModel.TABLE_NAME, null, null);
                    database.deleteDataTable(SalesBilling.TABLE_NAME, null, null);
                    database.deleteDataTable(PriceDataModel.TABLE_NAME, null, null);
                }
            }

        } finally {

            database.commitTransaction();
            database.closeTransaction();
            result = true;
            if (!isShowing) {
                pDialog.setMessage("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            } else {
                pDialog.dismiss();
            }
        }
        return result;
    }

    private boolean deletetable() {
        if (!isShowing) {
            pDialog.setMessage("Loading");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        boolean result = false;
        database.openTransaction();
        int rowID = 0;
        int rowID2 = 0;
        try {
            rowID = database.deleteDataTemporary(GroupCompanyModel.TABLE_NAME, null, null);
            if (rowID > 0 || rowID == 0) {
                rowID2 = database.deleteDataTemporary(GroupProductModel.TABLE_NAME, null, null);
                if (rowID2 > 0 || rowID2 == 0) {
                    database.deleteDataTable(GroupCompanyModel.TABLE_NAME, null, null);
                    database.deleteDataTable(GroupProductModel.TABLE_NAME, null, null);
                }
            }

        } finally {

            database.commitTransaction();
            database.closeTransaction();
            result = true;
            if (!isShowing) {
                pDialog.setMessage("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            } else {
                pDialog.dismiss();
            }
        }
        return result;
    }


    private void GetDataCompany() {
        if (!isShowing) {
            pDialog.setMessage("Loading");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        Log.d(TAGCOMPANY, "LOADAGE: ");

        GetCompanies().enqueue(new Callback<GroupCompanyResponse>() {
            @Override
            public void onResponse(Call<GroupCompanyResponse> call, Response<GroupCompanyResponse> response) {
                // Got data. Send it to adapter
                pDialog.dismiss();
                if (response.code() == 200) {
                    groupCompanyResponse = fetchResultsCompanies(response);
                    groupCompanyModelList = groupCompanyResponse.getCompanies();
                    setupInsertCompany();

                } else {
                    Toast.makeText(getApplicationContext(), "Ada Kegagalan Server", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GroupCompanyResponse> call, Throwable t) {
                t.printStackTrace();
                //progressBar.setVisibility(View.GONE);
                pDialog.dismiss();
                // TODO: 08/11/16 handle failure
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void GetDataProduct() {
        if (!isShowing) {
            pDialog.setMessage("Loading");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        Log.d(TAGCOMPANY, "LOADAGE: ");

        GetProducts().enqueue(new Callback<GroupProductResponse>() {
            @Override
            public void onResponse(Call<GroupProductResponse> call, Response<GroupProductResponse> response) {
                // Got data. Send it to adapter
                pDialog.dismiss();
                if (response.code() == 200) {
                    groupProductResponse = fetchResultsProducts(response);
                    groupProductModelList = groupProductResponse.getProducts();
                    setupInsertProducts();
                } else {
                    Toast.makeText(getApplicationContext(), "Toast Gagal", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GroupProductResponse> call, Throwable t) {
                t.printStackTrace();
                pDialog.dismiss();
                // TODO: 08/11/16 handle failure
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void GetDataPrice() {
        if (!isShowing) {
            pDialog.setMessage("Loading");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        Log.d(TAGCOMPANY, "LOADAGE: ");

        GetPrice().enqueue(new Callback<PriceResponse>() {
            @Override
            public void onResponse(Call<PriceResponse> call, Response<PriceResponse> response) {
                // Got data. Send it to adapter
                pDialog.dismiss();
                if (response.code() == 200) {
                    priceResponse = PriceCurrent(response);
                    priceDataModelList = priceResponse.getPrice();
                    setupInsertPrice();
                } else {
                    Toast.makeText(getApplicationContext(), "Toast Gagal", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PriceResponse> call, Throwable t) {
                t.printStackTrace();
                pDialog.dismiss();
                // TODO: 08/11/16 handle failure
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void GetDataSales() {
        if (!isShowing) {
            pDialog.setMessage("Loading");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        Log.d(TAGCOMPANY, "LOADAGE: ");

        GetAllSales().enqueue(new Callback<SalesDataResponse>() {
            @Override
            public void onResponse(Call<SalesDataResponse> call, Response<SalesDataResponse> response) {
                pDialog.dismiss();
                if (response.code() == 200) {
                    salesDataResponse = AllResultSales(response);
                    salesDataModelList = salesDataResponse.getSales();
                    setupInsertSales();

                } else {
                    Toast.makeText(getApplicationContext(), "Toast Gagal", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SalesDataResponse> call, Throwable t) {
                t.printStackTrace();
                pDialog.dismiss();
                // TODO: 08/11/16 handle failure
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void GetDataSalesMonth(String MONTH) {
        if (deletetableSalesMonth() > 0) {
            if (!isShowing) {
                pDialog.setMessage("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            }
            Log.d(TAGCOMPANY, "LOADAGE: ");

            MonthResultSales(MONTH).enqueue(new Callback<SalesDataResponse>() {
                @Override
                public void onResponse(Call<SalesDataResponse> call, Response<SalesDataResponse> response) {
                    // Got data. Send it to adapter
                    pDialog.dismiss();
                    if (response.code() == 200) {
                        salesDataResponse = SalesMonth(response);
                        salesDataModelList = salesDataResponse.getSales();
                        setupInsertSales();
                    } else {
                        Toast.makeText(getApplicationContext(), "Toast Gagal", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SalesDataResponse> call, Throwable t) {
                    t.printStackTrace();
                    pDialog.dismiss();
                    // TODO: 08/11/16 handle failure
                    Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            if (!isShowing) {
                pDialog.setMessage("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            }
            Log.d(TAGCOMPANY, "LOADAGE: ");

            MonthResultSales(MONTH).enqueue(new Callback<SalesDataResponse>() {
                @Override
                public void onResponse(Call<SalesDataResponse> call, Response<SalesDataResponse> response) {
                    // Got data. Send it to adapter
                    pDialog.dismiss();
                    if (response.code() == 200) {
                        salesDataResponse = SalesThisMonth(response);
                        salesDataModelList = salesDataResponse.getSales();
                        setupInsertSales();

                    } else {
                        Toast.makeText(getApplicationContext(), "Toast Gagal", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SalesDataResponse> call, Throwable t) {
                    t.printStackTrace();
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void GetDataSalesMonthYear(String MONTH, String YEAR) {
        if (deletetableSalesMonth() > 0) {
            if (!isShowing) {
                pDialog.setMessage("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            }
            Log.d(TAGCOMPANY, "LOADAGE: ");

            MonthYearResultSales(MONTH, YEAR).enqueue(new Callback<SalesDataResponse>() {
                @Override
                public void onResponse(Call<SalesDataResponse> call, Response<SalesDataResponse> response) {
                    // Got data. Send it to adapter
                    pDialog.dismiss();
                    if (response.code() == 200) {
                        salesDataResponse = SalesMonth(response);
                        salesDataModelList = salesDataResponse.getSales();
                        setupInsertSales();
                    } else {
                        Toast.makeText(getApplicationContext(), "Toast Gagal", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SalesDataResponse> call, Throwable t) {
                    t.printStackTrace();
                    pDialog.dismiss();
                    // TODO: 08/11/16 handle failure
                    Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            if (!isShowing) {
                pDialog.setMessage("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            }
            Log.d(TAGCOMPANY, "LOADAGE: ");

            MonthYearResultSales(MONTH, YEAR).enqueue(new Callback<SalesDataResponse>() {
                @Override
                public void onResponse(Call<SalesDataResponse> call, Response<SalesDataResponse> response) {
                    // Got data. Send it to adapter
                    pDialog.dismiss();
                    if (response.code() == 200) {
                        salesDataResponse = SalesThisMonth(response);
                        salesDataModelList = salesDataResponse.getSales();
                        setupInsertSales();

                    } else {
                        Toast.makeText(getApplicationContext(), "Toast Gagal", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SalesDataResponse> call, Throwable t) {
                    t.printStackTrace();
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void GetDataSalesYear(String YEAR) {
        if (deletetableSalesYear() > 0) {
            if (!isShowing) {
                pDialog.setMessage("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            }
            Log.d(TAGCOMPANY, "LOADAGE: ");

            YearResultSales(YEAR).enqueue(new Callback<SalesDataResponse>() {
                @Override
                public void onResponse(Call<SalesDataResponse> call, Response<SalesDataResponse> response) {
                    // Got data. Send it to adapter
                    pDialog.dismiss();
                    if (response.code() == 200) {
                        salesDataResponse = SalesMonth(response);
                        salesDataModelList = salesDataResponse.getSales();
                        setupInsertSales();
                    } else {
                        Toast.makeText(getApplicationContext(), "Toast Gagal", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SalesDataResponse> call, Throwable t) {
                    t.printStackTrace();
                    pDialog.dismiss();
                    // TODO: 08/11/16 handle failure
                    Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            if (!isShowing) {
                pDialog.setMessage("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            }
            Log.d(TAGCOMPANY, "LOADAGE: ");

            YearResultSales(YEAR).enqueue(new Callback<SalesDataResponse>() {
                @Override
                public void onResponse(Call<SalesDataResponse> call, Response<SalesDataResponse> response) {
                    // Got data. Send it to adapter
                    pDialog.dismiss();
                    if (response.code() == 200) {
                        salesDataResponse = SalesThisMonth(response);
                        salesDataModelList = salesDataResponse.getSales();
                        setupInsertSales();

                    } else {
                        Toast.makeText(getApplicationContext(), "Toast Gagal", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SalesDataResponse> call, Throwable t) {
                    t.printStackTrace();
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void GetDataSalesThisMonth() {
        if (deletetableSales() > 0) {
            if (!isShowing) {
                pDialog.setMessage("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            }
            Log.d(TAGCOMPANY, "LOADAGE: ");

            ThisMonthResultSales().enqueue(new Callback<SalesDataResponse>() {
                @Override
                public void onResponse(Call<SalesDataResponse> call, Response<SalesDataResponse> response) {
                    // Got data. Send it to adapter
                    pDialog.dismiss();
                    if (response.code() == 200) {
                        salesDataResponse = SalesThisMonth(response);
                        salesDataModelList = salesDataResponse.getSales();
                        setupInsertSales();
                    } else {
                        Toast.makeText(getApplicationContext(), "Toast Gagal", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SalesDataResponse> call, Throwable t) {
                    t.printStackTrace();
                    pDialog.dismiss();
                    // TODO: 08/11/16 handle failure
                    Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            if (!isShowing) {
                pDialog.setMessage("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            }
            Log.d(TAGCOMPANY, "LOADAGE: ");

            ThisMonthResultSales().enqueue(new Callback<SalesDataResponse>() {
                @Override
                public void onResponse(Call<SalesDataResponse> call, Response<SalesDataResponse> response) {
                    // Got data. Send it to adapter
                    pDialog.dismiss();
                    if (response.code() == 200) {
                        salesDataResponse = SalesThisMonth(response);
                        salesDataModelList = salesDataResponse.getSales();
                        setupInsertSales();

                    } else {
                        Toast.makeText(getApplicationContext(), "Toast Gagal", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SalesDataResponse> call, Throwable t) {
                    t.printStackTrace();
                    pDialog.dismiss();
                    // TODO: 08/11/16 handle failure
                    Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void setupInsertCompany() {
        database.openTransaction();
        for (int i = 0; i < groupCompanyModelList.size(); i++) {
            GroupCompanyModel groupCompanyModel = groupCompanyModelList.get(i);
            ContentValues values = new ContentValues();
            values.put(GroupCompanyModel.XML_groupcompany, groupCompanyModel.getGroupcompany().trim());
            values.put(GroupCompanyModel.XML_companycode, groupCompanyModel.getCompanycode().trim());
            values.put(GroupCompanyModel.XML_description, groupCompanyModel.getDescription().trim());
            database.insertDataSQL(GroupCompanyModel.TABLE_NAME, values);
        }
        database.commitTransaction();
        database.closeTransaction();
        GetDataProduct();
    }

    private void setupInsertProducts() {
        database.openTransaction();
        for (int i = 0; i < groupProductModelList.size(); i++) {
            GroupProductModel groupProductModel = groupProductModelList.get(i);
            ContentValues values = new ContentValues();
            values.put(GroupProductModel.XML_groupproduct, groupProductModel.getGroupproduct().trim());
            values.put(GroupProductModel.XML_subgroup, groupProductModel.getSubgroup().trim());
            values.put(GroupProductModel.XML_material, groupProductModel.getMaterial().trim());
            values.put(GroupProductModel.XML_materialDescription, groupProductModel.getMaterialdescription().trim());
            database.insertDataSQL(GroupProductModel.TABLE_NAME, values);
        }
        database.commitTransaction();
        database.closeTransaction();
        GetDataPrice();
    }

    private void setupInsertPrice() {
        database.openTransaction();
        for (int i = 0; i < priceDataModelList.size(); i++) {
            PriceDataModel priceDataModel = priceDataModelList.get(i);
            ContentValues values = new ContentValues();
            values.put(PriceDataModel.XML_ID, priceDataModel.getId());
            values.put(PriceDataModel.XML_MATERIAL, priceDataModel.getMaterial());
            values.put(PriceDataModel.XML_DESCRIPTION, priceDataModel.getDescription());
            values.put(PriceDataModel.XML_PRICE, priceDataModel.getPrice());
            values.put(PriceDataModel.XML_DATE, priceDataModel.getDate().substring(0, 10));
            values.put(PriceDataModel.XML_GROUPPRODUCT, priceDataModel.getGrouproduct());
            values.put(PriceDataModel.XML_SUBGROUP, priceDataModel.getSubgroup());
            values.put(PriceDataModel.XML_PRICETYPE, priceDataModel.getPricetype());
            values.put(PriceDataModel.XML_CURRENCY, priceDataModel.getCurrency());
            database.insertDataSQL(PriceDataModel.TABLE_NAME, values);
        }
        database.commitTransaction();
        database.closeTransaction();
        if (checkRow() > 0) {
            GetDataSalesThisMonth();
        } else {
            GetDataSales();
        }
        initFooter();
    }

    private void setupInsertSales() {
        database.openTransaction();
        for (int i = 0; i < salesDataModelList.size(); i++) {
            SalesDataModel salesDataModel = salesDataModelList.get(i);

            ContentValues values = new ContentValues();
            values.put(SalesDataModel.XML_idx, salesDataModel.getIdx());
            values.put(SalesDataModel.XML_source, salesDataModel.getSource());
            values.put(SalesDataModel.XML_vbeln, salesDataModel.getVbeln());
            values.put(SalesDataModel.XML_posnr, salesDataModel.getPosnr());
            values.put(SalesDataModel.XML_vkorg, salesDataModel.getVkorg());
            values.put(SalesDataModel.XML_vtweg, salesDataModel.getVtweg());
            values.put(SalesDataModel.XML_spart, salesDataModel.getSpart());
            values.put(SalesDataModel.XML_erdat, salesDataModel.getErdat());
            values.put(SalesDataModel.XML_erzet, salesDataModel.getErzet());
            values.put(SalesDataModel.XML_audat, salesDataModel.getAudat());
            values.put(SalesDataModel.XML_auart, salesDataModel.getAuart());
            values.put(SalesDataModel.XML_kunnr, salesDataModel.getKunnr());
            values.put(SalesDataModel.XML_namE1, salesDataModel.getNameE1());
            values.put(SalesDataModel.XML_zterm, salesDataModel.getZterm());
            values.put(SalesDataModel.XML_incO1, salesDataModel.getInco1());
            values.put(SalesDataModel.XML_incO2, salesDataModel.getInco2());
            values.put(SalesDataModel.XML_werks, salesDataModel.getWerks());
            values.put(SalesDataModel.XML_vstel, salesDataModel.getVstel());
//            values.put(SalesDataModel.XML_matnr, salesDataModel.getMatnr().substring(12));
            values.put(SalesDataModel.XML_matnr,salesDataModel.getMatnr());
            values.put(SalesDataModel.XML_arktx, salesDataModel.getArktx());
            values.put(SalesDataModel.XML_kwmeng, salesDataModel.getKwmeng());
            values.put(SalesDataModel.XML_vrkme, salesDataModel.getVrkme());
            values.put(SalesDataModel.XML_netwr, salesDataModel.getNetwr());
            values.put(SalesDataModel.XML_waerk, salesDataModel.getWaerk());
            values.put(SalesDataModel.XML_gjahr, salesDataModel.getGjahr());
            values.put(SalesDataModel.XML_monat, salesDataModel.getMonat());

            database.insertDataSQL(SalesDataModel.TABLE_NAME, values);
        }
        database.commitTransaction();
        database.closeTransaction();
        new SharePreference(MainActivity.this).setUpdateSyncDateSales(dateToStr);
    }

    // PROTECTED //

    // company //
    private GroupCompanyResponse fetchResultsCompanies(Response<GroupCompanyResponse> response) {
        GroupCompanyResponse fleetsResponse = response.body();
        return fleetsResponse;
    }

    private Call<GroupCompanyResponse> GetCompanies() {
        return apiServices.getCompany();
    }

    // products //
    private GroupProductResponse fetchResultsProducts(Response<GroupProductResponse> response) {
        GroupProductResponse groupProductResponse = response.body();
        return groupProductResponse;
    }

    private Call<GroupProductResponse> GetProducts() {
        return apiServices.getProducts();
    }

    // salesdata //
    private SalesDataResponse AllResultSales(Response<SalesDataResponse> response) {
        SalesDataResponse fleetsResponse = response.body();
        return fleetsResponse;
    }

    private Call<SalesDataResponse> GetAllSales() {
        return apiServices.getDataSales();
    }


    // salesdata THis Month //
    private SalesDataResponse SalesThisMonth(Response<SalesDataResponse> response) {
        SalesDataResponse fleetsResponse = response.body();
        return fleetsResponse;
    }


    // salesdata Month //
    private SalesDataResponse SalesMonth(Response<SalesDataResponse> response) {
        SalesDataResponse fleetsResponse = response.body();
        return fleetsResponse;
    }

    private Call<SalesDataResponse> ThisMonthResultSales() {
        return apiServices.getDataSalesThisMonth();
    }

    private Call<SalesDataResponse> MonthResultSales(String month) {
        return apiServices.getDataSalesMonth(month);
    }

    private Call<SalesDataResponse> MonthYearResultSales(String month, String year) {
        return apiServices.getDataSalesMonthYear(month, year);
    }

    private Call<SalesDataResponse> YearResultSales(String year) {
        return apiServices.getDataSalesYear(year);
    }

    // Price //
    private PriceResponse PriceCurrent(Response<PriceResponse> response) {
        PriceResponse priceResponse = response.body();
        return priceResponse;
    }

    private Call<PriceResponse> GetPrice() {
        return apiServices.getDataPriceCurrent();
    }


    private void Comingsoon() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.information));
        builder.setMessage(getResources().getString(R.string.comming_soon));
        builder.setCancelable(false)
                .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        final AlertDialog alertdialog = builder.create();
        alertdialog.show();

        alertdialog.getWindow().setGravity(Gravity.BOTTOM);
    }

    private void Logout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to logout ?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        sharePreference.setLogin(false);
                        Intent a = new Intent(MainActivity.this, LoginActivity.class);
                        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        finish();
                        startActivity(a);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private int checkRow() {
        int totalRow = 0;
        totalRow = database.COUNT(SalesDataModel.TABLE_NAME, "SELECT * FROM " + SalesDataModel.TABLE_NAME);
        return totalRow;
    }

    private int deletetableSales() {
        int result = 0;
        database.openTransaction();
        try {
            String[] a = new String[2];
            a[0] = new SharePreference(MainActivity.this).isUpdateSyncSales().substring(3, 5);
            a[1] = Tahun;
            result = database.deleteDataTemporary(SalesDataModel.TABLE_NAME, "monat= ? and gjahr = ?", a);
        } finally {
            database.commitTransaction();
            database.closeTransaction();
        }
        return result;
    }

    private int deletetableSalesMonth() {
        int result = 0;
        database.openTransaction();
        try {
            String[] a = new String[2];
            a[0] = new SharePreference(MainActivity.this).isRefreshMonth();
            a[1] = Tahun;
            result = database.deleteDataTemporary(SalesDataModel.TABLE_NAME, "monat= ? and gjahr = ?", a);
        } finally {
            database.commitTransaction();
            database.closeTransaction();
        }
        return result;
    }

    private int deletetableSalesYear() {
        int result = 0;
        database.openTransaction();
        try {
            String[] a = new String[1];
            a[0] = new SharePreference(MainActivity.this).isRefreshYear();
            result = database.deleteDataTemporary(SalesDataModel.TABLE_NAME, "gjahr = ?", a);
        } finally {
            database.commitTransaction();
            database.closeTransaction();
        }
        return result;
    }

    public void showYearDialog() {

        final Dialog d = new Dialog(MainActivity.this);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.dialog_year);
        Button set = (Button) d.findViewById(R.id.btnOk);
        Button cancel = (Button) d.findViewById(R.id.btnCancel);
        TextView year_text = (TextView) d.findViewById(R.id.year_text);
        year_text.setText("Set Month");
        final NumberPicker yearpicker = (NumberPicker) d.findViewById(R.id.picker_year);

        yearpicker.setMinValue(2016);
        yearpicker.setMaxValue(2025);
        yearpicker.setWrapSelectorWheel(false);
        yearpicker.setValue(years);

        set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Years = String.valueOf(yearpicker.getValue());
                new SharePreference(MainActivity.this).setRefreshYear(Years);
                GetDataSalesYear(Years);
                d.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }

    public void showMonthYearDialog() {

        final Dialog d = new Dialog(MainActivity.this);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.dialog_month_year);
        Button set = (Button) d.findViewById(R.id.btnOk);
        Button cancel = (Button) d.findViewById(R.id.btnCancel);
        TextView year_text = (TextView) d.findViewById(R.id.year_text);
        year_text.setText("Set Month and Year");
        final NumberPicker monthpicker = (NumberPicker) d.findViewById(R.id.picker_month);
        final NumberPicker yearpicker = (NumberPicker) d.findViewById(R.id.picker_year);

        monthpicker.setMaxValue(12);
        monthpicker.setMinValue(1);
        monthpicker.setWrapSelectorWheel(false);
        monthpicker.setValue(months);
        monthpicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);


        yearpicker.setMinValue(2016);
        yearpicker.setMaxValue(2025);
        yearpicker.setWrapSelectorWheel(false);
        yearpicker.setValue(years);

        set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Months = String.valueOf(monthpicker.getValue() < 10 ? ("0" + monthpicker.getValue()) : (monthpicker.getValue()));
                String Years = String.valueOf(yearpicker.getValue());
                new SharePreference(MainActivity.this).setRefreshMonth(Months);
                new SharePreference(MainActivity.this).setRefreshYear(Years);
                // Toast.makeText(ActivityAR.this, Months+" "+Years, Toast.LENGTH_SHORT).show();
                //  GetDataARMonthYear();
                GetDataSalesMonthYear(Months, Years);
                d.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }

    private void showDialogInfoVersion() {
        final ArrayAdapter adapter = ArrayAdapter.createFromResource(
                getApplicationContext(), R.array.information, R.layout.item_spinner);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Informasi Pembaruan v." + BuildConfig.VERSION_NAME + " ");
        dialogBuilder.setNegativeButton("close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                sharePreference.setKonfirmationUpdate(true);
            }
        });
        dialogBuilder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        //Create alert dialog object via builder
        AlertDialog alertDialogObject = dialogBuilder.create();
        //Show the dialog
        alertDialogObject.show();
    }

    private void AsyncLogout(Context context, final String email, String DialogMessage, String password, final String username, String domain) {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage(DialogMessage);
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
        pDialog.show();
        Log.d("LOGOUT", "loadFirstPage: ");

        getUsersMobile(email, password, username, domain).enqueue(new Callback<UsersLoginResponse>() {
            @Override
            public void onResponse(Call<UsersLoginResponse> call, Response<UsersLoginResponse> response) {
                pDialog.dismiss();
                if (response.code() == 200) {
                    usersLoginResponse = fetchResults(response);
                    userModel = usersLoginResponse.getUserLogout();
                    Logout();
                } else {
                    Log.e("Error", "Logout Error");
                }
            }

            @Override
            public void onFailure(Call<UsersLoginResponse> call, Throwable t) {
                t.printStackTrace();
                pDialog.dismiss();

            }
        });

    }

    private UsersLoginResponse fetchResults(Response<UsersLoginResponse> response) {
        UsersLoginResponse loginResponse = response.body();
        return loginResponse;
    }

    private Call<UsersLoginResponse> getUsersMobile(String email, String password, String username, String domain) {
        return apiServices.LogoutPermission(Constants.APIKEY, username, password, domain, "939328493284983");
    }

    public void initFooter() {
        TextView tvLastUpdate = findViewById(R.id.txtJudulfooter);
        TextView tvCPO = findViewById(R.id.cpo);
        TextView tvPKO = findViewById(R.id.pko);
        TextView tvPK = findViewById(R.id.pk);
        TextView tvRss3Sicom = findViewById(R.id.rss3_sicom);
        TextView tvRss3Thai = findViewById(R.id.rss3_thai);
        TextView tvRss1Thai = findViewById(R.id.rss1_thai);
        TextView tvTsr20Sicom = findViewById(R.id.tsr20_sicom);
        TextView tvSmr20Malay = findViewById(R.id.smr20_malay);

        tvCPO.setText(getDataFooter("KPB Dumai"));
        tvPKO.setText(getDataFooter("PKO CIF ROtteredam"));
        tvPK.setText(getDataFooter("Astra Dumai"));
        tvRss3Sicom.setText(getDataFooterRubber("RSS3 SICOM","A00000000000000001"));
        tvRss3Thai.setText(getDataFooterRubber("RSS3 Thai","A00000000000000001"));
        tvRss1Thai.setText(getDataFooterRubber("RSS1 Thai","A00000000000000001"));
        tvTsr20Sicom.setText(getDataFooterRubber("TSR20 SICOM","A00000000000000002"));
        tvSmr20Malay.setText(getDataFooterRubber("SMR20 Malaysia","A00000000000000002"));
        tvLastUpdate.setText("Latest Update Price on " + getDataFooterLastUpdate());
    }

    public String getDataFooter(String type) {
        database.openTransaction();
        String retVal = "-";
        String sqlQuery = "Select Price From Table_price WHERE LOWER(PRICETYPE) = ? order by DATE desc limit 1";
        String[] a = new String[1];
        a[0] = type.toLowerCase();
        List<Object> listObject = database.getListDataRawQuery(sqlQuery, PriceDataModel.TABLE_NAME, a);
        database.closeTransaction();
        for (Object o : listObject) {
            PriceDataModel p = (PriceDataModel) o;
            retVal = p.getPrice();
        }
        return retVal;
    }

    public String getDataFooterRubber(String type,String material) {
        database.openTransaction();
        String retVal = "-";
        String sqlQuery = "Select Price From Table_price WHERE LOWER(PRICETYPE) = ? AND MATERIAL = ? order by DATE desc limit 1";
        String[] a = new String[2];
        a[0] = type.toLowerCase();
        a[1] = material;
        List<Object> listObject = database.getListDataRawQuery(sqlQuery, PriceDataModel.TABLE_NAME, a);
        database.closeTransaction();
        for (Object o : listObject) {
            PriceDataModel p = (PriceDataModel) o;
            retVal = p.getPrice();
        }
        return retVal;
    }

    public String getDataFooterLastUpdate() {
        SimpleDateFormat formatUpdateDated = new SimpleDateFormat("dd MMM yyyy");
        return formatUpdateDated.format(Calendar.getInstance().getTime());
    }
}
