package com.indoagri.comsatic.widget.font;

/**
 * Created by Dwi.Fajar on 2/22/2018.
 */

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Dwi.Fajar on 2/21/2018.
 */

@SuppressLint("AppCompatCustomView")
public class RobotoTextView extends TextView {

    /*
     * Caches typefaces based on their file path and name, so that they don't have to be created every time when they are referenced.
     */
    private static Typeface mTypeface;

    public RobotoTextView(final Context context) {
        this(context, null);
    }

    public RobotoTextView(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RobotoTextView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);

        if (mTypeface == null) {
            mTypeface = Typeface.createFromAsset(context.getAssets(), "RobotoCondensed-Light.ttf");
        }
        setTypeface(mTypeface);
    }

}