package com.indoagri.comsatic.widget.utils;


import android.net.ParseException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/***
 * Provides helper methods for date utilities.
 * Yes, some times I write shit code.
 */
public class DateLibs {

    public static String getDay(String date) {
        // format dd/MM/YYYY;
        Date dateDT = parseDate(date);
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+07:00"));
            String currentDateTime = dateFormat.format(dateDT); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }

    }
    public static String getHariini() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        df.setTimeZone(TimeZone.getTimeZone("GMT+07:00"));
        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DAY_OF_MONTH, 0);
        Date todate1 = cal1.getTime();
        String startdate = df.format(todate1);
        return startdate;
    }

    public static String getTanggalandTime(String date) {
        // format dd/MM/YYYY;
        Date dateDT = parseDate(date);
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+07:00"));
            String currentDateTime = dateFormat.format(dateDT); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }

    }



    public static String getTgl(String date) {
        // format dd/MM/YYYY;
        Date dateDT = parseDate(date);
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd");
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+07:00"));
            String currentDateTime = dateFormat.format(dateDT); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }

    }
    public static String getMonth2(String date) {
        // format dd/MM/YYYY;
        Date dateDT = parseDate(date);
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("MM");
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+07:00"));
            String currentDateTime = dateFormat.format(dateDT); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }

    }
    public static String getYear(String date) {
        // format dd/MM/YYYY;
        Date dateDT = parseDate(date);
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+07:00"));
            String currentDateTime = dateFormat.format(dateDT); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }

    }
    public static String ConvertDateToDateTime(String dateString)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date convertedDate = null;
        try {
            try {
                convertedDate = dateFormat.parse(dateString);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String startdate = dateFormat.format(convertedDate);
        return startdate;
    }
    public static String getToday() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        df.setTimeZone(TimeZone.getTimeZone("GMT+07:00"));
        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DAY_OF_MONTH, 0);
        Date todate1 = cal1.getTime();
        String startdate = df.format(todate1);
        return startdate;
    }
    public static String getTodayDate() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        df.setTimeZone(TimeZone.getTimeZone("GMT+07:00"));
        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DAY_OF_MONTH, 0);
        Date todate1 = cal1.getTime();
        String startdate = df.format(todate1);
        return startdate;
    }
    public static String nextTomorrow() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        df.setTimeZone(TimeZone.getTimeZone("GMT+07:00"));
        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DAY_OF_MONTH, 1);
        Date todate1 = cal1.getTime();
        String startdate = df.format(todate1);
        return startdate;
    }

    public static String nextWeek() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        df.setTimeZone(TimeZone.getTimeZone("GMT+07:00"));
        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.WEEK_OF_MONTH, 1);
        Date todate1 = cal1.getTime();
        String startdate = df.format(todate1);
        return startdate;
    }

    public static String nextMonth() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        df.setTimeZone(TimeZone.getTimeZone("GMT+07:00"));
        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.MONTH, 1);
        Date todate1 = cal1.getTime();
        String startdate = df.format(todate1);
        return startdate;
    }
    public static String beforeMonth(int jumlah) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        df.setTimeZone(TimeZone.getTimeZone("GMT+07:00"));
        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.MONTH, -jumlah);
        Date todate1 = cal1.getTime();
        String startdate = df.format(todate1);
        return startdate;
    }

    public static String getUtcTime(String dateAndTime) {
        Date d = parseDate(dateAndTime);

        String format = "yyyy-MM-dd'T'HH:mm:ss'Z'";
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());

        // Convert Local Time to UTC
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        return sdf.format(d);
    }

    /****
     * Parses date string and return a {@link Date} object
     *
     * @return The ISO formatted date object
     */
    public static Date parseDate(String date) {

        if (date == null) {
            return null;
        }

        StringBuffer sbDate = new StringBuffer();
        sbDate.append(date);
        String newDate = null;
        Date dateDT = null;

        try {
            newDate = sbDate.substring(0, 19).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String rDate = newDate.replace("T", " ");
        String nDate = rDate.replaceAll("-", "/");

        try {
            dateDT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault()).parse(nDate);
            // Log.v( TAG, "#parseDate dateDT: " + dateDT );
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dateDT;
    }

    public static Date toLocalTime(String utcDate, SimpleDateFormat sdf) throws Exception {

        // create a new Date object using
        // the timezone of the specified city
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date localDate = sdf.parse(utcDate);

        sdf.setTimeZone(TimeZone.getDefault());
        String dateFormateInUTC = sdf.format(localDate);

        return sdf.parse(dateFormateInUTC);
    }

    /**
     * Returns abbreviated (3 letters) day of the week.
     *
     * @param date ISO format date
     * @return The name of the day of the week
     */
    public static String getDayOfWeekAbbreviated(String date) {
        Date dateDT = parseDate(date);

        if (dateDT == null) {
            return null;
        }

        // Get current date
        Calendar c = Calendar.getInstance();
        // it is very important to
        // set the date of
        // the calendar.
        c.setTime(dateDT);
        int day = c.get(Calendar.DAY_OF_WEEK);

        String dayStr = null;

        switch (day) {

            case Calendar.SUNDAY:
                dayStr = "Sun";
                break;

            case Calendar.MONDAY:
                dayStr = "Mon";
                break;

            case Calendar.TUESDAY:
                dayStr = "Tue";
                break;

            case Calendar.WEDNESDAY:
                dayStr = "Wed";
                break;

            case Calendar.THURSDAY:
                dayStr = "Thu";
                break;

            case Calendar.FRIDAY:
                dayStr = "Fri";
                break;

            case Calendar.SATURDAY:
                dayStr = "Sat";
                break;
        }

        return dayStr;
    }

    /***
     * Gets the name of the month from the given date.
     *
     * @param date ISO format date
     * @return Returns the name of the month
     */
    public static String getMonth(String date) {
        Date dateDT = parseDate(date);

        if (dateDT == null) {
            return null;
        }

        // Get current date
        Calendar c = Calendar.getInstance();
        // it is very important to
        // set the date of
        // the calendar.
        c.setTime(dateDT);
        int day = c.get(Calendar.MONTH);

        String dayStr = null;

        switch (day) {

            case Calendar.JANUARY:
                dayStr = "January";
                break;

            case Calendar.FEBRUARY:
                dayStr = "February";
                break;

            case Calendar.MARCH:
                dayStr = "March";
                break;

            case Calendar.APRIL:
                dayStr = "April";
                break;

            case Calendar.MAY:
                dayStr = "May";
                break;

            case Calendar.JUNE:
                dayStr = "June";
                break;

            case Calendar.JULY:
                dayStr = "July";
                break;

            case Calendar.AUGUST:
                dayStr = "August";
                break;

            case Calendar.SEPTEMBER:
                dayStr = "September";
                break;

            case Calendar.OCTOBER:
                dayStr = "October";
                break;

            case Calendar.NOVEMBER:
                dayStr = "November";
                break;

            case Calendar.DECEMBER:
                dayStr = "December";
                break;
        }

        return dayStr;
    }

    /**
     * Gets abbreviated name of the month from the given date.
     *
     * @param date ISO format date
     * @return Returns the name of the month
     */
    public static String getMonthAbbreviated(String date) {
        Date dateDT = parseDate(date);

        if (dateDT == null) {
            return null;
        }

        // Get current date
        Calendar c = Calendar.getInstance();
        // it is very important to
        // set the date of
        // the calendar.
        c.setTime(dateDT);
        int day = c.get(Calendar.MONTH);

        String dayStr = null;

        switch (day) {

            case Calendar.JANUARY:
                dayStr = "Jan";
                break;

            case Calendar.FEBRUARY:
                dayStr = "Feb";
                break;

            case Calendar.MARCH:
                dayStr = "Mar";
                break;

            case Calendar.APRIL:
                dayStr = "Apr";
                break;

            case Calendar.MAY:
                dayStr = "May";
                break;

            case Calendar.JUNE:
                dayStr = "Jun";
                break;

            case Calendar.JULY:
                dayStr = "Jul";
                break;

            case Calendar.AUGUST:
                dayStr = "Aug";
                break;

            case Calendar.SEPTEMBER:
                dayStr = "Sep";
                break;

            case Calendar.OCTOBER:
                dayStr = "Oct";
                break;

            case Calendar.NOVEMBER:
                dayStr = "Nov";
                break;

            case Calendar.DECEMBER:
                dayStr = "Dec";
                break;
        }

        return dayStr;
    }


    public static String getTimeStamp(Date originalDate){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+07:00"));
        String currentDateTime = dateFormat.format(new Date());
        return dateFormat.format(new Date());
    }


    public static String getTimeStamp() {
        // format yyyy-MM-dd HH:mm:ss";
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+07:00"));
            String currentDateTime = dateFormat.format(new Date()); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }

    }
    public static String getTimeStamp2() {
            // format yyyy-MM-dd;
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+07:00"));
            String currentDateTime = dateFormat.format(new Date()); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }

    }

    public static Long getDateTime(){
        Date date1 = null;
        long difference = 0;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+07:00"));
        Date date = new Date();
        String todate = dateFormat.format(date);
        try {
            date1 = dateFormat.parse(todate);
            difference = date1.getTime();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return difference;
    }
    public static Long getDateTimeBy(String SDate){
        Date date1 = null;
        long difference = 0;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+07:00"));

        try {
            date1 = dateFormat.parse(SDate);
            difference = date1.getTime();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return difference;
    }

    public static String ChangeDate(String Date){
        String Result = null;
        String month = "";
        String Years = Date.substring(2,4);
        String Tgl = Date.substring(8,10);
        String Mon = Date.substring(5,7);
        if(Mon.equals("01")){
            month="Jan";
        }
        else if(Mon.equals("02")){
            month="Feb";
        }
        else if(Mon.equals("02")){
            month="Feb";
        }
        else if(Mon.equals("03")){
            month="Mar";
        }
        else if(Mon.equals("04")){
            month="Apr";
        }
        else if(Mon.equals("05")){
            month="May";
        }
        else if(Mon.equals("06")){
            month="Jun";
        }
        else if(Mon.equals("07")){
            month="Jul";
        }
        else if(Mon.equals("08")){
            month="Aug";
        }
        else if(Mon.equals("09")){
            month="Sep";
        }
        else if(Mon.equals("10")){
            month="Oct";
        }
        else if(Mon.equals("11")){
            month="Nov";
        }
        else if(Mon.equals("12")){
            month="Des";
        }
        Result = Tgl+"-"+month+"-"+Years;
        return Result;
    }


}
