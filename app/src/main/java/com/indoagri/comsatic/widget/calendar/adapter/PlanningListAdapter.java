package com.indoagri.comsatic.widget.calendar.adapter;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.indoagri.comsatic.R;
import com.indoagri.comsatic.widget.calendar.activity.CustomCalendar;
import com.indoagri.comsatic.widget.calendar.adapter.handler.ItemClickListener;
import com.indoagri.comsatic.widget.calendar.model.CalendarCollection;
import com.indoagri.comsatic.widget.calendar.views.PlanningView;

import java.util.ArrayList;


/**
 * Created by Soegidev on 11/22/2016.
 */

public class PlanningListAdapter extends RecyclerView.Adapter<PlanningListAdapter.ViewHolder> {
    RelativeLayout RLMenu;
    Dialog dialog = null;
    //DialogFragment RS;
    private Context mContext;
    private ArrayList<CalendarCollection> planningDetailModels;
    View mView;
    Activity mActivity;
    String pilihString;
    PlanningView planningView;
    String date;
    CustomCalendar.CalendarListener calendarListener;

    public PlanningListAdapter(Context context, Activity activity, View view, ArrayList<CalendarCollection> items, String date, String pilih, CustomCalendar.CalendarListener calendarListener) {
        mView = view;
        pilihString = pilih;
        mContext = context;
        mActivity = activity;
        this.date = date;
        this.planningDetailModels = items;
        this.planningView = new PlanningView(view, context, activity, planningDetailModels, calendarListener);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        ViewHolder viewHolder;
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_planning_items_views_edit, parent, false);
        viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    public void onBindViewHolder(final ViewHolder holder, final int position) {
        //myFormater = new DecimalFormat("#,###,###,###");
        final CalendarCollection list = planningDetailModels.get(position);
        holder.textNo.setText(String.valueOf(holder.getAdapterPosition() + 1));
        holder.textDestination.setText(list.getEvent_message());
        holder.feed = list;
        holder.textCat.setText(list.getDuration());
        holder.textCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // showDetail(items.get(position).getNostruk().toString());
                //   Toast.makeText(view.getContext(),"TEXT CLICK "+list.getStoreName(),Toast.LENGTH_SHORT).show();

             /*   Intent intent = new Intent();
                intent.putExtra("STOREID",list.getStoreID());
                intent.putExtra("STORENAME",list.getStoreName());
                mActivity.setResult(Activity.RESULT_OK,intent);
                mActivity.finish();
                mActivity.overridePendingTransition(R.anim.fade_in,R.anim.fade_out);*/

            }
        });

        holder.setClickListener(new ItemClickListener() {
            @Override
            public void onClick(final View view, final int position, boolean isLongClick) {
                if (isLongClick) {
                    //Toast.makeText(view.getContext(),  String.valueOf(items.get(position).getRoomName()), Toast.LENGTH_SHORT).show();
                } else {
                    //Toast.makeText(view.getContext(),  String.valueOf(items.get(position).getRoomName()), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        public TextView textNo, textDestination, textCat;
        public CalendarCollection feed;
        // public EventObject feed;
        private ItemClickListener clickListener;

        public ViewHolder(View itemView) {
            super(itemView);
            textNo = (TextView) itemView.findViewById(R.id.textNo);
            textDestination = (TextView) itemView.findViewById(R.id.textDestination);
            textCat = (TextView) itemView.findViewById(R.id.textCat);
            itemView.setTag(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

        }

        // itemView.setOnClickListener((View.OnClickListener) this);
        public void setClickListener(ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View v) {
            clickListener.onClick(v, getPosition(), false);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onClick(v, getPosition(), true);
            return true;
        }
    }


    @Override
    public int getItemCount() {
        return planningDetailModels.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}
