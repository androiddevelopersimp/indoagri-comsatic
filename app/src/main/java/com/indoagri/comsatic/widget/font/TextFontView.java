package com.indoagri.comsatic.widget.font;

/**
 * Created by Dwi.Fajar on 2/21/2018.
 */

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by Dwi.Bagus on 10/3/2017.
 */

public class TextFontView {
    public static Typeface getFont(Context context, int code) {
        switch(code) {
//            case 600: return context.getResources().getString(R.string.error_code_600);
            case 0: return Typeface.createFromAsset(context.getAssets(),"arial.ttf");
            case 1: return Typeface.createFromAsset(context.getAssets(),"segoeui.ttf");
            case 2: return Typeface.createFromAsset(context.getAssets(),"Candara.ttf");
            case 3: return Typeface.createFromAsset(context.getAssets(),"Montserrat-Light.otf");
            case 4: return Typeface.createFromAsset(context.getAssets(),"Montserrat-Medium.otf");
            case 5: return Typeface.createFromAsset(context.getAssets(),"Montserrat-SemiBold.otf");
            case 6: return Typeface.createFromAsset(context.getAssets(),"COOPBL.TTF");
            case 7: return Typeface.createFromAsset(context.getAssets(),"impact.ttf");
            case 8: return Typeface.createFromAsset(context.getAssets(),"HelveticaNeue.TTF");
            case 9: return Typeface.createFromAsset(context.getAssets(),"Roboto-Light.ttf");
            case 10: return Typeface.createFromAsset(context.getAssets(),"RobotoCondensed-Light.ttf");
            case 11: return Typeface.createFromAsset(context.getAssets(),"Chunkfive.TTF");
            case 12: return Typeface.createFromAsset(context.getAssets(),"calibri.ttf");
            case 13: return Typeface.createFromAsset(context.getAssets(),"Candaraz.ttf");
            case 14: return Typeface.createFromAsset(context.getAssets(),"Candarai.TTF");
            default: return Typeface.createFromAsset(context.getAssets(),"arial.ttf");
        }
    }

}
