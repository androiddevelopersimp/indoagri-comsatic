package com.indoagri.comsatic.widget.calendar.views;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.indoagri.comsatic.R;
import com.indoagri.comsatic.widget.calendar.activity.CustomCalendar;
import com.indoagri.comsatic.widget.calendar.adapter.PlanningCalendar_Adapter;
import com.indoagri.comsatic.widget.calendar.adapter.PlanningListAdapter;
import com.indoagri.comsatic.widget.calendar.model.CalendarCollection;
import com.indoagri.comsatic.widget.utils.DateLibs;
import com.indoagri.comsatic.widget.utils.ToastMessage;

import java.util.ArrayList;
import java.util.GregorianCalendar;


public class PlanningView {
    Activity mActivity;
    Context mContext;
    ToastMessage toastMessage;
    View mrootView;
    ProgressDialog myDialog;
    private ArrayList<CalendarCollection> planningModels = new ArrayList<>();
    public GregorianCalendar cal_month, cal_month_copy;
    private PlanningCalendar_Adapter cal_adapter;
    private TextView tv_month;
    PlanningListAdapter planningListAdapter;
    CustomCalendar.CalendarListener calendarListener;


    public PlanningView(View rootView, Context context, Activity activity, ArrayList<CalendarCollection> calendarCollection, CustomCalendar.CalendarListener calendarListener) {
        this.mContext = context;
        this.mrootView = rootView;
        this.mActivity = activity;
        this.planningModels = calendarCollection;
        this.calendarListener = calendarListener;
        toastMessage = new ToastMessage(context);
        myDialog = new ProgressDialog(context);
    }

    public void AmbilPlanHeader(final View mrootView, final Context mContext, final Activity mActivity) {

        try {
            setGrid(mrootView, mContext, mActivity, planningModels);
            AmbilPlanning(mrootView, mContext, mActivity, DateLibs.getHariini() + " " + DateLibs.getTimeStamp(), planningModels,0);

        } catch (Exception e) {
            toastMessage.shortMessage(e.toString());
        }
    }

    public void setGrid(final View view, final Context context, final Activity mActivity, ArrayList<CalendarCollection> date_collection_arr) {

        cal_month = (GregorianCalendar) GregorianCalendar.getInstance();
        cal_month_copy = (GregorianCalendar) cal_month.clone();
        cal_adapter = new PlanningCalendar_Adapter(view, context, mActivity, cal_month, planningModels, calendarListener );

        tv_month = (TextView) view.findViewById(R.id.tv_month);
        tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));

        ImageButton previous = (ImageButton) view.findViewById(R.id.ib_prev);

        previous.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setPreviousMonth();
                refreshCalendar();
            }
        });

        ImageButton next = (ImageButton) view.findViewById(R.id.Ib_next);
        next.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setNextMonth();
                refreshCalendar();

            }
        });


        GridView gridview = (GridView) view.findViewById(R.id.gv_calendar);
        gridview.setAdapter(cal_adapter);
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                ((PlanningCalendar_Adapter) parent.getAdapter()).setSelected(v, position);
                String selectedGridDate = PlanningCalendar_Adapter.day_string
                        .get(position);
                String[] separatedTime = selectedGridDate.split("-");
                String gridvalueString = separatedTime[2].replaceFirst("^0*", "");
                int gridvalue = Integer.parseInt(gridvalueString);


                if ((gridvalue > 10) && (position < 8)) {
                    setPreviousMonth();
                    refreshCalendar();
                } else if ((gridvalue < 7) && (position > 28)) {
                    setNextMonth();
                    refreshCalendar();
                }
                ((PlanningCalendar_Adapter) parent.getAdapter()).setSelected(v, position);


                ((PlanningCalendar_Adapter) parent.getAdapter()).getPositionList(selectedGridDate, view, context, mActivity);
            }

        });

    }


    protected void setNextMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month
                .getActualMaximum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) + 1),
                    cal_month.getActualMinimum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH,
                    cal_month.get(GregorianCalendar.MONTH) + 1);
        }

    }

    protected void setPreviousMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month
                .getActualMinimum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) - 1),
                    cal_month.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH,
                    cal_month.get(GregorianCalendar.MONTH) - 1);
        }

    }

    public void refreshCalendar() {
        cal_adapter.refreshDays();
        cal_adapter.notifyDataSetChanged();
        tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));
    }

    public void AmbilPlanning(final View mrootView, final Context mContext, final Activity mActivity, final String DateDay, ArrayList<CalendarCollection> date_collection_arr, int click) {
        final RelativeLayout relativeHeader = (RelativeLayout) mrootView.findViewById(R.id.detailHeader);
        final RelativeLayout relativeContent = (RelativeLayout) mrootView.findViewById(R.id.detailContent);
        final RecyclerView recyclerView = (RecyclerView) mrootView.findViewById(R.id.recyclerViewPlanDetail);
        final LinearLayout layarList = (LinearLayout) mrootView.findViewById(R.id.layarLIST);
        final LinearLayout layarNULL = (LinearLayout) mrootView.findViewById(R.id.layarNULL);
        final TextView textSummary = (TextView) mrootView.findViewById(R.id.textSummary);
        textSummary.setVisibility(View.GONE);
        String hal1 = String.valueOf(DateLibs.getDateTimeBy(DateDay.substring(0, 10)));
        String hal2 = String.valueOf(DateLibs.getDateTimeBy(DateLibs.getHariini().substring(0, 10)));
        textSummary.setText("Tanggal " + DateLibs.ChangeDate(DateDay));
        long pilihdate = DateLibs.getDateTimeBy(DateDay.substring(0, 10));
        long hariini = DateLibs.getDateTimeBy(DateLibs.getHariini().substring(0, 10));

        ArrayList<CalendarCollection> cals = new ArrayList<>();
        for (CalendarCollection cal : date_collection_arr) {
            if (DateDay.substring(0, 10).equals(cal.getDate())) {
                cals.add(cal);
            }
        }

        if (cals.size() == 0) {
            relativeHeader.setVisibility(View.GONE);
            relativeContent.setVisibility(View.VISIBLE);
            layarList.setVisibility(View.GONE);
            layarNULL.setVisibility(View.VISIBLE);
//            long Hasil = DateLibs.getDateTimeBy(DateDay.substring(0, 10)) - DateLibs.getDateTimeBy(DateLibs.beforeMonth(1));
//            if (Hasil <= 0) {
//
//            } else {
////                dialoxAddPlanning("0",DateDay);
//            }
            if(click == 1){
                calendarListener.onDayClick(DateDay);
            }
        } else {
            planningListAdapter = new PlanningListAdapter(mContext, mActivity, mrootView, cals, DateDay, "0", calendarListener);
            RecyclerView.LayoutManager HorizontalLayout = new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false);
            recyclerView.setLayoutManager(HorizontalLayout);
            recyclerView.setAdapter(planningListAdapter);
            planningListAdapter.notifyDataSetChanged();
            relativeHeader.setVisibility(View.GONE);
            relativeContent.setVisibility(View.VISIBLE);
            layarList.setVisibility(View.GONE);
            layarNULL.setVisibility(View.GONE);
            if(click == 1){
                calendarListener.onDayClick(DateDay);
            }
        }
    }


}
