package com.indoagri.comsatic.widget.calendar.activity;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.indoagri.comsatic.R;
import com.indoagri.comsatic.widget.calendar.model.CalendarCollection;
import com.indoagri.comsatic.widget.calendar.views.PlanningView;

import java.util.ArrayList;

public class CustomCalendar extends LinearLayout {

    ArrayList<CalendarCollection> calendarCollections;

    PlanningView planningView;
    CoordinatorLayout cdLayout;
    View rootView;
    private CalendarListener calendarListener;

    public CustomCalendar(Context context) {
        super(context);
        init(null);
    }

    public CustomCalendar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CustomCalendar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(@Nullable AttributeSet set) {

        if (isInEditMode()) {
            return;
        }

        LayoutInflater inflate = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        rootView = inflate.inflate(R.layout.activity_calendar, this, true);
        findViewsById(rootView);

        calendarCollections = new ArrayList<>();
    }


    private void findViewsById(View view) {
        cdLayout = view.findViewById(R.id.cdLayout);
    }


    public void setEventDate(String date, String eventMessage, String duration) {
        calendarCollections.add(new CalendarCollection(date, eventMessage, duration));
    }

    public void clearEventDate() {
        calendarCollections.clear();
    }

    public void refresh() {
        planningView = new PlanningView(rootView, rootView.getContext(), (Activity) rootView.getContext(), calendarCollections, calendarListener);
        planningView.AmbilPlanHeader(rootView, rootView.getContext(), (Activity) rootView.getContext());
    }


    public void setCalendarListener(CalendarListener calendarListener) {
        this.calendarListener = calendarListener;
    }

    public interface CalendarListener {
        void onDayClick(String date);
    }


}
