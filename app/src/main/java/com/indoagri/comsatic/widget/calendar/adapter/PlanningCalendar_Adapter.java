package com.indoagri.comsatic.widget.calendar.adapter;


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.indoagri.comsatic.R;
import com.indoagri.comsatic.widget.calendar.activity.CustomCalendar;
import com.indoagri.comsatic.widget.calendar.model.CalendarCollection;
import com.indoagri.comsatic.widget.calendar.views.PlanningView;
import com.indoagri.comsatic.widget.utils.DateLibs;
import com.indoagri.comsatic.widget.utils.ToastMessage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;


public class PlanningCalendar_Adapter extends BaseAdapter {
    private Context mContext;

    private java.util.Calendar month;
    public GregorianCalendar pmonth;
    /**
     * calendar instance for previous month for getting complete view
     */
    public GregorianCalendar pmonthmaxset;
    private GregorianCalendar selectedDate;
    int firstDay;
    int maxWeeknumber;
    int maxP;
    int calMaxP;
    int lastWeekDay;
    int leftDays;
    int mnthlength;
    String itemvalue, curentDateString;
    DateFormat df;

    Activity mactivity;
    private ArrayList<String> items;
    public static List<String> day_string;
    private View previousView;
    View mView;
    public ArrayList<CalendarCollection> date_collection_arr;
    ToastMessage toastMessage;
    CustomCalendar.CalendarListener calendarListener;

    public PlanningCalendar_Adapter(View view, Context context, Activity activity, GregorianCalendar monthCalendar, ArrayList<CalendarCollection> date_collection_arr, CustomCalendar.CalendarListener calendarListener) {
        this.date_collection_arr = date_collection_arr;
        PlanningCalendar_Adapter.day_string = new ArrayList<String>();
        Locale.setDefault(Locale.US);
        month = monthCalendar;
        mactivity = activity;
        selectedDate = (GregorianCalendar) monthCalendar.clone();
        this.mContext = context;
        this.calendarListener = calendarListener;
        month.set(GregorianCalendar.DAY_OF_MONTH, 1);
        mView = view;
        this.items = new ArrayList<String>();
        df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        curentDateString = df.format(selectedDate.getTime());
        refreshDays();
        toastMessage = new ToastMessage(context);
    }

    public void setItems(ArrayList<String> items) {
        for (int i = 0; i != items.size(); i++) {
            if (items.get(i).length() == 1) {
                items.set(i, "0" + items.get(i));
            }
        }
        this.items = items;
    }

    public int getCount() {
        return day_string.size();
    }

    public Object getItem(int position) {
        return day_string.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new view for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        TextView dayView;
        ImageView eventView;
        if (convertView == null) { // if it's not recycled, initialize some
            // attributes
            LayoutInflater vi = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.list_item_calendar, null);

        }
        dayView = (TextView) v.findViewById(R.id.date);
        eventView = (ImageView) v.findViewById(R.id.date_icon);
        String[] separatedTime = day_string.get(position).split("-");
        String gridvalue = separatedTime[2].replaceFirst("^0*", "");
        if ((Integer.parseInt(gridvalue) > 1) && (position < firstDay)) {
            eventView.setVisibility(View.GONE);
            dayView.setTextColor(Color.GRAY);
            dayView.setBackgroundColor(0);
            dayView.setClickable(false);
            dayView.setFocusable(false);
        } else if ((Integer.parseInt(gridvalue) < 7) && (position > 28)) {
            eventView.setVisibility(View.GONE);
            dayView.setTextColor(Color.GRAY);
            dayView.setBackgroundColor(0);
            dayView.setClickable(false);
            dayView.setFocusable(false);
        } else {
            // setting curent month's days in blue color.
            dayView.setTextColor(Color.WHITE);
            dayView.setBackgroundColor(0);
            eventView.setVisibility(View.GONE);
        }


        if (day_string.get(position).equals(curentDateString)) {
            dayView.setBackgroundResource(R.drawable.rounded_calender_item);
        } else {
            v.setBackgroundColor(Color.parseColor("#343434"));
        }


        dayView.setText(gridvalue);
        // create date string for comparison
        String date = day_string.get(position);

        if (date.length() == 1) {
            date = "0" + date;
        }
        String monthStr = "" + (month.get(GregorianCalendar.MONTH) + 1);
        if (monthStr.length() == 1) {
            monthStr = "0" + monthStr;
        }
        setEventView(v, position, dayView, eventView);

        return v;
    }

    public View setSelected(View view, int pos) {
        LinearLayout nowView = (LinearLayout) view.findViewById(R.id.date_box);
        if (previousView != null) {
            previousView.setBackgroundColor(Color.parseColor("#343434"));
//            nowView.setBackgroundResource(0);
        }


//        view.setBackgroundColor(Color.CYAN);
        nowView.setBackgroundResource(R.drawable.draw_selected_date);

        int len = day_string.size();
        if (len > pos) {
//            if (day_string.get(pos).equals(curentDateString)) {
//            } else {
            previousView = view;
//            }
        }

        return view;
    }

    public void refreshDays() {
        // clear items
        items.clear();
        day_string.clear();
        Locale.setDefault(Locale.US);
        pmonth = (GregorianCalendar) month.clone();
        // month start day. ie; sun, mon, etc
        firstDay = month.get(GregorianCalendar.DAY_OF_WEEK);
        // finding number of weeks in current month.
        maxWeeknumber = month.getActualMaximum(GregorianCalendar.WEEK_OF_MONTH);
        // allocating maximum row number for the gridview.
        mnthlength = maxWeeknumber * 7;
        maxP = getMaxP(); // previous month maximum day 31,30....
        calMaxP = maxP - (firstDay - 1);// calendar offday starting 24,25 ...
        /**
         * Calendar instance for getting a complete gridview including the three
         * month's (previous,current,next) dates.
         */
        pmonthmaxset = (GregorianCalendar) pmonth.clone();
        /**
         * setting the start date as previous month's required date.
         */
        pmonthmaxset.set(GregorianCalendar.DAY_OF_MONTH, calMaxP + 1);

        /**
         * filling calendar gridview.
         */
        for (int n = 0; n < mnthlength; n++) {

            itemvalue = df.format(pmonthmaxset.getTime());
            pmonthmaxset.add(GregorianCalendar.DATE, 1);
            day_string.add(itemvalue);

        }
    }

    private int getMaxP() {
        int maxP;
        if (month.get(GregorianCalendar.MONTH) == month
                .getActualMinimum(GregorianCalendar.MONTH)) {
            pmonth.set((month.get(GregorianCalendar.YEAR) - 1),
                    month.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            pmonth.set(GregorianCalendar.MONTH,
                    month.get(GregorianCalendar.MONTH) - 1);
        }
        maxP = pmonth.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);

        return maxP;
    }


    public void setEventView(View v, int pos, TextView txt, ImageView img) {

        int len = date_collection_arr.size();
        for (int i = 0; i < len; i++) {
            CalendarCollection cal_obj = date_collection_arr.get(i);
            String date = cal_obj.date;
            int len1 = day_string.size();
            if (len1 > pos) {

                if (day_string.get(pos).equals(date)) {
                    img.setVisibility(View.VISIBLE);
                }
            }
        }


    }


    public void getPositionList(String date, View view, Context context, final Activity activity) {
        PlanningView planningView;
        planningView = new PlanningView(view, context, activity, date_collection_arr, calendarListener);

        int len = date_collection_arr.size();
        for (int i = 0; i < len; i++) {
            CalendarCollection cal_collection = date_collection_arr.get(i);
            String event_date = cal_collection.date;

            String event_message = cal_collection.event_message;
            if (date.equalsIgnoreCase(event_date)) {
                //planningView.setGrid(view, context, activity, CalendarCollection.date_collection_arr);
                planningView.AmbilPlanning(view, context, activity, date + " " + DateLibs.getTimeStamp(), date_collection_arr, 1);
                break;
            } else {
                //planningView.setGrid(view, context, activity, CalendarCollection.date_collection_arr);
                planningView.AmbilPlanning(view, context, activity, date + " " + DateLibs.getTimeStamp(), date_collection_arr , 1);
                break;

            }

        }

    }

}