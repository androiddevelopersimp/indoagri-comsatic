package com.indoagri.comsatic.widget.calendar.model;


public class CalendarCollection {
    public String date = "";
    public String event_message = "";
    public String duration = "";

    public CalendarCollection(String date, String event_message, String duration) {
        this.date = date;
        this.event_message = event_message;
        this.duration = duration;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEvent_message() {
        return event_message;
    }

    public void setEvent_message(String event_message) {
        this.event_message = event_message;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
