package com.indoagri.comsatic.SalesBilling;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.indoagri.comsatic.R;
import com.indoagri.comsatic.Retrofit.DB.DatabaseQuery;
import com.indoagri.comsatic.Retrofit.Model.GroupCompanyModel;
import com.indoagri.comsatic.Retrofit.SharePreference;

import java.util.ArrayList;
import java.util.List;

public class ListCompanyActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, TextWatcher {

    Bundle extras;
    String extrasString;
    Toolbar toolbar;
    TextView mTextToolbar,txtSetAll;
    ListView listView;
    EditText etSearch;
    DatabaseQuery query;
    SharePreference sharedPreferences;
    List<GroupCompanyModel> companyModelList;
    ListCompanyAdapter listCompanyAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_company);
        setUpToolbar();
        sharedPreferences = new SharePreference(ListCompanyActivity.this);
        query = new DatabaseQuery(ListCompanyActivity.this);
        listView=(ListView) findViewById(R.id.listView);
        etSearch = findViewById(R.id.et_search);
        txtSetAll = (findViewById(R.id.txt_setAll));
        txtSetAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code="";
                String description="";
                Intent intent=new Intent();
                intent.putExtra("CODE",code);
                intent.putExtra("DESC",description);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        etSearch.setVisibility(View.VISIBLE);
        extras = getIntent().getExtras();
        if (extras != null) {
            extrasString = extras.getString("Group");
            init();
            // and get whatever type user account id is
            //Toast.makeText(this, extrasString+" || "+new SharePreference(ListSubProduct.this).isFormProduct(), Toast.LENGTH_SHORT).show();
        }else{

        }

    }
    private void setUpToolbar() {
        toolbar = (findViewById(R.id.toolbar));
        mTextToolbar= (findViewById(R.id.txt_Toolbar));
        mTextToolbar.setText(getResources().getString(R.string.listcompany));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    void init(){
        companyModelList=new ArrayList<GroupCompanyModel>();
        etSearch.addTextChangedListener(this);
        listView.setOnItemClickListener(this);
        listCompanyAdapter = new ListCompanyAdapter(this, R.layout.item_listcontent,companyModelList);
        listView.setAdapter(listCompanyAdapter);
        listView.setScrollingCacheEnabled(false);
        setHalaman();

    }

    private void setHalaman(){
        etSearch.addTextChangedListener(this);
        List<Object> listObject;
        query.openTransaction();
        String sqldb_query = "select groupCompany,companyCode,description from T_GROUPCOMPANY where groupCompany=?";
        String isAD = sharedPreferences.isFormGroupCompany();
        String[] a = new String[1];
        a[0] = isAD;
        listObject = query.getListDataRawQuery(sqldb_query,GroupCompanyModel.TABLE_NAME,a);
        query.closeTransaction();
        if(listObject.size() > 0){
            for(int i = 0; i < listObject.size(); i++){
                GroupCompanyModel hasil = (GroupCompanyModel)listObject.get(i);
                companyModelList.add(hasil);
            }

        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        listCompanyAdapter.getFilter().filter(s);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
        GroupCompanyModel companyModel= (GroupCompanyModel) listCompanyAdapter.getItem(i);
        String code=companyModel.getCompanycode();
        String description=companyModel.getDescription();
        Intent intent=new Intent();
        intent.putExtra("CODE",code);
        intent.putExtra("DESC",description);
        setResult(RESULT_OK, intent);
        finish();
    }
}
