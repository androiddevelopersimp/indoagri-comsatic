package com.indoagri.comsatic.SalesBilling;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.indoagri.comsatic.Common.DateLocal;
import com.indoagri.comsatic.Fragments.FormSearching;
import com.indoagri.comsatic.R;
import com.indoagri.comsatic.Retrofit.ApiServices;
import com.indoagri.comsatic.Retrofit.DB.DatabaseQuery;
import com.indoagri.comsatic.Retrofit.GroupCompanyResponse;
import com.indoagri.comsatic.Retrofit.GroupProductResponse;
import com.indoagri.comsatic.Retrofit.Model.GroupCompanyModel;
import com.indoagri.comsatic.Retrofit.Model.GroupProductModel;
import com.indoagri.comsatic.Retrofit.Model.SalesDataModel;
import com.indoagri.comsatic.Retrofit.NetClient;
import com.indoagri.comsatic.Retrofit.SalesDataResponse;
import com.indoagri.comsatic.Retrofit.SharePreference;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SalesBilling extends AppCompatActivity implements FormSearching.OnFragmentInteractionListener,FormSearching.OnSubProductListener,FormSearching.OnCompanyListener {
    private static final String TAGCOMPANY = "GET COMPANY GROUP";
    ApiServices apiServices;
    GroupCompanyResponse groupCompanyResponse;
    List<GroupCompanyModel>groupCompanyModelList;
    GroupProductResponse groupProductResponse;
    List<GroupProductModel>groupProductModelList;
    SalesDataResponse salesDataResponse;
    List<SalesDataModel>salesDataModelList;
    Toolbar toolbar;
    TextView mTextToolbar;
    EditText et_year;
    int year = Calendar.getInstance().get(Calendar.YEAR);
    DatabaseQuery query;
    com.indoagri.comsatic.Retrofit.Model.SalesBilling salesBilling;
    String dataYear,dataCompany,dataProduct,dataTipe;
    TableLayout tableLayout,tableLayoutfooter;
    ProgressBar mProgressbar;
    Button btnSearch;
    public static int SUBPRODUCTSLIST = 11;
    public static int COMPANYLIST = 12;
    private RadioGroup radioGroupNb;
    FrameLayout frame_display,searchFragment;
    CardView layarView;
    Date today;
    String dateToStr;
    ImageView imgSettings;
    ProgressBar progressBar;
    TextView uptoDate;
    ImageView imgSearch;
    boolean ShowSearch = false;
    ProgressDialog pDialog;
    int selectedId = 0;
    public FragmentRefreshListener getFragmentRefreshListener() {
        return fragmentRefreshListener;
    }

    public void setFragmentRefreshListener(FragmentRefreshListener fragmentRefreshListener) {
        this.fragmentRefreshListener = fragmentRefreshListener;
    }

    private FragmentRefreshListener fragmentRefreshListener;
    @Override
    protected void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing_sales);
        et_year = (findViewById(R.id.et_year));
        apiServices = NetClient.DataProduction().create(ApiServices.class);
        et_year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            showYearDialog();
            }
        });
        radioGroupNb = (findViewById(R.id.rg));
        radioGroupNb.setVisibility(View.GONE);
        frame_display = (findViewById(R.id.frame_view_display));
        searchFragment = (findViewById(R.id.fragment_container));
        progressBar  = (findViewById(R.id.progress_bar));
        progressBar.setVisibility(View.GONE);
        query = new DatabaseQuery(SalesBilling.this);
        setUpToolbar();
        imgSettings = (findViewById(R.id.settings));
        imgSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(deletetable()){
          //          Toast.makeText(SalesBilling.this, "Delete Berhasil", Toast.LENGTH_SHORT).show();
                }
            }
        });
        imgSearch = (findViewById(R.id.imgSearch));
        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ShowSearch){
                    onBackPressed();
                }else{
                    toSearch();
                }
            }
        });
        uptoDate = (findViewById(R.id.txtUpToDate));
        uptoDate.setText(new SharePreference(SalesBilling.this).isUpdateSyncSales());
        tableLayout = (TableLayout) findViewById(R.id.main_table);
        tableLayoutfooter = (TableLayout) findViewById(R.id.main_tableFooter);
        tableLayout.bringToFront();
        tableLayout.setStretchAllColumns(true);
        mProgressbar = (findViewById(R.id.progress_bar));
        pDialog = new ProgressDialog(SalesBilling.this);
        btnSearch = (findViewById(R.id.btn_search));
        layarView = (findViewById(R.id.layarVIew));
        layarView.setVisibility(View.GONE);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et_year.getText().toString().length()<1){
                    Toast.makeText(SalesBilling.this,getResources().getString(R.string.dataempty),Toast.LENGTH_SHORT).show();
                }else{
                    // DataSourceContract();
                    toSearch();
                }
            }
        });
        dataTipe = "Contract";
        radioGroupNb.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radiocontract:
                        // do operations specific to this selection

                        dataTipe = "Contract";
                        selectedId=0;
                        DataSourceContract();
                      //  Toast.makeText(SalesBilling.this,dataTipe,Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioBilling :
                        dataTipe = "Billing";
                        selectedId=1;
                        DataSourceBilling();
                        // do operations specific to this selection
                      //  Toast.makeText(SalesBilling.this,dataTipe,Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
        today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        dateToStr = format.format(today);
        toSearch();
    }

    private void setUpToolbar() {
        toolbar = (findViewById(R.id.toolbar));
        mTextToolbar= (findViewById(R.id.title_toolbar));
        mTextToolbar.setText(getResources().getString(R.string.salesbill));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    public void showYearDialog()
    {

        final Dialog d = new Dialog(SalesBilling.this);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.dialog_year);
        Button set = (Button) d.findViewById(R.id.btnOk);
        Button cancel = (Button) d.findViewById(R.id.btnCancel);
        TextView year_text=(TextView)d.findViewById(R.id.year_text);
        year_text.setText("Year");
        final NumberPicker nopicker = (NumberPicker) d.findViewById(R.id.picker_year);

        nopicker.setMaxValue(year+50);
        nopicker.setMinValue(year-50);
        nopicker.setWrapSelectorWheel(false);
        nopicker.setValue(year);
        nopicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        set.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                et_year.setText(String.valueOf(nopicker.getValue()));
                new SharePreference(SalesBilling.this).setFormYear(String.valueOf(nopicker.getValue()));
                dataYear=String.valueOf(nopicker.getValue());
                d.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }


    private void DataSourceContract(){
            query.openTransaction();
            List<Object> listObject;
            List<com.indoagri.comsatic.Retrofit.Model.SalesBilling> listTemp = new ArrayList<com.indoagri.comsatic.Retrofit.Model.SalesBilling>();
            listTemp.clear();
            String queryString = "SELECT Company,Product,Year,Periode,Tipe,sum(Qty) as Qty,sum(Value) as Value, (sum(value) / sum(qty))as price from T_SalesBilling where Tipe!='BILL' group by Periode order by Periode asc";
            listObject = query.getListDataRawQuery(queryString, com.indoagri.comsatic.Retrofit.Model.SalesBilling.TABLE_NAME,null);
            query.closeTransaction();
            if(listObject==null){
           //     Toast.makeText(SalesBilling.this,  "Data Tidak Ada", Toast.LENGTH_LONG).show();
                tableLayout.removeAllViews();
            }
            else{
                if(listObject.size() > 0){
            for(int i = 0; i < listObject.size(); i++){
                salesBilling= (com.indoagri.comsatic.Retrofit.Model.SalesBilling) listObject.get(i);
                listTemp.add(salesBilling);
            }
                    startLoadData();
                    AsyncDetail asyncBersih = new AsyncDetail(listObject);
                    asyncBersih.execute();
                }else{
                    tableLayout.removeAllViews();
                  //  Toast.makeText(SalesBilling.this,  "Data Tidak Ada", Toast.LENGTH_LONG).show();
                }
            }
    }

    private void DataSourceBilling(){
        query.openTransaction();
        List<Object> listObject;
        List<com.indoagri.comsatic.Retrofit.Model.SalesBilling> listTemp = new ArrayList<com.indoagri.comsatic.Retrofit.Model.SalesBilling>();
        listTemp.clear();
        int TotalRows = 12;
        int rowsObject= 0;
        String queryString = "SELECT Company,Product,Year,Periode,Tipe,sum(Qty) as Qty,sum(Value) as Value, sum(Price) as Price from T_SalesBilling where Tipe=='BILL' group by Periode order by Periode asc";
        listObject = query.getListDataRawQuery(queryString, com.indoagri.comsatic.Retrofit.Model.SalesBilling.TABLE_NAME,null);
        query.closeTransaction();
        if(listObject==null){
            Toast.makeText(SalesBilling.this,  getResources().getString(R.string.dataempty), Toast.LENGTH_LONG).show();
            tableLayout.removeAllViews();
        }
        else{
            if(listObject.size() > 0){
                String monthPeriode = null;
               // rowsObject = listObject.size();
                for(int i = 0; i < listObject.size(); i++){
                        salesBilling= (com.indoagri.comsatic.Retrofit.Model.SalesBilling) listObject.get(i);
                        listTemp.add(salesBilling);
                }
                startLoadData();
                AsyncDetail asyncBersih = new AsyncDetail(listObject);
                asyncBersih.execute();
            }else{
                tableLayout.removeAllViews();
                Toast.makeText(SalesBilling.this,  getResources().getString(R.string.dataempty), Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public void onFragmentInteraction(String year) {
   //     Toast.makeText(SalesBilling.this,year, Toast.LENGTH_SHORT).show();
        frame_display.setVisibility(View.VISIBLE);
        searchFragment.setVisibility(View.GONE);
        if(new SharePreference(SalesBilling.this).isFormCompany().equalsIgnoreCase("") &&
                new SharePreference(SalesBilling.this).isFormProduct().equalsIgnoreCase("")){
            CARIDATA();
        }if(!new SharePreference(SalesBilling.this).isFormCompany().equalsIgnoreCase("") &&
                new SharePreference(SalesBilling.this).isFormProduct().equalsIgnoreCase("")){
            CARIDATACOMPANY();
        }if(new SharePreference(SalesBilling.this).isFormCompany().equalsIgnoreCase("") &&
                !new SharePreference(SalesBilling.this).isFormProduct().equalsIgnoreCase("")){
            CARIDATAPRODUCT();
        }

        if(!new SharePreference(SalesBilling.this).isFormCompany().equalsIgnoreCase("") &&
                !new SharePreference(SalesBilling.this).isFormProduct().equalsIgnoreCase("")){
            CARIDATACOMPANYPRODUCT();
        }
    }

    private void CARIDATA(){
        query.openTransaction();
        List<com.indoagri.comsatic.Retrofit.Model.SalesBilling> listTemp2 = new ArrayList<com.indoagri.comsatic.Retrofit.Model.SalesBilling>();
        List<Object> listObject2;
        String[] a = new String[2];
        a[0] = new SharePreference(SalesBilling.this).isFormGroupCompany();
        a[1] = new SharePreference(SalesBilling.this).isFormYear();
        String sqldb_query = "SELECT " +
                "a.vkorg as Company, " +
                "a.matnr as product, " +
                "a.gjahr as year, " +
                "a.monat as periode, " +
                "a.source as Tipe, " +
                "sum(a.kwmeng) as qty, "+
                "sum(a.netwr) as value, "+
                "(sum(a.netwr) / sum(a.kwmeng)) as price "+
                "from T_SalesData a " +
                "WHERE vkorg  IN (Select companyCode as vkorg " +
                "from T_GROUPCOMPANY where groupCompany = ?) " +
                "AND gjahr=? "+
                "group by Tipe,Periode";
        listObject2 = query.getListDataRawQuery(sqldb_query, com.indoagri.comsatic.Retrofit.Model.SalesBilling.TABLE_NAME,a);
        query.closeTransaction();
        if(listObject2.size() > 0){
            for(int i = 0; i < listObject2.size(); i++){
                com.indoagri.comsatic.Retrofit.Model.SalesBilling blk = (com.indoagri.comsatic.Retrofit.Model.SalesBilling) listObject2.get(i);
                listTemp2.add(blk);
               // Toast.makeText(SalesBilling.this,blk.getProduct(),Toast.LENGTH_SHORT).show();
              HapusTransaksi(listObject2);
            }
            if(selectedId==0){
                DataSourceContract();
            }else{
                DataSourceBilling();
            }
        }
        else{
            listObject2.clear();
            HapusTransaksi(listObject2);
            Toast.makeText(SalesBilling.this,getResources().getString(R.string.dataempty),Toast.LENGTH_SHORT).show();
        }
    }

    private void CARIDATACOMPANY(){
        query.openTransaction();
        List<com.indoagri.comsatic.Retrofit.Model.SalesBilling> listTemp2 = new ArrayList<com.indoagri.comsatic.Retrofit.Model.SalesBilling>();
        List<Object> listObject2;
        String[] a = new String[2];
        a[0] = new SharePreference(SalesBilling.this).isFormCompany();
        a[1] = new SharePreference(SalesBilling.this).isFormYear();
        ;
        String sqldb_query = "SELECT " +
                "a.vkorg as Company, " +
                "a.matnr as product, " +
                "a.gjahr as year, " +
                "a.monat as periode , " +
                "a.source as Tipe, " +
                "sum(a.kwmeng) as qty, "+
                "sum(a.netwr) as value, "+
                "(sum(a.netwr) / sum(a.kwmeng)) as price "+
                "from T_SalesData a " +
                "WHERE vkorg  IN (Select companyCode as vkorg " +
                "from T_GROUPCOMPANY where companyCode = ?) " +
                "AND gjahr=? "+
                "group by Tipe,monat";
        listObject2 = query.getListDataRawQuery(sqldb_query, com.indoagri.comsatic.Retrofit.Model.SalesBilling.TABLE_NAME,a);
        query.closeTransaction();
        if(listObject2.size() > 0){
            for(int i = 0; i < listObject2.size(); i++){
                com.indoagri.comsatic.Retrofit.Model.SalesBilling blk = (com.indoagri.comsatic.Retrofit.Model.SalesBilling) listObject2.get(i);
                listTemp2.add(blk);
                // Toast.makeText(SalesBilling.this,blk.getProduct(),Toast.LENGTH_SHORT).show();
                HapusTransaksi(listObject2);
                if(selectedId==0){
                    DataSourceContract();
                }else{
                    DataSourceBilling();
                }
            }
        }
        else{
            listObject2.clear();
            HapusTransaksi(listObject2);
            Toast.makeText(SalesBilling.this,getResources().getString(R.string.dataempty),Toast.LENGTH_SHORT).show();
        }
    }

    private void CARIDATAPRODUCT(){
        query.openTransaction();
        List<com.indoagri.comsatic.Retrofit.Model.SalesBilling> listTemp2 = new ArrayList<com.indoagri.comsatic.Retrofit.Model.SalesBilling>();
        List<Object> listObject2;
        String[] a = new String[3];
        a[0] = new SharePreference(SalesBilling.this).isFormYear();
        a[1] = new SharePreference(SalesBilling.this).isFormProduct();
        a[2] = new SharePreference(SalesBilling.this).isFormSubProduct();
        ;
        String sqldb_query = "SELECT " +
                "a.vkorg as Company, " +
                "a.matnr as product, " +
                "a.gjahr as year, " +
                "a.monat as periode , " +
                "a.source as Tipe, " +
                "sum(a.kwmeng) as qty, "+
                "sum(a.netwr) as value, "+
                "(sum(a.netwr) / sum(a.kwmeng)) as price "+
                "from T_SalesData a " +
                "WHERE gjahr=? "+
                "AND (matnr IN (select material as matnr from T_GROUPPRODUCT where groupProduct= ? ) OR " +
                "matnr IN (select material as matnr from T_GROUPPRODUCT where subgroup= ? ) " +
                ")" +
                "group by Tipe,monat";
        listObject2 = query.getListDataRawQuery(sqldb_query, com.indoagri.comsatic.Retrofit.Model.SalesBilling.TABLE_NAME,a);
        query.closeTransaction();
        if(listObject2.size() > 0){
            for(int i = 0; i < listObject2.size(); i++){
                com.indoagri.comsatic.Retrofit.Model.SalesBilling blk = (com.indoagri.comsatic.Retrofit.Model.SalesBilling) listObject2.get(i);
                listTemp2.add(blk);
                // Toast.makeText(SalesBilling.this,blk.getProduct(),Toast.LENGTH_SHORT).show();
                HapusTransaksi(listObject2);
                if(selectedId==0){
                    DataSourceContract();
                }else{
                    DataSourceBilling();
                }
            }
        }
        else{
            listObject2.clear();
            HapusTransaksi(listObject2);
            Toast.makeText(SalesBilling.this,getResources().getString(R.string.dataempty),Toast.LENGTH_SHORT).show();
        }
    }


    private void CARIDATACOMPANYPRODUCT(){
        query.openTransaction();
        List<com.indoagri.comsatic.Retrofit.Model.SalesBilling> listTemp2 = new ArrayList<com.indoagri.comsatic.Retrofit.Model.SalesBilling>();
        List<Object> listObject2;
        String[] a = new String[4];
        a[0] = new SharePreference(SalesBilling.this).isFormCompany();
        a[1] = new SharePreference(SalesBilling.this).isFormYear();
        a[2] = new SharePreference(SalesBilling.this).isFormProduct();
        a[3] = new SharePreference(SalesBilling.this).isFormSubProduct();
        ;
        String sqldb_query = "SELECT " +
                "a.vkorg as Company, " +
                "a.matnr as product, " +
                "a.gjahr as year, " +
                "a.monat as periode , " +
                "a.source as Tipe, " +
                "sum(a.kwmeng) as qty, "+
                "sum(a.netwr) as value, "+
                "(sum(a.netwr) / sum(a.kwmeng)) as price "+
                "from T_SalesData a " +
                "WHERE vkorg  IN (Select companyCode as vkorg " +
                "from T_GROUPCOMPANY where companyCode = ?) " +
                "AND gjahr=? "+
                "AND (matnr IN (select material as matnr from T_GROUPPRODUCT where groupProduct=?) OR " +
                "matnr IN (select material as matnr from T_GROUPPRODUCT where subgroup=?) " +
                ")" +
                "group by Tipe,Periode";
        listObject2 = query.getListDataRawQuery(sqldb_query, com.indoagri.comsatic.Retrofit.Model.SalesBilling.TABLE_NAME,a);
        query.closeTransaction();
        if(listObject2.size() > 0){
            for(int i = 0; i < listObject2.size(); i++){
                com.indoagri.comsatic.Retrofit.Model.SalesBilling blk = (com.indoagri.comsatic.Retrofit.Model.SalesBilling) listObject2.get(i);
                listTemp2.add(blk);
                // Toast.makeText(SalesBilling.this,blk.getProduct(),Toast.LENGTH_SHORT).show();
                HapusTransaksi(listObject2);
                if(selectedId==0){
                    DataSourceContract();
                }else{
                    DataSourceBilling();
                }
            }
        }
        else{
            listObject2.clear();
            HapusTransaksi(listObject2);
            Toast.makeText(SalesBilling.this,getResources().getString(R.string.dataempty),Toast.LENGTH_SHORT).show();
        }
    }


    private void HapusTransaksi(List<Object> list){
        query.openTransaction();
        query.deleteDataTemporary(com.indoagri.comsatic.Retrofit.Model.SalesBilling.TABLE_NAME,null,null);
        query.deleteDataTable(com.indoagri.comsatic.Retrofit.Model.SalesBilling.TABLE_NAME,null,null);
        query.commitTransaction();
        query.closeTransaction();
        SetDataSearch(list);
        radioGroupNb.setVisibility(View.VISIBLE);
        layarView.setVisibility(View.VISIBLE);
        TextView textGroup = (findViewById(R.id.groupCompany));
        TextView textCompany = (findViewById(R.id.company));
        TextView textProduct = (findViewById(R.id.product));
        TextView txtInfoYear = (findViewById(R.id.yearInfo));
        textGroup.setText("Group Company :: "+new SharePreference(SalesBilling.this).isFormGroupCompany());
        if(new SharePreference(SalesBilling.this).isFormCompany().equalsIgnoreCase("")){
            textCompany.setText("Company :: "+" ALL");
        }
        if(!new SharePreference(SalesBilling.this).isFormCompany().equalsIgnoreCase("")){
            textCompany.setText("Company :: "+new SharePreference(SalesBilling.this).isFormCompany()+" - "+new SharePreference(SalesBilling.this).isFormCompanyDesc());
        }

        if(new SharePreference(SalesBilling.this).isFormProduct().equalsIgnoreCase("")){
            textProduct.setText("Product :: "+" ALL");
        }
        if(!new SharePreference(SalesBilling.this).isFormProduct().equalsIgnoreCase("")){
            textProduct.setText("Product :: "+new SharePreference(SalesBilling.this).isFormProduct());
        }
        if(new SharePreference(SalesBilling.this).isFormYear().equalsIgnoreCase("")){
            txtInfoYear.setText("Year :: "+" - ");
        }
        if(!new SharePreference(SalesBilling.this).isFormYear().equalsIgnoreCase("")){
            txtInfoYear.setText("Year :: "+new SharePreference(SalesBilling.this).isFormYear());
        }


    }

    private void SetDataSearch(List<Object> list){
        query.openTransaction();
          for(int i=0;i<list.size();i++){
            com.indoagri.comsatic.Retrofit.Model.SalesBilling salesBilling = (com.indoagri.comsatic.Retrofit.Model.SalesBilling) list.get(i);

            query.setData(new com.indoagri.comsatic.Retrofit.Model.SalesBilling(0,salesBilling.getCompany(),
                    salesBilling.getProduct(),
                    salesBilling.getYear(),
                    salesBilling.getPeriode(),
                    salesBilling.getTipe(),
                    salesBilling.getQty(),
                    salesBilling.getValue(),
                    salesBilling.getPrice()));
        }
        query.commitTransaction();
        query.closeTransaction();
        DataSourceContract();
    }

    @Override
    public void onFragmentInteractionSubProducts(String Group) {
        Intent newIntent = new Intent(SalesBilling.this,ListSubProduct.class);
        newIntent.putExtra("Group",Group);
        startActivityForResult(newIntent, SUBPRODUCTSLIST);
    }

    @Override
    public void onFragmentInteractionCompany(String Group) {
        Intent newIntent = new Intent(SalesBilling.this,ListCompanyActivity.class);
        newIntent.putExtra("Group",Group);
        startActivityForResult(newIntent, COMPANYLIST);
    }

    private class AsyncDetail extends AsyncTask<Void, Integer, Void> {
        List<Object> objectList;
        com.indoagri.comsatic.Retrofit.Model.SalesBilling salesBilling;
        public AsyncDetail(List<Object> objects) {

            objectList = objects;
        }
        @Override
        protected void onPreExecute() {

            //mProgressbar.setVisibility(View.VISIBLE);
            boolean iShowing = pDialog.isShowing();
            if(!iShowing){
                pDialog.setMessage("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            pDialog.setProgress(values[0]);

        }

        @Override
        protected void onPostExecute(Void result) {

            loadData(objectList,salesBilling);
          pDialog.dismiss();
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (int i = 0; i<objectList.size(); i++) {
                publishProgress(i);
                salesBilling = (com.indoagri.comsatic.Retrofit.Model.SalesBilling) objectList.get(i);
            }

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }
    }


    public void startLoadData() {
        boolean iShowing = pDialog.isShowing();
        if(!iShowing){
            pDialog.setMessage("Loading");
            pDialog.setCancelable(false);
            pDialog.show();
        }
//    mProgressbar.setVisibility(View.VISIBLE);

      //  new LoadDataTask().execute(0);

    }

    public void loadData(List<Object> objectList, com.indoagri.comsatic.Retrofit.Model.SalesBilling salesBilling) {

        int leftRowMargin=0;
        int topRowMargin=0;
        int rightRowMargin=0;
        int bottomRowMargin = 0;
        int textSize = 0, smallTextSize =0, mediumTextSize = 0;

        textSize = (int) getResources().getDimension(R.dimen.font_size_medium);
        smallTextSize = (int) getResources().getDimension(R.dimen.font_size_normal);
        mediumTextSize = (int) getResources().getDimension(R.dimen.font_size_medium);

        int totalrows = 12;
        int rows = objectList.size();
        int rowsIden = 0;
        String periodeBulangIden = null;


        getSupportActionBar().setTitle("Invoices (" + String.valueOf(rows) + ")");
        TextView textSpacer = null;

        tableLayout.removeAllViews();

        NumberFormat QtyFormat = new DecimalFormat("#,###,###,###,###,###");
        double  totalQty = 0 ;
        String formatQtyTotal = null;
        double  totalValue = 0 ;
        String formatValueTotal = null;
        double  totalPrice = 0 ;
        String formatPriceTotal = null;
        String periodeBulan = null;
        String textPeriode = null;
        NumberFormat formatterValue;
        String formatQty = null;
        double  myQty;
        double myNumbervalue;
        String formatValue = null;
        double myNumber;
        String formattedNumber = null;
        boolean bln1 = false;
        boolean bln2= false;
        boolean bln3= false;
        boolean bln4= false;
        boolean bln5= false;
        boolean bln6= false;
        boolean bln7= false;
        boolean bln8= false;
        boolean bln9= false;
        boolean bln10= false;
        boolean bln11= false;
        boolean bln12= false;
        // -1 means heading row
        for (rowsIden= -1; rowsIden < totalrows; rowsIden++) {
            if (rowsIden > -1) {
                if(rowsIden==0){
                    periodeBulangIden = "01";
                    for (rows= 0; rows < objectList.size(); rows++) {
                        salesBilling = (com.indoagri.comsatic.Retrofit.Model.SalesBilling) objectList.get(rows);
                        if(salesBilling.getPeriode().equalsIgnoreCase(periodeBulangIden)){
                            formatterValue = new DecimalFormat("#,###,###,###,###,###");
                            myQty = Double.parseDouble(salesBilling.getQty())/1000;
                            formatQty = formatterValue.format(myQty);
                            myNumbervalue= Double.parseDouble(salesBilling.getValue())/1000000;
                            formatValue = formatterValue.format(myNumbervalue);
                            myNumber= (myNumbervalue/myQty)*1000;
                            formattedNumber = formatterValue.format(myNumber);
                            totalQty += myQty;
                            formatQtyTotal = QtyFormat.format(totalQty);
                            totalValue+= myNumbervalue;
                            formatValueTotal= QtyFormat.format(totalValue);
                            totalPrice = (totalValue/totalQty)*1000;
                            formatPriceTotal= QtyFormat.format(totalPrice);
                            DateLocal dateLocal = new DateLocal();
                            periodeBulan = dateLocal.getMonthAlphabet(periodeBulangIden);
                            bln1 = true;
                        }else{
                            if(bln1){
                            }else{
                                formatterValue = new DecimalFormat("#,###,###,###,###,###");
                                myQty = 0/1000;
                                formatQty = formatterValue.format(myQty);
                                myNumbervalue= 0/1000000;
                                formatValue = formatterValue.format(myNumbervalue);
                                myNumber= 0/1000000;
                                formattedNumber = formatterValue.format(myNumber);
                                DateLocal dateLocal = new DateLocal();
                                periodeBulan = dateLocal.getMonthAlphabet(periodeBulangIden);
                            }
                        }
                    }
                }
                if(rowsIden==1){
                    periodeBulangIden = "02";
                    for (rows= 0; rows < objectList.size(); rows++) {
                        salesBilling = (com.indoagri.comsatic.Retrofit.Model.SalesBilling) objectList.get(rows);
                        if(salesBilling.getPeriode().equalsIgnoreCase(periodeBulangIden)){
                            formatterValue = new DecimalFormat("#,###,###,###,###,###");
                            myQty = Double.parseDouble(salesBilling.getQty())/1000;
                            formatQty = formatterValue.format(myQty);
                            myNumbervalue= Double.parseDouble(salesBilling.getValue())/1000000;
                            formatValue = formatterValue.format(myNumbervalue);
                            myNumber= (myNumbervalue/myQty)*1000;
                            formattedNumber = formatterValue.format(myNumber);
                            totalQty += myQty;
                            formatQtyTotal = QtyFormat.format(totalQty);
                            totalValue+= myNumbervalue;
                            formatValueTotal= QtyFormat.format(totalValue);
                            totalPrice = (totalValue/totalQty)*1000;
                            formatPriceTotal= QtyFormat.format(totalPrice);
                            DateLocal dateLocal = new DateLocal();
                            periodeBulan = dateLocal.getMonthAlphabet(periodeBulangIden);
                            bln2 = true;
                        }else{
                            if(bln2){
                            }else{
                                formatterValue = new DecimalFormat("#,###,###,###,###,###");
                                myQty = 0/1000;
                                formatQty = formatterValue.format(myQty);
                                myNumbervalue= 0/1000000;
                                formatValue = formatterValue.format(myNumbervalue);
                                myNumber= 0/1000000;
                                formattedNumber = formatterValue.format(myNumber);
                                DateLocal dateLocal = new DateLocal();
                                periodeBulan = dateLocal.getMonthAlphabet(periodeBulangIden);
                            }
                        }
                    }

                }
                if(rowsIden==2){
                    periodeBulangIden = "03";
                    for (rows= 0; rows < objectList.size(); rows++) {
                        salesBilling = (com.indoagri.comsatic.Retrofit.Model.SalesBilling) objectList.get(rows);
                        if(salesBilling.getPeriode().equalsIgnoreCase(periodeBulangIden)){
                            formatterValue = new DecimalFormat("#,###,###,###,###,###");
                            myQty = Double.parseDouble(salesBilling.getQty())/1000;
                            formatQty = formatterValue.format(myQty);
                            myNumbervalue= Double.parseDouble(salesBilling.getValue())/1000000;
                            formatValue = formatterValue.format(myNumbervalue);
                            myNumber= (myNumbervalue/myQty)*1000;
                            formattedNumber = formatterValue.format(myNumber);
                            totalQty += myQty;
                            formatQtyTotal = QtyFormat.format(totalQty);
                            totalValue+= myNumbervalue;
                            formatValueTotal= QtyFormat.format(totalValue);
                            totalPrice = (totalValue/totalQty)*1000;
                            formatPriceTotal= QtyFormat.format(totalPrice);
                            DateLocal dateLocal = new DateLocal();
                            periodeBulan = dateLocal.getMonthAlphabet(periodeBulangIden);
                            bln3 = true;
                        }else{
                            if(bln3){
                            }else{
                                formatterValue = new DecimalFormat("#,###,###,###,###,###");
                                myQty = 0/1000;
                                formatQty = formatterValue.format(myQty);
                                myNumbervalue= 0/1000000;
                                formatValue = formatterValue.format(myNumbervalue);
                                myNumber= 0/1000000;
                                formattedNumber = formatterValue.format(myNumber);
                                DateLocal dateLocal = new DateLocal();
                                periodeBulan = dateLocal.getMonthAlphabet(periodeBulangIden);
                            }
                        }
                    }
                }
                if(rowsIden==3){
                    periodeBulangIden = "04";
                    for (rows= 0; rows < objectList.size(); rows++) {
                        salesBilling = (com.indoagri.comsatic.Retrofit.Model.SalesBilling) objectList.get(rows);
                        if(salesBilling.getPeriode().equalsIgnoreCase(periodeBulangIden)){
                            formatterValue = new DecimalFormat("#,###,###,###,###,###");
                            myQty = Double.parseDouble(salesBilling.getQty())/1000;
                            formatQty = formatterValue.format(myQty);
                            myNumbervalue= Double.parseDouble(salesBilling.getValue())/1000000;
                            formatValue = formatterValue.format(myNumbervalue);
                            myNumber= (myNumbervalue/myQty)*1000;
                            formattedNumber = formatterValue.format(myNumber);
                            totalQty += myQty;
                            formatQtyTotal = QtyFormat.format(totalQty);
                            totalValue+= myNumbervalue;
                            formatValueTotal= QtyFormat.format(totalValue);
                            totalPrice = (totalValue/totalQty)*1000;
                            formatPriceTotal= QtyFormat.format(totalPrice);
                            DateLocal dateLocal = new DateLocal();
                            periodeBulan = dateLocal.getMonthAlphabet(periodeBulangIden);
                            bln4 = true;
                        }else{
                            if(bln4){
                            }else{
                                formatterValue = new DecimalFormat("#,###,###,###,###,###");
                                myQty = 0/1000;
                                formatQty = formatterValue.format(myQty);
                                myNumbervalue= 0/1000000;
                                formatValue = formatterValue.format(myNumbervalue);
                                myNumber= 0/1000000;
                                formattedNumber = formatterValue.format(myNumber);
                                DateLocal dateLocal = new DateLocal();
                                periodeBulan = dateLocal.getMonthAlphabet(periodeBulangIden);
                            }
                        }
                    }
                }
                if(rowsIden==4){
                    periodeBulangIden = "05";
                    for (rows= 0; rows < objectList.size(); rows++) {
                        salesBilling = (com.indoagri.comsatic.Retrofit.Model.SalesBilling) objectList.get(rows);
                        if(salesBilling.getPeriode().equalsIgnoreCase(periodeBulangIden)){
                            formatterValue = new DecimalFormat("#,###,###,###,###,###");
                            myQty = Double.parseDouble(salesBilling.getQty())/1000;
                            formatQty = formatterValue.format(myQty);
                            myNumbervalue= Double.parseDouble(salesBilling.getValue())/1000000;
                            formatValue = formatterValue.format(myNumbervalue);
                            myNumber= (myNumbervalue/myQty)*1000;
                            formattedNumber = formatterValue.format(myNumber);
                            totalQty += myQty;
                            formatQtyTotal = QtyFormat.format(totalQty);
                            totalValue+= myNumbervalue;
                            formatValueTotal= QtyFormat.format(totalValue);
                            totalPrice = (totalValue/totalQty)*1000;
                            formatPriceTotal= QtyFormat.format(totalPrice);
                            DateLocal dateLocal = new DateLocal();
                            periodeBulan = dateLocal.getMonthAlphabet(periodeBulangIden);
                            bln5 = true;
                        }else{
                            if(bln5){
                            }else{
                                formatterValue = new DecimalFormat("#,###,###,###,###,###");
                                myQty = 0/1000;
                                formatQty = formatterValue.format(myQty);
                                myNumbervalue= 0/1000000;
                                formatValue = formatterValue.format(myNumbervalue);
                                myNumber= 0/1000000;
                                formattedNumber = formatterValue.format(myNumber);
                                DateLocal dateLocal = new DateLocal();
                                periodeBulan = dateLocal.getMonthAlphabet(periodeBulangIden);
                            }
                        }
                    }
                }
                if(rowsIden==5){
                    periodeBulangIden = "06";
                    for (rows= 0; rows < objectList.size(); rows++) {
                        salesBilling = (com.indoagri.comsatic.Retrofit.Model.SalesBilling) objectList.get(rows);
                        if(salesBilling.getPeriode().equalsIgnoreCase(periodeBulangIden)){
                            formatterValue = new DecimalFormat("#,###,###,###,###,###");
                            myQty = Double.parseDouble(salesBilling.getQty())/1000;
                            formatQty = formatterValue.format(myQty);
                            myNumbervalue= Double.parseDouble(salesBilling.getValue())/1000000;
                            formatValue = formatterValue.format(myNumbervalue);
                            myNumber= (myNumbervalue/myQty)*1000;
                            formattedNumber = formatterValue.format(myNumber);
                            totalQty += myQty;
                            formatQtyTotal = QtyFormat.format(totalQty);
                            totalValue+= myNumbervalue;
                            formatValueTotal= QtyFormat.format(totalValue);
                            totalPrice = (totalValue/totalQty)*1000;
                            formatPriceTotal= QtyFormat.format(totalPrice);
                            DateLocal dateLocal = new DateLocal();
                            periodeBulan = dateLocal.getMonthAlphabet(periodeBulangIden);
                            bln6 = true;
                        }else{
                            if(bln6){
                            }else{
                                formatterValue = new DecimalFormat("#,###,###,###,###,###");
                                myQty = 0/1000;
                                formatQty = formatterValue.format(myQty);
                                myNumbervalue= 0/1000000;
                                formatValue = formatterValue.format(myNumbervalue);
                                myNumber= 0/1000000;
                                formattedNumber = formatterValue.format(myNumber);
                                DateLocal dateLocal = new DateLocal();
                                periodeBulan = dateLocal.getMonthAlphabet(periodeBulangIden);
                            }
                        }
                    }
                }
                if(rowsIden==6){
                    periodeBulangIden = "07";
                    for (rows= 0; rows < objectList.size(); rows++) {
                        salesBilling = (com.indoagri.comsatic.Retrofit.Model.SalesBilling) objectList.get(rows);
                        if(salesBilling.getPeriode().equalsIgnoreCase(periodeBulangIden)){
                            formatterValue = new DecimalFormat("#,###,###,###,###,###");
                            myQty = Double.parseDouble(salesBilling.getQty())/1000;
                            formatQty = formatterValue.format(myQty);
                            myNumbervalue= Double.parseDouble(salesBilling.getValue())/1000000;
                            formatValue = formatterValue.format(myNumbervalue);
                            myNumber= (myNumbervalue/myQty)*1000;
                            formattedNumber = formatterValue.format(myNumber);
                            totalQty += myQty;
                            formatQtyTotal = QtyFormat.format(totalQty);
                            totalValue+= myNumbervalue;
                            formatValueTotal= QtyFormat.format(totalValue);
                            totalPrice = (totalValue/totalQty)*1000;
                            formatPriceTotal= QtyFormat.format(totalPrice);
                            DateLocal dateLocal = new DateLocal();
                            periodeBulan = dateLocal.getMonthAlphabet(periodeBulangIden);
                            bln7 = true;
                        }else{
                            if(bln7){
                            }else{
                                formatterValue = new DecimalFormat("#,###,###,###,###,###");
                                myQty = 0/1000;
                                formatQty = formatterValue.format(myQty);
                                myNumbervalue= 0/1000000;
                                formatValue = formatterValue.format(myNumbervalue);
                                myNumber= 0/1000000;
                                formattedNumber = formatterValue.format(myNumber);
                                DateLocal dateLocal = new DateLocal();
                                periodeBulan = dateLocal.getMonthAlphabet(periodeBulangIden);
                            }
                        }
                    }
                }
                if(rowsIden==7){
                    periodeBulangIden = "08";
                    for (rows= 0; rows < objectList.size(); rows++) {
                        salesBilling = (com.indoagri.comsatic.Retrofit.Model.SalesBilling) objectList.get(rows);
                        if(salesBilling.getPeriode().equalsIgnoreCase(periodeBulangIden)){
                            formatterValue = new DecimalFormat("#,###,###,###,###,###");
                            myQty = Double.parseDouble(salesBilling.getQty())/1000;
                            formatQty = formatterValue.format(myQty);
                            myNumbervalue= Double.parseDouble(salesBilling.getValue())/1000000;
                            formatValue = formatterValue.format(myNumbervalue);
                            myNumber= (myNumbervalue/myQty)*1000;
                            formattedNumber = formatterValue.format(myNumber);
                            totalQty += myQty;
                            formatQtyTotal = QtyFormat.format(totalQty);
                            totalValue+= myNumbervalue;
                            formatValueTotal= QtyFormat.format(totalValue);
                            totalPrice = (totalValue/totalQty)*1000;
                            formatPriceTotal= QtyFormat.format(totalPrice);
                            DateLocal dateLocal = new DateLocal();
                            periodeBulan = dateLocal.getMonthAlphabet(periodeBulangIden);
                            bln8 = true;
                        }else{
                            if(bln8){
                            }else{
                                formatterValue = new DecimalFormat("#,###,###,###,###,###");
                                myQty = 0/1000;
                                formatQty = formatterValue.format(myQty);
                                myNumbervalue= 0/1000000;
                                formatValue = formatterValue.format(myNumbervalue);
                                myNumber= 0/1000000;
                                formattedNumber = formatterValue.format(myNumber);
                                DateLocal dateLocal = new DateLocal();
                                periodeBulan = dateLocal.getMonthAlphabet(periodeBulangIden);
                            }
                        }
                    }
                }
                if(rowsIden==8){
                    periodeBulangIden = "09";
                    for (rows= 0; rows < objectList.size(); rows++) {
                        salesBilling = (com.indoagri.comsatic.Retrofit.Model.SalesBilling) objectList.get(rows);
                        if(salesBilling.getPeriode().equalsIgnoreCase(periodeBulangIden)){
                            formatterValue = new DecimalFormat("#,###,###,###,###,###");
                            myQty = Double.parseDouble(salesBilling.getQty())/1000;
                            formatQty = formatterValue.format(myQty);
                            myNumbervalue= Double.parseDouble(salesBilling.getValue())/1000000;
                            formatValue = formatterValue.format(myNumbervalue);
                            myNumber= (myNumbervalue/myQty)*1000;
                            formattedNumber = formatterValue.format(myNumber);
                            totalQty += myQty;
                            formatQtyTotal = QtyFormat.format(totalQty);
                            totalValue+= myNumbervalue;
                            formatValueTotal= QtyFormat.format(totalValue);
                            totalPrice = (totalValue/totalQty)*1000;
                            formatPriceTotal= QtyFormat.format(totalPrice);
                            DateLocal dateLocal = new DateLocal();
                            periodeBulan = dateLocal.getMonthAlphabet(periodeBulangIden);
                            bln9 = true;
                        }else{
                            if(bln9){
                            }else{
                                formatterValue = new DecimalFormat("#,###,###,###,###,###");
                                myQty = 0/1000;
                                formatQty = formatterValue.format(myQty);
                                myNumbervalue= 0/1000000;
                                formatValue = formatterValue.format(myNumbervalue);
                                myNumber= 0/1000000;
                                formattedNumber = formatterValue.format(myNumber);
                                DateLocal dateLocal = new DateLocal();
                                periodeBulan = dateLocal.getMonthAlphabet(periodeBulangIden);
                            }
                        }
                    }
                }
                if(rowsIden==9){
                    periodeBulangIden = "10";
                    for (rows= 0; rows < objectList.size(); rows++) {
                        salesBilling = (com.indoagri.comsatic.Retrofit.Model.SalesBilling) objectList.get(rows);
                        if(salesBilling.getPeriode().equalsIgnoreCase(periodeBulangIden)){
                            formatterValue = new DecimalFormat("#,###,###,###,###,###");
                            myQty = Double.parseDouble(salesBilling.getQty())/1000;
                            formatQty = formatterValue.format(myQty);
                            myNumbervalue= Double.parseDouble(salesBilling.getValue())/1000000;
                            formatValue = formatterValue.format(myNumbervalue);
                            myNumber= (myNumbervalue/myQty)*1000;
                            formattedNumber = formatterValue.format(myNumber);
                            totalQty += myQty;
                            formatQtyTotal = QtyFormat.format(totalQty);
                            totalValue+= myNumbervalue;
                            formatValueTotal= QtyFormat.format(totalValue);
                            totalPrice = (totalValue/totalQty)*1000;
                            formatPriceTotal= QtyFormat.format(totalPrice);
                            DateLocal dateLocal = new DateLocal();
                            periodeBulan = dateLocal.getMonthAlphabet(periodeBulangIden);
                            bln10 = true;
                        }else{
                            if(bln10){
                            }else{
                                formatterValue = new DecimalFormat("#,###,###,###,###,###");
                                myQty = 0/1000;
                                formatQty = formatterValue.format(myQty);
                                myNumbervalue= 0/1000000;
                                formatValue = formatterValue.format(myNumbervalue);
                                myNumber= 0/1000000;
                                formattedNumber = formatterValue.format(myNumber);
                                DateLocal dateLocal = new DateLocal();
                                periodeBulan = dateLocal.getMonthAlphabet(periodeBulangIden);
                            }
                        }
                    }
                }
                if(rowsIden==10){
                    periodeBulangIden = "11";
                    for (rows= 0; rows < objectList.size(); rows++) {
                        salesBilling = (com.indoagri.comsatic.Retrofit.Model.SalesBilling) objectList.get(rows);
                        if(salesBilling.getPeriode().equalsIgnoreCase(periodeBulangIden)){
                            formatterValue = new DecimalFormat("#,###,###,###,###,###");
                            myQty = Double.parseDouble(salesBilling.getQty())/1000;
                            formatQty = formatterValue.format(myQty);
                            myNumbervalue= Double.parseDouble(salesBilling.getValue())/1000000;
                            formatValue = formatterValue.format(myNumbervalue);
                            myNumber= (myNumbervalue/myQty)*1000;
                            formattedNumber = formatterValue.format(myNumber);
                            totalQty += myQty;
                            formatQtyTotal = QtyFormat.format(totalQty);
                            totalValue+= myNumbervalue;
                            formatValueTotal= QtyFormat.format(totalValue);
                            totalPrice = (totalValue/totalQty)*1000;
                            formatPriceTotal= QtyFormat.format(totalPrice);
                            DateLocal dateLocal = new DateLocal();
                            periodeBulan = dateLocal.getMonthAlphabet(periodeBulangIden);
                            bln11 = true;
                        }else{
                            if(bln11){
                            }else{
                                formatterValue = new DecimalFormat("#,###,###,###,###,###");
                                myQty = 0/1000;
                                formatQty = formatterValue.format(myQty);
                                myNumbervalue= 0/1000000;
                                formatValue = formatterValue.format(myNumbervalue);
                                myNumber= 0/1000000;
                                formattedNumber = formatterValue.format(myNumber);
                                DateLocal dateLocal = new DateLocal();
                                periodeBulan = dateLocal.getMonthAlphabet(periodeBulangIden);
                            }
                        }
                    }
                }
                if(rowsIden==11){
                    periodeBulangIden = "12";
                    for (rows= 0; rows < objectList.size(); rows++) {
                        salesBilling = (com.indoagri.comsatic.Retrofit.Model.SalesBilling) objectList.get(rows);
                        if(salesBilling.getPeriode().equalsIgnoreCase(periodeBulangIden)){
                            formatterValue = new DecimalFormat("#,###,###,###,###,###");
                            myQty = Double.parseDouble(salesBilling.getQty())/1000;
                            formatQty = formatterValue.format(myQty);
                            myNumbervalue= Double.parseDouble(salesBilling.getValue())/1000000;
                            formatValue = formatterValue.format(myNumbervalue);
                            myNumber= (myNumbervalue/myQty)*1000;
                            formattedNumber = formatterValue.format(myNumber);
                            totalQty += myQty;
                            formatQtyTotal = QtyFormat.format(totalQty);
                            totalValue+= myNumbervalue;
                            formatValueTotal= QtyFormat.format(totalValue);
                            totalPrice = (totalValue/totalQty)*1000;
                            formatPriceTotal= QtyFormat.format(totalPrice);
                            DateLocal dateLocal = new DateLocal();
                            periodeBulan = dateLocal.getMonthAlphabet(periodeBulangIden);
                            bln12 = true;
                        }else{
                            if(bln12){
                            }else{
                                formatterValue = new DecimalFormat("#,###,###,###,###,###");
                                myQty = 0/1000;
                                formatQty = formatterValue.format(myQty);
                                myNumbervalue= 0/1000000;
                                formatValue = formatterValue.format(myNumbervalue);
                                myNumber= 0/1000000;
                                formattedNumber = formatterValue.format(myNumber);
                                DateLocal dateLocal = new DateLocal();
                                periodeBulan = dateLocal.getMonthAlphabet(periodeBulangIden);
                            }
                        }
                    }
                }
            }else {
                textSpacer = new TextView(this);
                textSpacer.setText("");
            }

            final TextView tvPeriode = new TextView(this);
            if (rowsIden == -1) {
                tvPeriode.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.WRAP_CONTENT));
                tvPeriode.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            } else {
                tvPeriode.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                tvPeriode.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
            }

            tvPeriode.setGravity(Gravity.LEFT);

            tvPeriode.setPadding(15, 15, 0, 15);
            if (rowsIden == -1) {
                tvPeriode.setText("Period");
                tvPeriode.setTextColor(getResources().getColor(R.color.colorWhiteFont));
                tvPeriode.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
            }else {
                tvPeriode.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                tvPeriode.setTextColor(getResources().getColor(R.color.colorBlackGray));
                tvPeriode.setText(periodeBulan);
            }
            final LinearLayout layQty = new LinearLayout(this);
            layQty.setOrientation(LinearLayout.VERTICAL);
            layQty.setGravity(Gravity.RIGHT);
            layQty.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            final TextView tvQty = new TextView(this);
            tvQty.setPadding(5, 15, 15, 15);
            if (rowsIden == -1) {
                tvQty.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                tvQty.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            }
            else {
                tvQty.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                tvQty.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
            }
            tvQty.setGravity(Gravity.RIGHT);
            if (rowsIden == -1) {
                tvQty.setText("Qty (Ton)");
                tvQty.setTextColor(getResources().getColor(R.color.colorWhiteFont));
                tvQty.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
            }
            else {
                tvQty.setBackgroundColor(getResources().getColor(R.color.colorWhiteinit));
                tvQty.setTextColor(getResources().getColor(R.color.colorBlackGray));
                tvQty.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
                tvQty.setText(formatQty);
            }
            layQty.addView(tvQty);



            final LinearLayout layValue = new LinearLayout(this);
            layValue.setOrientation(LinearLayout.VERTICAL);
            layValue.setGravity(Gravity.RIGHT);
            layValue.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));



            final TextView tvValue = new TextView(this);
            tvValue.setPadding(5, 15, 15, 15);
            if (rowsIden == -1) {
                tvValue.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                layValue.setBackgroundColor(Color.parseColor("#f7f7f7"));
            } else {
                tvValue.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.WRAP_CONTENT));
                layValue.setBackgroundColor(Color.parseColor("#ffffff"));
            }
            tvValue.setGravity(Gravity.RIGHT);
            if (rowsIden == -1) {
                tvValue.setText("Value (Rp Mio)");
                tvValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
                tvValue.setTextColor(getResources().getColor(R.color.colorWhiteFont));
                tvValue.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
            } else {
                tvValue.setBackgroundColor(getResources().getColor(R.color.colorWhiteinit));
                tvValue.setTextColor(getResources().getColor(R.color.colorBlackGray));
                tvValue.setText(formatValue);
                tvValue.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
            }

            layValue.addView(tvValue);

            final LinearLayout layPrices = new LinearLayout(this);
            layPrices.setOrientation(LinearLayout.VERTICAL);
            layPrices.setGravity(Gravity.RIGHT);
            layPrices.setBackgroundColor(Color.parseColor("#f8f8f8"));

            final TextView tvPrice = new TextView(this);

            tvPrice.setPadding(5, 15, 15, 15);
            if (rowsIden == -1) {
                tvPrice.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                tvPrice.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);

            } else {
                tvPrice.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                tvPrice.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);

            }

            tvPrice.setGravity(Gravity.RIGHT);

            if (rowsIden == -1) {
                tvPrice.setText("Price (Rp/kg)");
                tvPrice.setTextColor(getResources().getColor(R.color.colorWhiteFont));
                tvPrice.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
            } else {
                tvPrice.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                tvPrice.setTextColor(getResources().getColor(R.color.colorBlackGray));
                tvPrice.setText(formattedNumber);
                tvPrice.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
            }
            layPrices.addView(tvPrice);

            // add table row
            final TableRow tr = new TableRow(this);
            tr.setId(rowsIden + 1);
            TableLayout.LayoutParams trParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT);
            trParams.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
            tr.setPadding(0,0,0,0);
            tr.setLayoutParams(trParams);

            //tr.addView(tv);
            tr.addView(tvPeriode);
            tr.addView(layQty);
            tr.addView(layValue);
            tr.addView(layPrices);
            if (rowsIden > -1) {
                tr.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        TableRow tr = (TableRow) v;
                        //do whatever action is needed
                     //   Toast.makeText(SalesBilling.this,"Test Toast",Toast.LENGTH_SHORT).show();
                    }
                });
            }
            tableLayout.addView(tr, trParams);
            if (rowsIden > -1) {
                // add separator row
                final TableRow trSep = new TableRow(this);
                TableLayout.LayoutParams trParamsSep = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                        TableLayout.LayoutParams.WRAP_CONTENT);
                trParamsSep.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);

                trSep.setLayoutParams(trParamsSep);
                TextView tvSep = new TextView(this);
                TableRow.LayoutParams tvSepLay = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.WRAP_CONTENT);
                tvSepLay.span = 4;
                tvSep.setLayoutParams(tvSepLay);
                tvSep.setBackgroundColor(Color.parseColor("#d9d9d9"));
                tvSep.setHeight(1);
                trSep.addView(tvSep);
                tableLayout.addView(trSep, trParamsSep);
            }
        }

        tableLayoutfooter.removeAllViews();
        final TextView tvPeriodeFooter = new TextView(this);
        tvPeriodeFooter.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
        tvPeriodeFooter.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        tvPeriodeFooter.setGravity(Gravity.LEFT);
        tvPeriodeFooter.setPadding(15, 15, 0, 15);
        tvPeriodeFooter.setText("Total");
        tvPeriodeFooter.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvPeriodeFooter.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));

        final LinearLayout layQtyFooter = new LinearLayout(this);
        layQtyFooter.setOrientation(LinearLayout.VERTICAL);
        layQtyFooter.setGravity(Gravity.RIGHT);
        layQtyFooter.setBackgroundColor(getResources().getColor(R.color.colorWhiteFont));
        final TextView tvQtyFooter = new TextView(this);
        tvQtyFooter.setPadding(5, 15, 15, 15);
        tvQtyFooter.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        tvQtyFooter.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        tvQtyFooter.setGravity(Gravity.RIGHT);
        tvQtyFooter.setEms(6);
        tvQtyFooter.setText(formatQtyTotal);
        tvQtyFooter.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvQtyFooter.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        layQtyFooter.addView(tvQtyFooter);


        final LinearLayout layValueFooter = new LinearLayout(this);
        layValueFooter.setOrientation(LinearLayout.VERTICAL);
        layValueFooter.setGravity(Gravity.RIGHT);
        layValueFooter.setBackgroundColor(getResources().getColor(R.color.colorWhiteFont));
        final TextView tvValueFooter = new TextView(this);
        tvValueFooter.setPadding(5, 15, 15, 15);
        tvValueFooter.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        tvValueFooter.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        tvValueFooter.setGravity(Gravity.RIGHT);
        tvValueFooter.setEms(6);
        tvValueFooter.setText(formatValueTotal);
        tvValueFooter.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvValueFooter.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        layValueFooter.addView(tvValueFooter);


        final LinearLayout layPricesFooter = new LinearLayout(this);
        layPricesFooter.setOrientation(LinearLayout.VERTICAL);
        layPricesFooter.setGravity(Gravity.RIGHT);
        layPricesFooter.setBackgroundColor(getResources().getColor(R.color.colorWhiteFont));
        final TextView tvPriceFooter = new TextView(this);
        tvPriceFooter.setPadding(5, 15, 15, 15);
        tvPriceFooter.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
        tvPriceFooter.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        tvPriceFooter.setGravity(Gravity.RIGHT);
        tvPriceFooter.setEms(6);
        tvPriceFooter.setText(formatPriceTotal);
        tvPriceFooter.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvPriceFooter.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        layPricesFooter.addView(tvPriceFooter);

        // add table row
        final TableRow trFooter = new TableRow(this);
        TableLayout.LayoutParams trParamsFooter = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT);
        trParamsFooter.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
        trFooter.setPadding(0,0,0,0);
        trFooter.setLayoutParams(trParamsFooter);

        //tr.addView(tv);
        trFooter.addView(tvPeriodeFooter);
        trFooter.addView(layQtyFooter);
        trFooter.addView(layValueFooter);
        trFooter.addView(layPricesFooter);
        tableLayoutfooter.addView(trFooter, trParamsFooter);

    }

    class LoadDataTask extends AsyncTask<Integer, Integer, String> {
        @Override
        protected String doInBackground(Integer... params) {

            try {
                Thread.sleep(2000);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return "Task Completed.";
        }
        @Override
        protected void onPostExecute(String result) {
//            mProgressbar.setVisibility(View.GONE);
            pDialog.dismiss();
            //loadData();
        }
        @Override
        protected void onPreExecute() {
        }
        @Override
        protected void onProgressUpdate(Integer... values) {

        }
    }

    public void toSearch(){
        searchFragment.setVisibility(View.VISIBLE);
        frame_display.setVisibility(View.GONE);
        Fragment fragment = new FormSearching();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
        ShowSearch = true;
    }
    @Override
    public void onBackPressed() {
        if(searchFragment.getVisibility()==View.VISIBLE){
            searchFragment.setVisibility(View.GONE);
            frame_display.setVisibility(View.VISIBLE);
        }
        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();
            new SharePreference(SalesBilling.this).setFormGroupcompany("");
            new SharePreference(SalesBilling.this).setFormCompany("");
            new SharePreference(SalesBilling.this).setFormProduct("");
            new SharePreference(SalesBilling.this).setFormSubproduct("");
            new SharePreference(SalesBilling.this).setFormYear("");
        } else {
            getSupportFragmentManager().popBackStack();
        }
        ShowSearch = false;
    }





    private boolean deletetable(){
        progressBar.setVisibility(View.VISIBLE);
        boolean result = false;
        query.openTransaction();
        int rowID = 0;
        int rowID2 = 0;
        try{
            rowID =  query.deleteDataTemporary(GroupCompanyModel.TABLE_NAME,null,null);
            if(rowID>0 || rowID==0){
                rowID2 = query.deleteDataTemporary(GroupProductModel.TABLE_NAME,null,null);
                if(rowID2>0||rowID2==0){
                    query.deleteDataTemporary(com.indoagri.comsatic.Retrofit.Model.SalesBilling.TABLE_NAME,null,null);
                    query.deleteDataTemporary(SalesDataModel.TABLE_NAME,null,null);
                    query.deleteDataTable(GroupCompanyModel.TABLE_NAME,null,null);
                    query.deleteDataTable(GroupProductModel.TABLE_NAME,null,null);
                    query.deleteDataTable(com.indoagri.comsatic.Retrofit.Model.SalesBilling.TABLE_NAME,null,null);
                    query.deleteDataTable(SalesDataModel.TABLE_NAME,null,null);
                }
            }
        }finally {
            query.commitTransaction();
            query.closeTransaction();
            result = true;
        }
        return result;
    }



    private void GetDataCompany(){
        if (progressBar.getVisibility() == View.GONE) {
            progressBar.setVisibility(View.VISIBLE);
        }
        Log.d(TAGCOMPANY, "LOADAGE: ");

        GetCompanies().enqueue(new Callback<GroupCompanyResponse>() {
            @Override
            public void onResponse(Call<GroupCompanyResponse> call, Response<GroupCompanyResponse> response) {
                // Got data. Send it to adapter
                progressBar.setVisibility(View.GONE);
                if(response.code()==200){
                    groupCompanyResponse = fetchResultsCompanies(response);
                    groupCompanyModelList = groupCompanyResponse.getCompanies();
                    setupInsertCompany();

                }else{
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.gagal), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GroupCompanyResponse> call, Throwable t) {
                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
                // TODO: 08/11/16 handle failure
               // Toast.makeText(getApplicationContext(),t.toString(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void GetDataProduct(){
        if (progressBar.getVisibility() == View.GONE) {
            progressBar.setVisibility(View.VISIBLE);
        }
        Log.d(TAGCOMPANY, "LOADAGE: ");

        GetProducts().enqueue(new Callback<GroupProductResponse>() {
            @Override
            public void onResponse(Call<GroupProductResponse> call, Response<GroupProductResponse> response) {
                // Got data. Send it to adapter
                progressBar.setVisibility(View.GONE);
                if(response.code()==200){
                    groupProductResponse = fetchResultsProducts(response);
                    groupProductModelList = groupProductResponse.getProducts();
                    setupInsertProducts();

                }else{
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.gagal), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GroupProductResponse> call, Throwable t) {
                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
                // TODO: 08/11/16 handle failure
               // Toast.makeText(getApplicationContext(),t.toString(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void GetDataSales(){
        if (progressBar.getVisibility() == View.GONE) {
            progressBar.setVisibility(View.VISIBLE);
        }
        Log.d(TAGCOMPANY, "LOADAGE: ");

        GetDataTransaction().enqueue(new Callback<SalesDataResponse>() {
            @Override
            public void onResponse(Call<SalesDataResponse> call, Response<SalesDataResponse> response) {
                // Got data. Send it to adapter
                progressBar.setVisibility(View.GONE);
                if(response.code()==200){
                    salesDataResponse = fetchResultsSales(response);
                    salesDataModelList = salesDataResponse.getSales();
                    setupInsertSales();

                }else{
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.gagal), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SalesDataResponse> call, Throwable t) {
                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
                // TODO: 08/11/16 handle failure
               // Toast.makeText(getApplicationContext(),t.toString(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setupInsertCompany(){
        query.openTransaction();
        for (int i = 0; i < groupCompanyModelList.size(); i++) {
            GroupCompanyModel groupCompanyModel=groupCompanyModelList.get(i);
            ContentValues values = new ContentValues();
            values.put(GroupCompanyModel.XML_groupcompany,groupCompanyModel.getGroupcompany().trim());
            values.put(GroupCompanyModel.XML_companycode,groupCompanyModel.getCompanycode().trim());
            values.put(GroupCompanyModel.XML_description,groupCompanyModel.getDescription().trim());
            query.insertDataSQL(GroupCompanyModel.TABLE_NAME,values);
        }
        query.commitTransaction();
        query.closeTransaction();
        GetDataProduct();
    }

    private void setupInsertProducts(){
        query.openTransaction();
        for (int i = 0; i < groupProductModelList.size(); i++) {
            GroupProductModel groupProductModel=groupProductModelList.get(i);
            ContentValues values = new ContentValues();
            values.put(GroupProductModel.XML_groupproduct,groupProductModel.getGroupproduct().trim());
            values.put(GroupProductModel.XML_subgroup,groupProductModel.getSubgroup().trim());
            values.put(GroupProductModel.XML_material,groupProductModel.getMaterial().trim());
            values.put(GroupProductModel.XML_materialDescription,groupProductModel.getMaterialdescription());
            query.insertDataSQL(GroupProductModel.TABLE_NAME,values);
        }
        query.commitTransaction();
        query.closeTransaction();
        GetDataSales();
    }

    private void setupInsertSales(){
        query.openTransaction();
        for (int i = 0; i < salesDataModelList.size(); i++) {
            SalesDataModel salesDataModel=salesDataModelList.get(i);

            ContentValues values = new ContentValues();
            values.put(SalesDataModel.XML_idx, salesDataModel.getIdx());
            values.put(SalesDataModel.XML_source, salesDataModel.getSource());
            values.put(SalesDataModel.XML_vbeln, salesDataModel.getVbeln());
            values.put(SalesDataModel.XML_posnr, salesDataModel.getPosnr());
            values.put(SalesDataModel.XML_vkorg, salesDataModel.getVkorg());
            values.put(SalesDataModel.XML_vtweg, salesDataModel.getVtweg());
            values.put(SalesDataModel.XML_spart, salesDataModel.getSpart());
            values.put(SalesDataModel.XML_erdat, salesDataModel.getErdat());
            values.put(SalesDataModel.XML_erzet, salesDataModel.getErzet());
            values.put(SalesDataModel.XML_audat, salesDataModel.getAudat());
            values.put(SalesDataModel.XML_auart, salesDataModel.getAuart());
            values.put(SalesDataModel.XML_kunnr, salesDataModel.getKunnr());
            values.put(SalesDataModel.XML_namE1, salesDataModel.getNameE1());
            values.put(SalesDataModel.XML_zterm, salesDataModel.getZterm());
            values.put(SalesDataModel.XML_incO1, salesDataModel.getInco1());
            values.put(SalesDataModel.XML_incO2, salesDataModel.getInco2());
            values.put(SalesDataModel.XML_werks, salesDataModel.getWerks());
            values.put(SalesDataModel.XML_vstel, salesDataModel.getVstel());
            values.put(SalesDataModel.XML_matnr, salesDataModel.getMatnr().substring(12));
//            values.put(SalesDataModel.getXML_matnr(),salesDataModel.getMatnr());
            values.put(SalesDataModel.XML_arktx, salesDataModel.getArktx());
            values.put(SalesDataModel.XML_kwmeng, salesDataModel.getKwmeng());
            values.put(SalesDataModel.XML_vrkme, salesDataModel.getVrkme());
            values.put(SalesDataModel.XML_netwr, salesDataModel.getNetwr());
            values.put(SalesDataModel.XML_waerk, salesDataModel.getWaerk());
            values.put(SalesDataModel.XML_gjahr, salesDataModel.getGjahr());
            values.put(SalesDataModel.XML_monat, salesDataModel.getMonat());

            query.insertDataSQL(SalesDataModel.TABLE_NAME,values);
        }
        query.commitTransaction();
        query.closeTransaction();
        new SharePreference(SalesBilling.this).setUpdateSyncDateSales(dateToStr);
        uptoDate.setText(new SharePreference(SalesBilling.this).isUpdateSyncSales());
    }

    // PROTECTED //

    // company //
    private GroupCompanyResponse fetchResultsCompanies(Response<GroupCompanyResponse> response) {
        GroupCompanyResponse fleetsResponse = response.body();
        return fleetsResponse;
    }

    private Call<GroupCompanyResponse> GetCompanies() {
        return apiServices.getCompany();
    }

    // products //
    private GroupProductResponse fetchResultsProducts(Response<GroupProductResponse> response) {
        GroupProductResponse fleetsResponse = response.body();
        return fleetsResponse;
    }

    private Call<GroupProductResponse> GetProducts() {
        return apiServices.getProducts();
    }

    // salesdata //
    private SalesDataResponse fetchResultsSales(Response<SalesDataResponse> response) {
        SalesDataResponse fleetsResponse = response.body();
        return fleetsResponse;
    }

    private Call<SalesDataResponse> GetDataTransaction() {
        return apiServices.getDataSales();
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == SUBPRODUCTSLIST) {
            if(resultCode == RESULT_OK) {
                String subproducts=data.getStringExtra("SUBPRODUCTS");
                Log.i("Message is",subproducts+" "+subproducts);
                if(subproducts==null){

                }else{
                    new SharePreference(SalesBilling.this).setFormSubproduct(subproducts);
                    if(getFragmentRefreshListener()!=null){
                        getFragmentRefreshListener().onRefresh();
                    }
                }
            }
        }
        if (requestCode == COMPANYLIST) {
            if(resultCode == RESULT_OK) {
                String CODE=data.getStringExtra("CODE");
                String DESC=data.getStringExtra("DESC");
                Log.i("Message is",CODE+" "+DESC);
                if(CODE==null){

                }else{
                    new SharePreference(SalesBilling.this).setFormCompany(CODE);
                    new SharePreference(SalesBilling.this).setFormCompanyDesc(DESC);
                    if(getFragmentRefreshListener()!=null){
                        getFragmentRefreshListener().onRefresh();
                    }
                }
            }
        }
    }
    public interface FragmentRefreshListener{
        void onRefresh();
    }

}
