package com.indoagri.comsatic.SalesBilling;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.indoagri.comsatic.R;
import com.indoagri.comsatic.Retrofit.DB.DatabaseQuery;
import com.indoagri.comsatic.Retrofit.Model.GroupProductModel;
import com.indoagri.comsatic.Retrofit.SharePreference;

import java.util.ArrayList;
import java.util.List;

public class ListSubProduct extends AppCompatActivity implements AdapterView.OnItemClickListener, TextWatcher {

    Bundle extras;
    String extrasString;
    Toolbar toolbar;
    TextView mTextToolbar,txtSetAll;
    ListView listView;
    EditText etSearch;
    DatabaseQuery query;
    SharePreference sharedPreferences;
    List<GroupProductModel> productModelList;
    ListSubProductAdapter listSubProductAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_sub_product);
        setUpToolbar();
        sharedPreferences = new SharePreference(ListSubProduct.this);
        query = new DatabaseQuery(ListSubProduct.this);
        listView=(ListView) findViewById(R.id.listView);
        etSearch = findViewById(R.id.et_search);
        etSearch.setVisibility(View.VISIBLE);
        txtSetAll = (findViewById(R.id.txt_setAll));
        txtSetAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String subproduct="";
                Intent intent=new Intent();
                intent.putExtra("SUBPRODUCTS",subproduct);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        extras = getIntent().getExtras();
        if (extras != null) {
            extrasString = extras.getString("Group");
            init();
            // and get whatever type user account id is
            //Toast.makeText(this, extrasString+" || "+new SharePreference(ListSubProduct.this).isFormProduct(), Toast.LENGTH_SHORT).show();
        }else{

        }

    }
    private void setUpToolbar() {
        toolbar = (findViewById(R.id.toolbar));
        mTextToolbar= (findViewById(R.id.txt_Toolbar));
        mTextToolbar.setText(getResources().getString(R.string.salesprice));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    void init(){
        productModelList=new ArrayList<GroupProductModel>();
        etSearch.addTextChangedListener(this);
        listView.setOnItemClickListener(this);
        listSubProductAdapter = new ListSubProductAdapter(this, R.layout.item_listcontent,productModelList);
        listView.setAdapter(listSubProductAdapter);
        listView.setScrollingCacheEnabled(false);
        setHalaman();

    }

    private void setHalaman(){
        etSearch.addTextChangedListener(this);
        List<Object> listObject;
        query.openTransaction();
        String sqldb_query = "select distinct groupproduct,subgroup from T_GROUPPRODUCT where groupProduct=?";
        String isAD = sharedPreferences.isFormProduct();
        String[] a = new String[1];
        a[0] = isAD;
        listObject = query.getListDataRawQuery(sqldb_query,GroupProductModel.TABLE_NAME,a);
        query.closeTransaction();
        if(listObject.size() > 0){
            for(int i = 0; i < listObject.size(); i++){
                GroupProductModel hasil = (GroupProductModel)listObject.get(i);
                productModelList.add(hasil);
            }
        }
        for(int i = 0; i < productModelList.size(); i++){
            GroupProductModel productModel = (GroupProductModel) productModelList.get(i);
            listSubProductAdapter.addData(productModel);

        }


    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        listSubProductAdapter.getFilter().filter(s);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
        GroupProductModel subProductAdapterItem= (GroupProductModel) listSubProductAdapter.getItem(i);
        String subproduct=subProductAdapterItem.getSubgroup();
        Intent intent=new Intent();
        intent.putExtra("SUBPRODUCTS",subproduct);
        setResult(RESULT_OK, intent);
        finish();
    }
}
