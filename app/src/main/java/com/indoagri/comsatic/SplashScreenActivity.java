package com.indoagri.comsatic;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.indoagri.comsatic.Common.Constants;
import com.indoagri.comsatic.Retrofit.SharePreference;

import static com.indoagri.comsatic.Retrofit.Model.UserModel.XML_DOMAIN;
import static com.indoagri.comsatic.Retrofit.Model.UserModel.XML_EMAIL;
import static com.indoagri.comsatic.Retrofit.Model.UserModel.XML_USERNAME;
import static com.indoagri.comsatic.Retrofit.Model.UserModel.XML_USERPASSWORD;

public class SplashScreenActivity extends AppCompatActivity {
    ProgressDialog pDialog;
    boolean isLogin,isShowing;
    String userad;
    String useremail;
    String userdomain;
    String userpassword;
    Cursor cursor;
    @Override
    protected void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);
        TextView txtVersion = (findViewById(R.id.txtVersion));
        txtVersion.setText("Vers. "+ Constants.versionApps);
        pDialog = new ProgressDialog(SplashScreenActivity.this);
        isShowing = pDialog.isShowing();

        if (Build.VERSION.SDK_INT >= 23)
        {
            if (checkPermission())
            {
                initLogin();
            } else {
                requestPermission(); // Code for permission
            }
        }
        else
        {
            initLogin();
        }
    }

    private void Init(Context context, String dialogMessage){
        pDialog.setMessage(dialogMessage);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setCancelable(false);
        pDialog.show();
        initLogin();
    }
    private void Running(){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if(!isFinishing()){
                    pDialog.dismiss();
                    isLogin = new SharePreference(SplashScreenActivity.this).isLoggedIn();
                    if(isLogin){
                        Intent intent = new Intent(SplashScreenActivity.this, CheckVersionActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }else{
                        Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }

                }
            }
        }, 3000);
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(SplashScreenActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }
    private static final int PERMISSION_REQUEST_CODE = 1;
    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(SplashScreenActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(SplashScreenActivity.this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(SplashScreenActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("value", "Permission Granted, Now you can use local drive .");
                    String Message = getResources().getString(R.string.msg_data_initialitation);
                    Init(SplashScreenActivity.this,Message);
                } else {
                    Log.e("value", "Permission Denied, You cannot use local drive .");
                }
                break;
        }
    }
    private void initLogin(){
        pDialog.dismiss();
        cursor = getContentResolver().query(Uri.parse("content://com.simp.portal.db.UserProvider/users"), null, null, null, null);
        if(cursor!=null){
            if (cursor.moveToFirst()) {
                isLogin = (cursor.getInt(cursor.getColumnIndex("isLogin")) == 1);
                if(isLogin){
                    userad = cursor.getString(cursor.getColumnIndex(XML_USERNAME));
                    useremail = cursor.getString(cursor.getColumnIndex(XML_EMAIL));
                    userdomain = cursor.getString(cursor.getColumnIndex(XML_DOMAIN));
                    userpassword = cursor.getString(cursor.getColumnIndex(XML_USERPASSWORD));
                    new SharePreference(SplashScreenActivity.this).setKeyUsername(userad.toLowerCase());
                    new SharePreference(SplashScreenActivity.this).setFormLoginUsername(userad.toLowerCase());
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Running();
                        }
                    }, 3000);
                }else{
                    new SharePreference(SplashScreenActivity.this).setLogin(false);
                    Intent a = new Intent(SplashScreenActivity.this, LoginActivity.class);
                    a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    finish();
                    startActivity(a);
                }
            }else{

            }
        }else{
            Intent a = new Intent(SplashScreenActivity.this, LoginActivity.class);
            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            finish();
            startActivity(a);
        }
    }
}
