package com.indoagri.comsatic;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.indoagri.comsatic.Retrofit.DB.DatabaseQuery;
import com.indoagri.comsatic.Retrofit.Model.PriceDataModel;
import com.indoagri.comsatic.widget.calendar.activity.CustomCalendar;

import java.util.ArrayList;
import java.util.List;


public class ChooseDate extends AppCompatActivity implements CustomCalendar.CalendarListener{

    DatabaseQuery query;
    Toolbar toolbar;
    TextView mTextToolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_date);
        toolbar = findViewById(R.id.toolbar);
        query = new DatabaseQuery(ChooseDate.this);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        fab.hide();
        setUpToolbar();
        DataPrice();
    }

        private void DataPrice(){
        CustomCalendar custom = findViewById(R.id.mycalendar);
        custom.setCalendarListener(this);
        query.openTransaction();
        List<Object> listObject;
        List<PriceDataModel> listTemp = new ArrayList<PriceDataModel>();
        listTemp.clear();
        listObject = null;
        String sqldb_query = "SELECT * FROM Table_Price GROUP BY DATE";
        listObject = query.getListDataRawQuery(sqldb_query, PriceDataModel.TABLE_NAME,null);
        query.closeTransaction();
        if(listObject==null){
            Toast.makeText(this, "Data Tidak ada", Toast.LENGTH_SHORT).show();
        }
        else{
            if(listObject.size() > 0){
                for (int i = 0; i < listObject.size(); i++) {
                    PriceDataModel md = (PriceDataModel) listObject.get(i);
                    String tgl = md.getDate().substring(0,10);
                    String message = md.getDescription();
                    custom.setEventDate(tgl, message, "All Day");
                }
                custom.refresh();
            }else{
                Toast.makeText(this, "Data Tidak ada", Toast.LENGTH_SHORT).show();
            }
        }

    }
    private void setUpToolbar() {
        mTextToolbar= (findViewById(R.id.txt_Toolbar));
        mTextToolbar.setText("Choose Date");
        toolbar = (findViewById(R.id.toolbar));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed(){
        Bundle mBundle = new Bundle();
        mBundle.putString("tanggal", null);
        mBundle.putString("message","Kirim Tanggal");
        Intent intent=new Intent();
        intent.putExtras(mBundle);
        setResult(99,intent);
        finish();//finishing activity
        }

    @Override
    public void onDayClick(String date) {
        Bundle mBundle = new Bundle();
        mBundle.putString("TANGGAL", date);
        mBundle.putString("MESSAGE","Kirim Tanggal");
        Intent intent=new Intent();
        intent.putExtras(mBundle);
        setResult(RESULT_OK,intent);
        finish();//finishing activity
        //Toast.makeText(getApplicationContext(), "Anda Memilih Tanggal " + date.substring(0,10), Toast.LENGTH_SHORT).show();
    }
}
