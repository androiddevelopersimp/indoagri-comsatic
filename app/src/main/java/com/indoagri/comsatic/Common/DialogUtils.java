package com.indoagri.comsatic.Common;

import android.app.Activity;
import android.app.ProgressDialog;

/**
 * Created by Dwi.Fajar on 2/21/2018.
 */

public class DialogUtils {

    public static ProgressDialog mProgressDialogShow(Activity activity, String message){
        ProgressDialog m_Dialog = new ProgressDialog(activity);
        m_Dialog.setMessage(message);
        m_Dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        m_Dialog.setCancelable(false);
        m_Dialog.show();
        return m_Dialog;
    }


}