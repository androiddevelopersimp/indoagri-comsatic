package com.indoagri.comsatic.Common;

import android.content.Context;
import android.os.Build;
import android.preference.PreferenceManager;

import com.indoagri.comsatic.Apps;
import com.indoagri.comsatic.BuildConfig;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Constants {

    public static String PRODUCTIONAPI = "https://webapp.indoagri.co.id/Imobile/api/";
    public static String DEVELOPMENTAPI = "http://dev.indoagri.co.id/fajar/imobile/api/";
    static Context context = Apps.getAppContext();

    public static  final String APIKEY = "0ooksgckckgokscw8ckook44kokow4040cgkoo8w";
    PreferenceManager preferenceManager;
    public static final String const_VersionName = DeviceUtils.getAppVersion(context);
    public static final String const_AppsName = DeviceUtils.getAppName(context);
    public static final String const_APIVERSION = String.valueOf(DeviceUtils.APIVERSION(context));
    public static final String shared_name = DeviceUtils.getAppName(context)+"Pref";
    public static final String db_name = DeviceUtils.getAppName(context)+".db";
    public static final String apk_name = DeviceUtils.getAppName(context)+".apk";
    public static final int db_version = BuildConfig.VERSION_CODE;
    public static final String versionApps = BuildConfig.VERSION_NAME;
}
