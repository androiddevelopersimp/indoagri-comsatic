package com.indoagri.comsatic.Common;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import static com.indoagri.comsatic.Common.NetworkUtils.isConnected;
import static com.indoagri.comsatic.Common.NetworkUtils.isConnectedFast;
import static com.indoagri.comsatic.Common.NetworkUtils.isConnectedMobile;
import static com.indoagri.comsatic.Common.NetworkUtils.isConnectedWifi;

public class BroadcastConnectorHandler extends BroadcastReceiver
{
    public static ConnectivityReceiverListener connectivityReceiverListener;

    public BroadcastConnectorHandler() {
        super();
    }
    @Override
    public void onReceive(Context context, Intent intent)
    {
        try
        {
            if (connectivityReceiverListener != null) {
                connectivityReceiverListener.onNetworkConnectionChanged(isConnected(context));
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    public static boolean isConnect(Context context) {
        boolean result = false;


        if(isConnected(context)){
            result = true;
        }
        if(isConnectedFast(context)){
            result = true;
        }
        if(isConnectedMobile(context)){
            result = true;
        }
        if(isConnectedWifi(context)){
            result = true;
        }
        return  result;
    }
    public interface ConnectivityReceiverListener {
        void onNetworkConnectionChanged(boolean isConnected);
    }

}