package com.indoagri.comsatic.Common;


import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import android.text.TextUtils;

public class Converter {

    private String value;

    public Converter(String value){
        if(!TextUtils.isEmpty(value)){
            this.value = value;
        }else{
            this.value = "0";
        }
    }

    public int StrToInt(){
        try{
            return Integer.parseInt(value);
        }catch(NumberFormatException e){
            return 0;
        }
    }

    public float StrToFloat(){
        try{
            return Float.parseFloat(value);
        }catch(NumberFormatException e){
            return 0;
        }
    }

    private double cnvToDouble(String value){
        try{
            return Double.parseDouble(value);
        }catch(NumberFormatException e){
            return 0;
        }
    }

    public double StrToDouble() {

//	    Locale theLocale = Locale.getDefault();
        Locale theLocale = Locale.US;
        NumberFormat numberFormat = DecimalFormat.getInstance(theLocale);
        Number theNumber;

        try {
            theNumber = numberFormat.parse(value);
            return theNumber.doubleValue();
        } catch (ParseException e) {
            String valueWithDot = value.replaceAll(",",".");
            return cnvToDouble(valueWithDot);
        }
    }

    public String StrToDigit(int digits){
        if(!TextUtils.isEmpty(value)){
            int length = value.length();

            if(length < digits){
                for(int i = 0; i < (digits - length); i++){
                    value = "0" + value;
                }
            }
        }

        return value;
    }

}
